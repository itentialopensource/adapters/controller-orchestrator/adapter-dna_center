# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Dna_center System. The API that was used to build the adapter for Dna_center is usually available in the report directory of this adapter. The adapter utilizes the Dna_center API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco DNA Center adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco DNA Center. With this adapter you have the ability to perform operations such as:

- Configure and Manage Cisco DNA Center Devices. 
- ITSM
- Sites
- Tasks
- Networks

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
