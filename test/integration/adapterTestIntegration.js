/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-dna_center',
      type: 'DnaCenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const DnaCenter = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Dna_center Adapter Test', () => {
  describe('DnaCenter Class Tests', () => {
    const a = new DnaCenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    /* eslint no-underscore-dangle:warn */

    let randExt = `${Math.random()}`;
    randExt = randExt.replace('.', '-');
    const contentType = 'application/json';
    const projectName = `project${randExt}`;
    let projectId = '';
    let templateId = '1234';
    const templateName = `template${randExt}`;

    const projectRequest = {
      description: 'This is a test',
      name: projectName
    };

    describe('#postDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerproject(projectRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1templateProgrammerproject(projectName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              projectId = data.response[0].id;

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const uprojectRequest = {
            description: 'This is a test - updated',
            id: projectId,
            name: `${projectName}-updated`
          };
          a.putDnaintentapiv1templateProgrammerproject(uprojectRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1templateProgrammerprojectprojectIdtemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const templateRequest = {
            description: 'This is a test template',
            deviceTypes: [
              {
                productFamily: 'Routers',
                productSeries: 'Cisco 3000 Series Routers',
                productType: 'Cisco 3000 Router'
              }
            ],
            name: templateName,
            projectName,
            softwareType: 'IOS'
          };
          a.postDnaintentapiv1templateProgrammerprojectprojectIdtemplate(templateRequest, contentType, projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        const p = new Promise((resolve) => {
          a.getDnaintentapiv1templateProgrammerproject(projectName, (data, error) => {
            resolve(data);
            runCommonAsserts(data, error);

            if (stub) {
              assert.equal('object', typeof data.response[0]);
            } else {
              runCommonAsserts(data, error);
            }

            templateId = data.response[0].templates[0].id;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammertemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const templateSoftwareType = 'IOS';
          a.getDnaintentapiv1templateProgrammertemplate(null, templateSoftwareType, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1templateProgrammertemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const utemplateRequest = {
            description: 'This is a test template - updated',
            deviceTypes: [
              {
                productFamily: 'Routers',
                productSeries: 'Cisco 3000 Series Routers',
                productType: 'Cisco 3000 Router'
              }
            ],
            id: templateId,
            name: templateName,
            projectId,
            softwareType: 'IOS'
          };
          a.putDnaintentapiv1templateProgrammertemplate(utemplateRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1templateProgrammertemplatedeploy - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1templateProgrammertemplatedeploy('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.deploymentId);
    //             assert.equal('string', data.response.deploymentName);
    //             assert.equal(true, Array.isArray(data.response.devices));
    //             assert.equal('string', data.response.duration);
    //             assert.equal('string', data.response.endTime);
    //             assert.equal('string', data.response.projectName);
    //             assert.equal('string', data.response.startTime);
    //             assert.equal('string', data.response.status);
    //             assert.equal('string', data.response.templateName);
    //             assert.equal('string', data.response.templateVersion);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1templateProgrammertemplateversion - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1templateProgrammertemplateversion('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1templateProgrammertemplatetemplateId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1templateProgrammertemplatetemplateId('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.author);
    //             assert.equal(false, data.response.composite);
    //             assert.equal(true, Array.isArray(data.response.containingTemplates));
    //             assert.equal(5, data.response.createTime);
    //             assert.equal('string', data.response.description);
    //             assert.equal(true, Array.isArray(data.response.deviceTypes));
    //             assert.equal('ROLLBACK_TARGET_ON_ERROR', data.response.failurePolicy);
    //             assert.equal('string', data.response.id);
    //             assert.equal(3, data.response.lastUpdateTime);
    //             assert.equal('string', data.response.name);
    //             assert.equal('string', data.response.parentTemplateId);
    //             assert.equal('string', data.response.projectId);
    //             assert.equal('string', data.response.projectName);
    //             assert.equal('string', data.response.rollbackTemplateContent);
    //             assert.equal(true, Array.isArray(data.response.rollbackTemplateParams));
    //             assert.equal('string', data.response.softwareType);
    //             assert.equal('string', data.response.softwareVariant);
    //             assert.equal('string', data.response.softwareVersion);
    //             assert.equal(true, Array.isArray(data.response.tags));
    //             assert.equal('string', data.response.templateContent);
    //             assert.equal(true, Array.isArray(data.response.templateParams));
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1templateProgrammertemplatetemplateId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1templateProgrammertemplatetemplateId('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.deploymentId);
    //             assert.equal('string', data.response.deploymentName);
    //             assert.equal(true, Array.isArray(data.response.devices));
    //             assert.equal('string', data.response.duration);
    //             assert.equal('string', data.response.endTime);
    //             assert.equal('string', data.response.projectName);
    //             assert.equal('string', data.response.startTime);
    //             assert.equal('string', data.response.status);
    //             assert.equal('string', data.response.templateName);
    //             assert.equal('string', data.response.templateVersion);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1templateProgrammertemplateversiontemplateId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1templateProgrammertemplateversiontemplateId('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response[0]);
    //             assert.equal('object', typeof data.response[1]);
    //             assert.equal('object', typeof data.response[2]);
    //             assert.equal('object', typeof data.response[3]);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1templateProgrammertemplatepreview - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1templateProgrammertemplatepreview('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.cliPreview);
    //             assert.equal('string', data.response.templateId);
    //             assert.equal('object', typeof data.response.validationErrors);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });
    const tagName = `tag${randExt}`;
    let tagId = '123';
    const memberType = 'host';
    const tagRequest = {
      systemTag: false,
      description: 'string',
      name: tagName
    };
    describe('#postDnaintentapiv1tag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1tag(tagRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tag(null, tagName, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.response));
              } else {
                runCommonAsserts(data, error);
                tagId = data.response.response[0].id;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1tag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const uTagRequest = {
            systemTag: false,
            name: `${tagName}-changed1}`,
            id: tagId
          };
          a.putDnaintentapiv1tag(uTagRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tagid(tagId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1tagidmember - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1tagidmember('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1tagidmember - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tagidmember(tagId, memberType, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.response));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagmembertype - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tagmembertype((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.response));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putDnaintentapiv1tagmember - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1tagmember('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1tagidmembercount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tagidmembercount(tagId, memberType, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagcount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tagcount(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteDnaintentapiv1tagidmembermemberId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1tagidmembermemberId('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialnetconf - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialnetconf('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialnetconf - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialnetconf('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialsnmpv2WriteCommunity - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialsnmpv2WriteCommunity('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialsnmpv2WriteCommunity - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialsnmpv2WriteCommunity('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialsnmpv3 - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialsnmpv3('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialsnmpv3 - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialsnmpv3('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialhttpWrite - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialhttpWrite('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialhttpWrite - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialhttpWrite('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1snmpProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1snmpProperty((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1snmpProperty - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1snmpProperty('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialsnmpv2ReadCommunity - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialsnmpv2ReadCommunity('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialsnmpv2ReadCommunity - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialsnmpv2ReadCommunity('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialglobalCredentialId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialglobalCredentialId('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const credentialSubType = 'NETCONF';
    let globalCredentialId = 'af345f05-14fd-4cac-94b7-ac3b8f9ff4d2';
    describe('#getDnaintentapiv1globalCredential - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1globalCredential(credentialSubType, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              if (data.response.response[0]) {
                globalCredentialId = data.response.response[0].id;
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1globalCredentialid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1globalCredentialid(globalCredentialId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1globalCredentialcli - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialcli('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialcli - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialcli('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1globalCredentialhttpRead - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1globalCredentialhttpRead('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1globalCredentialhttpRead - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1globalCredentialhttpRead('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1globalCredentialglobalCredentialId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1globalCredentialglobalCredentialId('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    let discoveryId = 1;
    const startIndex = 1;
    const recordsToReturn = 6;
    describe('#getDnaintentapiv1discoverystartIndexrecordsToReturn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoverystartIndexrecordsToReturn(startIndex, recordsToReturn, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
                if (data.response.response[0]) {
                  discoveryId = data.response.response[0].id;
                }
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidsummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryidsummary(discoveryId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(7, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryid(discoveryId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1discovery - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1discovery('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1discovery - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1discovery('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1discoverycount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoverycount((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(7, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteDnaintentapiv1discoveryid - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1discoveryid('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1discovery - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1discovery((data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const deviceIp = '10.10.20.51';
    describe('#getDnaintentapiv1discoveryjob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryjob(null, null, deviceIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidnetworkDevicecount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicecount(discoveryId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(7, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidjob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryidjob(discoveryId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn(discoveryId, startIndex, recordsToReturn, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteDnaintentapiv1discoverystartIndexrecordsToDelete - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1discoverystartIndexrecordsToDelete('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1discoveryidnetworkDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevice(discoveryId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let siteId = '12345';
    const epochTimeStampMillisecond = Date.now();
    describe('#getDnaintentapiv1siteHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1siteHealth(epochTimeStampMillisecond, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
              } else {
                runCommonAsserts(data, error);
              }
              if (data.response.response[0]) {
                /* eslint-disable-next-line */
                siteId = data.response.response[0].siteId;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1sitesiteIddevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1sitesiteIddevice('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnasystemapiv1sitesiteIddevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnasystemapiv1sitesiteIddevice('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1site - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1site('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnasystemapiv1site - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnasystemapiv1site('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let PnPdeviceId = '5c7e26ac30c95a00076fbb15';
    const pnpDeviceRequest = {
      deviceInfo: {
        serialNumber: '99999999999'
      }
    };
    describe('#postDnaintentapiv1onboardingpnpDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevice(pnpDeviceRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('object', typeof data.response.deviceInfo);
                assert.equal('object', typeof data.response.systemResetWorkflow);
                assert.equal('object', typeof data.response.systemWorkflow);
                assert.equal('object', typeof data.response.workflow);
                assert.equal(true, Array.isArray(data.response.runSummaryList));
                assert.equal('object', typeof data.response.workflowParameters);
                assert.equal('object', typeof data.response.dayZeroConfig);
                assert.equal('object', typeof data.response.dayZeroConfigPreview);
                assert.equal(10, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
                PnPdeviceId = data.response.id;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const upnpDeviceRequest = pnpDeviceRequest;
    upnpDeviceRequest.deviceInfo.name = `test${randExt}-changed`;
    describe('#putDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpDeviceid(upnpDeviceRequest, contentType, PnPdeviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('object', typeof data.response.deviceInfo);
                assert.equal('object', typeof data.response.systemResetWorkflow);
                assert.equal('object', typeof data.response.systemWorkflow);
                assert.equal('object', typeof data.response.workflow);
                assert.equal(true, Array.isArray(data.response.runSummaryList));
                assert.equal('object', typeof data.response.workflowParameters);
                assert.equal('object', typeof data.response.dayZeroConfig);
                assert.equal('object', typeof data.response.dayZeroConfigPreview);
                assert.equal(7, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevice(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('object', typeof data.response.deviceInfo);
                assert.equal('object', typeof data.response.systemResetWorkflow);
                assert.equal('object', typeof data.response.systemWorkflow);
                assert.equal('object', typeof data.response.workflow);
                assert.equal(true, Array.isArray(data.response.runSummaryList));
                assert.equal('object', typeof data.response.workflowParameters);
                assert.equal('object', typeof data.response.dayZeroConfig);
                assert.equal('object', typeof data.response.dayZeroConfigPreview);
                assert.equal(4, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDeviceid(PnPdeviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('object', typeof data.response.deviceInfo);
                assert.equal('object', typeof data.response.systemResetWorkflow);
                assert.equal('object', typeof data.response.systemWorkflow);
                assert.equal('object', typeof data.response.workflow);
                assert.equal(true, Array.isArray(data.response.runSummaryList));
                assert.equal('object', typeof data.response.workflowParameters);
                assert.equal('object', typeof data.response.dayZeroConfig);
                assert.equal('object', typeof data.response.dayZeroConfigPreview);
                assert.equal(3, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1onboardingpnpSettingssavacct - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpSettingssavacct('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.virtualAccountId);
    //             assert.equal(6, data.response.autoSyncPeriod);
    //             assert.equal('string', data.response.syncResultStr);
    //             assert.equal('object', typeof data.response.profile);
    //             assert.equal('string', data.response.ccoUser);
    //             assert.equal('object', typeof data.response.syncResult);
    //             assert.equal('string', data.response.token);
    //             assert.equal(4, data.response.syncStartTime);
    //             assert.equal(1, data.response.lastSync);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('string', data.response.smartAccountId);
    //             assert.equal(3, data.response.expiry);
    //             assert.equal('string', data.response.syncStatus);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1onboardingpnpSettingssavacct - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1onboardingpnpSettingssavacct('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.virtualAccountId);
    //             assert.equal(6, data.response.autoSyncPeriod);
    //             assert.equal('string', data.response.syncResultStr);
    //             assert.equal('object', typeof data.response.profile);
    //             assert.equal('string', data.response.ccoUser);
    //             assert.equal('object', typeof data.response.syncResult);
    //             assert.equal('string', data.response.token);
    //             assert.equal(5, data.response.syncStartTime);
    //             assert.equal(1, data.response.lastSync);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('string', data.response.smartAccountId);
    //             assert.equal(3, data.response.expiry);
    //             assert.equal('string', data.response.syncStatus);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1onboardingpnpSettingssacct - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1onboardingpnpSettingssacct((data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response[0]);
    //             assert.equal('string', data.response[1]);
    //             assert.equal('string', data.response[2]);
    //             assert.equal('string', data.response[3]);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.virtualAccountId);
    //             assert.equal(3, data.response.autoSyncPeriod);
    //             assert.equal('string', data.response.syncResultStr);
    //             assert.equal('object', typeof data.response.profile);
    //             assert.equal('string', data.response.ccoUser);
    //             assert.equal('object', typeof data.response.syncResult);
    //             assert.equal('string', data.response.token);
    //             assert.equal(3, data.response.syncStartTime);
    //             assert.equal(10, data.response.lastSync);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('string', data.response.smartAccountId);
    //             assert.equal(1, data.response.expiry);
    //             assert.equal('string', data.response.syncStatus);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1onboardingpnpSettingssacctdomainvacct - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1onboardingpnpSettingssacctdomainvacct('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response[0]);
    //             assert.equal('string', data.response[1]);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1onboardingpnpDeviceimport - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpDeviceimport('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.successList));
    //             assert.equal(true, Array.isArray(data.response.failureList));
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1onboardingpnpWorkflowid('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response._id);
    //             assert.equal('string', data.response.state);
    //             assert.equal('string', data.response.type);
    //             assert.equal('string', data.response.description);
    //             assert.equal(10, data.response.lastupdateOn);
    //             assert.equal('string', data.response.imageId);
    //             assert.equal(6, data.response.currTaskIdx);
    //             assert.equal(6, data.response.addedOn);
    //             assert.equal(true, Array.isArray(data.response.tasks));
    //             assert.equal(false, data.response.addToInventory);
    //             assert.equal('string', data.response.instanceType);
    //             assert.equal(10, data.response.endTime);
    //             assert.equal(10, data.response.execTime);
    //             assert.equal(6, data.response.startTime);
    //             assert.equal('string', data.response.useState);
    //             assert.equal('string', data.response.configId);
    //             assert.equal('string', data.response.name);
    //             assert.equal(2, data.response.version);
    //             assert.equal('string', data.response.tenantId);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1onboardingpnpWorkflowid('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response._id);
    //             assert.equal('string', data.response.state);
    //             assert.equal('string', data.response.type);
    //             assert.equal('string', data.response.description);
    //             assert.equal(6, data.response.lastupdateOn);
    //             assert.equal('string', data.response.imageId);
    //             assert.equal(4, data.response.currTaskIdx);
    //             assert.equal(3, data.response.addedOn);
    //             assert.equal(true, Array.isArray(data.response.tasks));
    //             assert.equal(true, data.response.addToInventory);
    //             assert.equal('string', data.response.instanceType);
    //             assert.equal(2, data.response.endTime);
    //             assert.equal(1, data.response.execTime);
    //             assert.equal(9, data.response.startTime);
    //             assert.equal('string', data.response.useState);
    //             assert.equal('string', data.response.configId);
    //             assert.equal('string', data.response.name);
    //             assert.equal(5, data.response.version);
    //             assert.equal('string', data.response.tenantId);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1onboardingpnpDevicesiteClaim - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpDevicesiteClaim('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1onboardingpnpSettingsvacct - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1onboardingpnpSettingsvacct('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.virtualAccountId);
    //             assert.equal(9, data.response.autoSyncPeriod);
    //             assert.equal('string', data.response.syncResultStr);
    //             assert.equal('object', typeof data.response.profile);
    //             assert.equal('string', data.response.ccoUser);
    //             assert.equal('object', typeof data.response.syncResult);
    //             assert.equal('string', data.response.token);
    //             assert.equal(4, data.response.syncStartTime);
    //             assert.equal(5, data.response.lastSync);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('string', data.response.smartAccountId);
    //             assert.equal(10, data.response.expiry);
    //             assert.equal('string', data.response.syncStatus);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1onboardingpnpSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1onboardingpnpSettings('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.savaMappingList));
    //             assert.equal('object', typeof data.response.taskTimeOuts);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('object', typeof data.response.aaaCredentials);
    //             assert.equal('object', typeof data.response.defaultProfile);
    //             assert.equal(false, data.response.acceptEula);
    //             assert.equal('string', data.response.id);
    //             assert.equal('string', data.response._id);
    //             assert.equal(2, data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1onboardingpnpSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1onboardingpnpSettings((data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.savaMappingList));
    //             assert.equal('object', typeof data.response.taskTimeOuts);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('object', typeof data.response.aaaCredentials);
    //             assert.equal('object', typeof data.response.defaultProfile);
    //             assert.equal(true, data.response.acceptEula);
    //             assert.equal('string', data.response.id);
    //             assert.equal('string', data.response._id);
    //             assert.equal(7, data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1onboardingpnpDevicereset - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpDevicereset('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.jsonArrayResponse));
    //             assert.equal('object', typeof data.response.jsonResponse);
    //             assert.equal('string', data.response.message);
    //             assert.equal(6, data.response.statusCode);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1onboardingpnpWorkflow - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpWorkflow('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response._id);
    //             assert.equal('string', data.response.state);
    //             assert.equal('string', data.response.type);
    //             assert.equal('string', data.response.description);
    //             assert.equal(5, data.response.lastupdateOn);
    //             assert.equal('string', data.response.imageId);
    //             assert.equal(10, data.response.currTaskIdx);
    //             assert.equal(3, data.response.addedOn);
    //             assert.equal(true, Array.isArray(data.response.tasks));
    //             assert.equal(false, data.response.addToInventory);
    //             assert.equal('string', data.response.instanceType);
    //             assert.equal(10, data.response.endTime);
    //             assert.equal(10, data.response.execTime);
    //             assert.equal(2, data.response.startTime);
    //             assert.equal('string', data.response.useState);
    //             assert.equal('string', data.response.configId);
    //             assert.equal('string', data.response.name);
    //             assert.equal(7, data.response.version);
    //             assert.equal('string', data.response.tenantId);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    let workflowId = '1234';
    describe('#getDnaintentapiv1onboardingpnpWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpWorkflow(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
                if (data.response[0]) {
                  workflowId = data.response[0].id;
                }
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpWorkflowid(workflowId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.lastupdateOn);
                assert.equal('string', data.response.imageId);
                assert.equal(5, data.response.currTaskIdx);
                assert.equal(1, data.response.addedOn);
                assert.equal(true, Array.isArray(data.response.tasks));
                assert.equal(true, data.response.addToInventory);
                assert.equal('string', data.response.instanceType);
                assert.equal(4, data.response.endTime);
                assert.equal(10, data.response.execTime);
                assert.equal(2, data.response.startTime);
                assert.equal('string', data.response.useState);
                assert.equal('string', data.response.configId);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpWorkflowcount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpWorkflowcount(null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.response);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDeviceunclaim - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          const deviceUnclaimRequest = {
            deviceIdList: [
              PnPdeviceId
            ]
          };
          a.postDnaintentapiv1onboardingpnpDeviceunclaim(deviceUnclaimRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.jsonArrayResponse));
                assert.equal('object', typeof data.response.jsonResponse);
                assert.equal('string', data.response.message);
                assert.equal(10, data.response.statusCode);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1onboardingpnpDevicevacctSync - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpDevicevacctSync('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.virtualAccountId);
    //             assert.equal(8, data.response.autoSyncPeriod);
    //             assert.equal('string', data.response.syncResultStr);
    //             assert.equal('object', typeof data.response.profile);
    //             assert.equal('string', data.response.ccoUser);
    //             assert.equal('object', typeof data.response.syncResult);
    //             assert.equal('string', data.response.token);
    //             assert.equal(3, data.response.syncStartTime);
    //             assert.equal(9, data.response.lastSync);
    //             assert.equal('string', data.response.tenantId);
    //             assert.equal('string', data.response.smartAccountId);
    //             assert.equal(10, data.response.expiry);
    //             assert.equal('string', data.response.syncStatus);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1onboardingpnpDevicecount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevicecount(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(3, data.response.response);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceSerialNumber = 'FCW2214L0VK';
    describe('#getDnaintentapiv1onboardingpnpDevicehistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevicehistory(deviceSerialNumber, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal(8, data.response.statusCode);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // const PnPdeviceIdConfigPreview = '5c7e26ac30c95a00076fbb15';
    // describe('#postDnaintentapiv1onboardingpnpDevicesiteConfigPreview - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       const siteConfigRequest = {
    //         deviceId: PnPdeviceIdConfigPreview,
    //         siteId,
    //         type: 'Default'
    //       };
    //       a.postDnaintentapiv1onboardingpnpDevicesiteConfigPreview(siteConfigRequest, contentType, (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1onboardingpnpDeviceclaim - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1onboardingpnpDeviceclaim('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.jsonArrayResponse));
    //             assert.equal('object', typeof data.response.jsonResponse);
    //             assert.equal('string', data.response.message);
    //             assert.equal(9, data.response.statusCode);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getDnaintentapiv1imageimportation - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1imageimportation('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.response));
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1imageimportationsourcefile - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1imageimportationsourcefile('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1imagedistribution - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1imagedistribution('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1imageimportationsourceurl - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1imageimportationsourceurl('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1imageactivationdevice - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1imageactivationdevice('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1networkDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevice(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceWithOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceWithOptions(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1networkDevice - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1networkDevice('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putDnaintentapiv1networkDevice - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1networkDevice('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const deviceId = '6a49c827-9b28-490b-8df0-8b6c3b582d8a';
    let interfaceName = 'Vlan823';
    describe('#getDnaintentapiv1networkDeviceidvlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidvlan(deviceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              if (data.response.response[0]) {
                /* eslint-disable-next-line */
                interfaceName = data.response.response[0].interfaceName;
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn(deviceId, startIndex, recordsToReturn, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicecollectionScheduleglobal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicecollectionScheduleglobal((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(4, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacecount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfacecount((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteDnaintentapiv1networkDeviceid - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1networkDeviceid('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1networkDeviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceid(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putDnaintentapiv1networkDevicesync - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1networkDevicesync('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1networkDevicecount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicecount((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceconfigcount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceconfigcount((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidcollectionSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidcollectionSchedule(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName(deviceId, interfaceName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getDnaintentapiv1networkDeviceidmerakiOrganization - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1networkDeviceidmerakiOrganization('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.response));
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdcount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdcount(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidbrief - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidbrief(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicenetworkDeviceIdconfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicenetworkDeviceIdconfig(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceospf - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfaceospf((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceisis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfaceisis((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceconfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceconfig((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let funcCapabilityId = '0ef81f3a-3fec-48a9-bf4c-678946331d8b';
    describe('#getDnaintentapiv1networkDevicefunctionalCapability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicefunctionalCapability(deviceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              if (data.response.response[0]) {
                funcCapabilityId = data.response.response[0].functionalCapability[0].id;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicefunctionalCapabilityid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicefunctionalCapabilityid(funcCapabilityId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceId = '72112d94-7bc4-4e3b-8a95-914e12e0d578';
    const interfaceIp = '10.10.20.81';
    describe('#getDnaintentapiv1interface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interface((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfaceid(interfaceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceId(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putDnaintentapiv1networkDevicebrief - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.putDnaintentapiv1networkDevicebrief('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1networkDevicefile - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1networkDevicefile('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const deviceMacAddress = '00:72:78:54:d1:00';
    describe('#getDnaintentapiv1networkDevicetenantinfomacaddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicetenantinfomacaddress(deviceSerialNumber, deviceMacAddress, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceipAddressipAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1interfaceipAddressipAddress(interfaceIp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceserialNumberserialNumber - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceserialNumberserialNumber(deviceSerialNumber, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceipAddressipAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceipAddressipAddress(deviceIp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let moduleId = '53ac6145-45ee-4122-84fb-9c729f388868';
    describe('#getDnaintentapiv1networkDevicemodule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemodule(deviceId, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              if (data.response.response[0]) {
                moduleId = data.response.response[0].id;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicemoduleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemoduleid(moduleId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicemodulecount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemodulecount(deviceId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(6, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicestartIndexrecordsToReturn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicestartIndexrecordsToReturn(startIndex, recordsToReturn, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identifier = 'uuid';
    describe('#getDnaintentapiv1deviceDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1deviceDetail(epochTimeStampMillisecond, deviceId, identifier, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceautocomplete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceautocomplete(null, null, null, deviceMacAddress, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidwirelessInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidwirelessInfo(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1taskcount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1taskcount(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let taskId = '1234';
    describe('#getDnaintentapiv1task - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1task(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              taskId = data.response.response[0].id;

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tasktaskId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tasktaskId(taskId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tasktaskIdtree - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1tasktaskIdtree(taskId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getDnaintentapiv1taskoperationoperationIdoffsetlimit - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1taskoperationoperationIdoffsetlimit('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal(true, Array.isArray(data.response.response));
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    let nameSpace = 'config';
    describe('#getDnaintentapiv1filenamespace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1filenamespace((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              /* eslint-disable-next-line */
              nameSpace = data.response.response[0];

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let fileId = '12345';
    describe('#getDnaintentapiv1filenamespacenameSpace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1filenamespacenameSpace(nameSpace, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              fileId = data.response.response[0].id;

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1filefileId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnaintentapiv1filefileId(fileId, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicePollerclilegitReads - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkDevicePollerclilegitReads((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1networkDevicePollerclireadRequest - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1networkDevicePollerclireadRequest('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('object', typeof data.response.response);
    //             assert.equal('string', data.response.version);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    let vlanName = 'Vlan1';
    describe('#getDnaintentapiv1topologyvlanvlanNames - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1topologyvlanvlanNames((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              /* eslint-disable-next-line */
              vlanName = data.response.response[0];

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologysiteTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1topologysiteTopology((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyphysicalTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1topologyphysicalTopology(null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyl2vlanID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1topologyl2vlanID(vlanName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyType = 'OSPF';
    describe('#getDnaintentapiv1topologyl3topologyType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1topologyl3topologyType(topologyType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1networkHealth(epochTimeStampMillisecond, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let flowId = '1234';
    describe('#getDnaintentapiv1flowAnalysis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1flowAnalysis(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              if (data.response.response[0]) {
                flowId = data.response.response[0].id;
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowAnalysisRequest = {
      controlPath: true,
      destIP: deviceIp,
      periodicRefresh: true,
      protocol: 'tcp',
      sourceIP: deviceIp
    };
    describe('#postDnaintentapiv1flowAnalysis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1flowAnalysis(flowAnalysisRequest, contentType, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              flowId = data.response.response.flowAnalysisId;

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1flowAnalysisflowAnalysisId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1flowAnalysisflowAnalysisId(flowId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1businessnfvprovisioningDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1businessnfvprovisioningDetail(deviceIp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.provisionDetails);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postDnaintentapiv1businessnfv - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1businessnfv('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.executionId);
    //             assert.equal('string', data.response.executionStatusUrl);
    //             assert.equal('string', data.response.message);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnasystemapiv1authtoken - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnasystemapiv1authtoken('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.Token);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1businessssidssidNamemanagedAPLocations - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1businessssidssidNamemanagedAPLocations('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.executionId);
    //             assert.equal('string', data.response.executionStatusUrl);
    //             assert.equal('string', data.response.message);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1businessssid - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1businessssid('fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.executionId);
    //             assert.equal('string', data.response.executionStatusUrl);
    //             assert.equal('string', data.response.message);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const enterpriseSSIDRequest = {
      name: `enterprise${randExt}`,
      securityLevel: 'WPA2_ENTERPRISE',
      passphrase: '',
      enableFastLane: true,
      enableMACFiltering: true,
      trafficType: 'voicedata',
      radioPolicy: 'Dual band operation (2.4GHz and 5GHz)',
      enableBroadcastSSID: true,
      fastTransition: 'Adaptive'
    };
    describe('#postDnaintentapiv1enterpriseSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDnaintentapiv1enterpriseSsid(enterpriseSSIDRequest, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ssidName = enterpriseSSIDRequest.name;
    describe('#getDnaintentapiv1enterpriseSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1enterpriseSsid(null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1clientDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1clientDetail(epochTimeStampMillisecond, deviceMacAddress, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.detail);
                assert.equal('object', typeof data.response.connectionInfo);
                assert.equal('object', typeof data.response.topology);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1clientHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnaintentapiv1clientHealth(epochTimeStampMillisecond, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.response));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getDnaintentapiv1businesssdaborderDevicedeviceIpAddress - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.getDnaintentapiv1businesssdaborderDevicedeviceIpAddress('fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.status);
    //             assert.equal('string', data.response.description);
    //             assert.equal('object', typeof data.response.payload);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postDnaintentapiv1businesssdaborderDevice - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postDnaintentapiv1businesssdaborderDevice('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.status);
    //             assert.equal('string', data.response.description);
    //             assert.equal('string', data.response.executionStatusUrl);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         try {
    //           runCommonAsserts(data, error);

    //           if (stub) {
    //             assert.equal('string', data.response.status);
    //             assert.equal('string', data.response.description);
    //             assert.equal('string', data.response.executionStatusUrl);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }

    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteDnaintentapiv1tagid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnaintentapiv1tagid(tagId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnaintentapiv1onboardingpnpDeviceid(PnPdeviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response._id);
                assert.equal('object', typeof data.response.deviceInfo);
                assert.equal('object', typeof data.response.systemResetWorkflow);
                assert.equal('object', typeof data.response.systemWorkflow);
                assert.equal('object', typeof data.response.workflow);
                assert.equal(true, Array.isArray(data.response.runSummaryList));
                assert.equal('object', typeof data.response.workflowParameters);
                assert.equal('object', typeof data.response.dayZeroConfig);
                assert.equal('object', typeof data.response.dayZeroConfigPreview);
                assert.equal(1, data.response.version);
                assert.equal('string', data.response.tenantId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1flowAnalysisflowAnalysisId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnaintentapiv1flowAnalysisflowAnalysisId(flowId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1enterpriseSsidssidName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnaintentapiv1enterpriseSsidssidName(ssidName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.executionId);
                assert.equal('string', data.response.executionStatusUrl);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1templateProgrammerprojectprojectId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnaintentapiv1templateProgrammerprojectprojectId(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.response);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePortAssignmentForAccessPointInSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deletePortAssignmentForAccessPointInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPortAssignmentForAccessPointInSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getPortAssignmentForAccessPointInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddPortAssignmentForAccessPointInSDAFabricBodyParam = {
      siteNameHierarchy: 'string',
      deviceManagementIpAddress: 'string',
      interfaceName: 'string',
      dataIpAddressPoolName: 'string',
      authenticateTemplateName: 'Closed Authentication ',
      interfaceDescription: 'string'
    };
    describe('#addPortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPortAssignmentForAccessPointInSDAFabric(sDAAddPortAssignmentForAccessPointInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addPortAssignmentForAccessPointInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInfoFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceInfoFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getDeviceInfoFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitPeerNetworkInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransitPeerNetworkInfo('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getTransitPeerNetworkInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddTransitPeerNetworkBodyParam = {
      transitPeerNetworkName: 'string',
      transitPeerNetworkType: 'ip_transit',
      ipTransitSettings: {
        routingProtocolName: 'BGP',
        autonomousSystemNumber: 'string'
      },
      sdaTransitSettings: {
        transitControlPlaneSettings: [
          {
            siteNameHierarchy: 'string',
            deviceManagementIpAddress: 'string'
          }
        ]
      }
    };
    describe('#addTransitPeerNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addTransitPeerNetwork(sDAAddTransitPeerNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addTransitPeerNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitPeerNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransitPeerNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteTransitPeerNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEdgeDeviceFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEdgeDeviceFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteEdgeDeviceFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEdgeDeviceFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEdgeDeviceFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getEdgeDeviceFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddEdgeDeviceInSDAFabricBodyParam = {
      deviceManagementIpAddress: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#addEdgeDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addEdgeDeviceInSDAFabric(sDAAddEdgeDeviceInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addEdgeDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddIPPoolInSDAVirtualNetworkBodyParam = {
      siteNameHierarchy: 'string',
      virtualNetworkName: 'string',
      isLayer2Only: true,
      ipPoolName: 'string',
      vlanId: 'string',
      vlanName: 'string',
      autoGenerateVlanName: true,
      trafficType: 'Data',
      scalableGroupName: 'string',
      isL2FloodingEnabled: false,
      isThisCriticalPool: true,
      isWirelessPool: true,
      isIpDirectedBroadcast: false,
      isCommonPool: false,
      isBridgeModeVm: false,
      poolType: 'AP'
    };
    describe('#addIPPoolInSDAVirtualNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addIPPoolInSDAVirtualNetwork(sDAAddIPPoolInSDAVirtualNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addIPPoolInSDAVirtualNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPPoolFromSDAVirtualNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIPPoolFromSDAVirtualNetwork('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteIPPoolFromSDAVirtualNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPPoolFromSDAVirtualNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPPoolFromSDAVirtualNetwork('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getIPPoolFromSDAVirtualNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMulticastFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMulticastFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteMulticastFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMulticastDetailsFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMulticastDetailsFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getMulticastDetailsFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddMulticastInSDAFabricBodyParam = {
      siteNameHierarchy: 'string',
      multicastMethod: 'native_multicast',
      multicastType: 'ssm',
      multicastVnInfo: [
        {
          virtualNetworkName: 'string',
          ipPoolName: 'string',
          internalRpIpAddress: [
            'string'
          ],
          externalRpIpAddress: 'string',
          ssmInfo: [
            {
              ssmGroupRange: 'string',
              ssmWildcardMask: 'string'
            }
          ]
        }
      ]
    };
    describe('#addMulticastInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMulticastInSDAFabric(sDAAddMulticastInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addMulticastInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVirtualNetworkFromSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getVirtualNetworkFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddVNInFabricBodyParam = {
      virtualNetworkName: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#addVNInFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addVNInFabric(sDAAddVNInFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addVNInFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVNFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVNFromSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteVNFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultAuthenticationProfileFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultAuthenticationProfileFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteDefaultAuthenticationProfileFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAUpdateDefaultAuthenticationProfileInSDAFabricBodyParam = [
      {
        siteNameHierarchy: 'string',
        authenticateTemplateName: 'Closed Authentication',
        authenticationOrder: 'mab',
        dot1xToMabFallbackTimeout: 'string',
        wakeOnLan: true,
        numberOfHosts: 'Single'
      }
    ];
    describe('#updateDefaultAuthenticationProfileInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDefaultAuthenticationProfileInSDAFabric(sDAUpdateDefaultAuthenticationProfileInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'updateDefaultAuthenticationProfileInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultAuthenticationProfileFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultAuthenticationProfileFromSDAFabric('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getDefaultAuthenticationProfileFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddDefaultAuthenticationTemplateInSDAFabricBodyParam = [
      {
        siteNameHierarchy: 'string',
        authenticateTemplateName: 'Open Authentication'
      }
    ];
    describe('#addDefaultAuthenticationTemplateInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addDefaultAuthenticationTemplateInSDAFabric(sDAAddDefaultAuthenticationTemplateInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addDefaultAuthenticationTemplateInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSiteFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteSiteFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getSiteFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddSiteInSDAFabricBodyParam = {
      fabricName: 'string',
      siteNameHierarchy: 'string',
      fabricType: 'string'
    };
    describe('#addSiteInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSiteInSDAFabric(sDAAddSiteInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addSiteInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAReProvisionWiredDeviceBodyParam = {
      deviceManagementIpAddress: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#reProvisionWiredDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reProvisionWiredDevice(sDAReProvisionWiredDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'reProvisionWiredDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAProvisionWiredDeviceBodyParam = {
      deviceManagementIpAddress: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#provisionWiredDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.provisionWiredDevice(sDAProvisionWiredDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'provisionWiredDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProvisionedWiredDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProvisionedWiredDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getProvisionedWiredDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProvisionedWiredDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteProvisionedWiredDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteProvisionedWiredDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVirtualNetworkSummary('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getVirtualNetworkSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceRoleInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceRoleInSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getDeviceRoleInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddPortAssignmentForUserDeviceInSDAFabricBodyParam = {
      siteNameHierarchy: 'string',
      deviceManagementIpAddress: 'string',
      interfaceName: 'string',
      interfaceNames: [
        'string'
      ],
      dataIpAddressPoolName: 'string',
      voiceIpAddressPoolName: 'string',
      authenticateTemplateName: 'Low Impact',
      scalableGroupName: 'string',
      interfaceDescription: 'string'
    };
    describe('#addPortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPortAssignmentForUserDeviceInSDAFabric(sDAAddPortAssignmentForUserDeviceInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addPortAssignmentForUserDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPortAssignmentForUserDeviceInSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getPortAssignmentForUserDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePortAssignmentForUserDeviceInSDAFabric('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deletePortAssignmentForUserDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBorderDeviceDetailFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBorderDeviceDetailFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getBorderDeviceDetailFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddBorderDeviceInSDAFabricBodyParam = [
      {
        deviceManagementIpAddress: 'string',
        siteNameHierarchy: 'string',
        deviceRole: [
          'string'
        ],
        routeDistributionProtocol: 'string',
        externalDomainRoutingProtocolName: 'BGP',
        externalConnectivityIpPoolName: 'string',
        internalAutonomouSystemNumber: 'string',
        borderPriority: 'string',
        borderSessionType: 'ANYWHERE',
        connectedToInternet: true,
        sdaTransitNetworkName: 'string',
        borderWithExternalConnectivity: false,
        externalConnectivitySettings: [
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          },
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          },
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              },
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          },
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          },
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              },
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          },
          {
            interfaceName: 'string',
            interfaceDescription: 'string',
            externalAutonomouSystemNumber: 'string',
            l3Handoff: [
              {
                virtualNetwork: {
                  virtualNetworkName: 'string',
                  vlanId: 'string'
                }
              }
            ],
            l2Handoff: [
              {
                virtualNetworkName: 'string',
                vlanName: 'string'
              }
            ]
          }
        ]
      }
    ];
    describe('#addBorderDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBorderDeviceInSDAFabric(sDAAddBorderDeviceInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addBorderDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBorderDeviceFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBorderDeviceFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteBorderDeviceFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControlPlaneDeviceFromSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getControlPlaneDeviceFromSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getControlPlaneDeviceFromSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddControlPlaneDeviceInSDAFabricBodyParam = {
      deviceManagementIpAddress: 'string',
      siteNameHierarchy: 'string',
      routeDistributionProtocol: 'string'
    };
    describe('#addControlPlaneDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addControlPlaneDeviceInSDAFabric(sDAAddControlPlaneDeviceInSDAFabricBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addControlPlaneDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControlPlaneDeviceInSDAFabric - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteControlPlaneDeviceInSDAFabric('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteControlPlaneDeviceInSDAFabric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAUpdateVirtualNetworkWithScalableGroupsBodyParam = {
      virtualNetworkName: 'string',
      isGuestVirtualNetwork: false,
      scalableGroupNames: [
        'string'
      ],
      vManageVpnId: 'string'
    };
    describe('#updateVirtualNetworkWithScalableGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateVirtualNetworkWithScalableGroups(sDAUpdateVirtualNetworkWithScalableGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'updateVirtualNetworkWithScalableGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualNetworkWithScalableGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualNetworkWithScalableGroups('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'deleteVirtualNetworkWithScalableGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkWithScalableGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVirtualNetworkWithScalableGroups('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'getVirtualNetworkWithScalableGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDAAddVirtualNetworkWithScalableGroupsBodyParam = {
      virtualNetworkName: 'string',
      isGuestVirtualNetwork: false,
      scalableGroupNames: [
        'string'
      ],
      vManageVpnId: 'string'
    };
    describe('#addVirtualNetworkWithScalableGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addVirtualNetworkWithScalableGroups(sDAAddVirtualNetworkWithScalableGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDA', 'addVirtualNetworkWithScalableGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationSetsV2('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationSetsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateApplicationSetsV2BodyParam = [
      {
        name: 'string',
        scalableGroupType: 'string',
        defaultBusinessRelevance: 'BUSINESS_IRRELEVANT',
        namespace: 'string',
        qualifier: 'string',
        type: 'string',
        scalableGroupExternalHandle: 'string'
      }
    ];
    describe('#createApplicationSetsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplicationSetsV2(applicationPolicyCreateApplicationSetsV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createApplicationSetsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyDeleteApplicationPolicyQueuingProfileBodyParam = {};
    describe('#deleteApplicationPolicyQueuingProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationPolicyQueuingProfile('fakedata', applicationPolicyDeleteApplicationPolicyQueuingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteApplicationPolicyQueuingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyQueuingProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationPolicyQueuingProfile(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationPolicyQueuingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyUpdateApplicationPolicyQueuingProfileBodyParam = [
      {
        id: 'string',
        description: 'string',
        name: 'string',
        clause: [
          {
            instanceId: 5,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 9,
                interfaceSpeed: 'HUNDRED_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 3,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 7,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              }
            ]
          },
          {
            instanceId: 1,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 9,
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 7,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 1,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 4,
                    trafficClass: 'BULK_DATA'
                  }
                ]
              },
              {
                instanceId: 5,
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 8,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 8,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 9,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 6,
                    trafficClass: 'BROADCAST_VIDEO'
                  }
                ]
              },
              {
                instanceId: 5,
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    instanceId: 10,
                    bandwidthPercentage: 8,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 6,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 8,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 9,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 8,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              },
              {
                instanceId: 3,
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 6,
                    bandwidthPercentage: 3,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 6,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 8,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 10,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 7,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 1,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 7,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  }
                ]
              },
              {
                instanceId: 3,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 6,
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 4,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 2,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 4,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                instanceId: 8,
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 7,
                    bandwidthPercentage: 4,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 10,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 10,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 9,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 1,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              }
            ]
          },
          {
            instanceId: 7,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 4,
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 8,
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  }
                ]
              },
              {
                instanceId: 8,
                interfaceSpeed: 'HUNDRED_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 3,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 5,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 1,
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                instanceId: 7,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 8,
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              }
            ]
          },
          {
            instanceId: 1,
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 3,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 2,
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 7,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 5,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              },
              {
                instanceId: 9,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 5,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 1,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                instanceId: 5,
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 10,
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              },
              {
                instanceId: 7,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                instanceId: 7,
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              },
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              }
            ]
          },
          {
            instanceId: 3,
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 6,
                interfaceSpeed: 'HUNDRED_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 1,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 10,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 10,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  }
                ]
              },
              {
                instanceId: 1,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 1,
                    bandwidthPercentage: 5,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 5,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 7,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              },
              {
                instanceId: 8,
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                instanceId: 1,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                instanceId: 9,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              }
            ]
          },
          {
            instanceId: 6,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 7,
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 2,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 6,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 6,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              },
              {
                instanceId: 3,
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    instanceId: 3,
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 7,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 8,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 9,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 3,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 10,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 8,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 5,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 9,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 1,
                dscp: 'string',
                trafficClass: 'OPS_ADMIN_MGMT'
              },
              {
                instanceId: 4,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_CONFERENCING'
              },
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                instanceId: 2,
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              }
            ]
          },
          {
            instanceId: 5,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 6,
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 3,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 10,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                instanceId: 3,
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 1,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 8,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 2,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 6,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 3,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 7,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 2,
                    trafficClass: 'SCAVENGER'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 6,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                instanceId: 1,
                dscp: 'string',
                trafficClass: 'SIGNALING'
              },
              {
                instanceId: 4,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                instanceId: 4,
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                instanceId: 8,
                dscp: 'string',
                trafficClass: 'OPS_ADMIN_MGMT'
              },
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                instanceId: 7,
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                instanceId: 7,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_CONFERENCING'
              },
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'OPS_ADMIN_MGMT'
              },
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              }
            ]
          },
          {
            instanceId: 5,
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                instanceId: 4,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 1,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              },
              {
                instanceId: 7,
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 10,
                    bandwidthPercentage: 9,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 7,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 10,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 9,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              },
              {
                instanceId: 6,
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 1,
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 2,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 7,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 3,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                instanceId: 6,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 1,
                    bandwidthPercentage: 2,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 9,
                    trafficClass: 'NETWORK_CONTROL'
                  }
                ]
              },
              {
                instanceId: 3,
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 4,
                    bandwidthPercentage: 3,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    instanceId: 5,
                    bandwidthPercentage: 4,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  }
                ]
              },
              {
                instanceId: 6,
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 7,
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 8,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 10,
                    bandwidthPercentage: 6,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 4,
                    trafficClass: 'BULK_DATA'
                  }
                ]
              },
              {
                instanceId: 6,
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 5,
                    bandwidthPercentage: 3,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 3,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                instanceId: 5,
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 2,
                    bandwidthPercentage: 1,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 8,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    instanceId: 1,
                    bandwidthPercentage: 5,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              },
              {
                instanceId: 10,
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    instanceId: 9,
                    bandwidthPercentage: 4,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 9,
                    bandwidthPercentage: 1,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    instanceId: 4,
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 6,
                    bandwidthPercentage: 7,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    instanceId: 2,
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    instanceId: 3,
                    bandwidthPercentage: 3,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    instanceId: 8,
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    instanceId: 7,
                    bandwidthPercentage: 4,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                instanceId: 5,
                dscp: 'string',
                trafficClass: 'VOIP_TELEPHONY'
              },
              {
                instanceId: 3,
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_CONFERENCING'
              },
              {
                instanceId: 10,
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                instanceId: 9,
                dscp: 'string',
                trafficClass: 'SIGNALING'
              }
            ]
          }
        ]
      }
    ];
    describe('#updateApplicationPolicyQueuingProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateApplicationPolicyQueuingProfile(applicationPolicyUpdateApplicationPolicyQueuingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'updateApplicationPolicyQueuingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateApplicationPolicyQueuingProfileBodyParam = [
      {
        description: 'string',
        name: 'string',
        clause: [
          {
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BROADCAST_VIDEO'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'NETWORK_CONTROL'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'VOIP_TELEPHONY'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'OPS_ADMIN_MGMT'
              },
              {
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              }
            ]
          },
          {
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BEST_EFFORT'
                  }
                ]
              },
              {
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'NETWORK_CONTROL'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                dscp: 'string',
                trafficClass: 'VOIP_TELEPHONY'
              },
              {
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              }
            ]
          },
          {
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              },
              {
                dscp: 'string',
                trafficClass: 'SIGNALING'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_CONFERENCING'
              },
              {
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              }
            ]
          },
          {
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: false,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              },
              {
                interfaceSpeed: 'TEN_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'SCAVENGER'
                  }
                ]
              },
              {
                interfaceSpeed: 'ALL',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'SCAVENGER'
              },
              {
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'VOIP_TELEPHONY'
              },
              {
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              }
            ]
          },
          {
            type: 'DSCP_CUSTOMIZATION',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'HUNDRED_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              },
              {
                dscp: 'string',
                trafficClass: 'BULK_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              }
            ]
          },
          {
            type: 'BANDWIDTH',
            isCommonBetweenAllInterfaceSpeeds: true,
            interfaceSpeedBandwidthClauses: [
              {
                interfaceSpeed: 'HUNDRED_GBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'OPS_ADMIN_MGMT'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'SIGNALING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'BEST_EFFORT'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 7,
                    trafficClass: 'SIGNALING'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BROADCAST_VIDEO'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  }
                ]
              },
              {
                interfaceSpeed: 'HUNDRED_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 8,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 5,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 2,
                    trafficClass: 'VOIP_TELEPHONY'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'REAL_TIME_INTERACTIVE'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'SCAVENGER'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'BROADCAST_VIDEO'
                  }
                ]
              },
              {
                interfaceSpeed: 'ONE_MBPS',
                tcBandwidthSettings: [
                  {
                    bandwidthPercentage: 10,
                    trafficClass: 'MULTIMEDIA_CONFERENCING'
                  },
                  {
                    bandwidthPercentage: 1,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'NETWORK_CONTROL'
                  },
                  {
                    bandwidthPercentage: 9,
                    trafficClass: 'BULK_DATA'
                  },
                  {
                    bandwidthPercentage: 3,
                    trafficClass: 'MULTIMEDIA_STREAMING'
                  },
                  {
                    bandwidthPercentage: 4,
                    trafficClass: 'TRANSACTIONAL_DATA'
                  },
                  {
                    bandwidthPercentage: 6,
                    trafficClass: 'VOIP_TELEPHONY'
                  }
                ]
              }
            ],
            tcDscpSettings: [
              {
                dscp: 'string',
                trafficClass: 'SIGNALING'
              },
              {
                dscp: 'string',
                trafficClass: 'TRANSACTIONAL_DATA'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_STREAMING'
              },
              {
                dscp: 'string',
                trafficClass: 'SIGNALING'
              },
              {
                dscp: 'string',
                trafficClass: 'NETWORK_CONTROL'
              },
              {
                dscp: 'string',
                trafficClass: 'REAL_TIME_INTERACTIVE'
              },
              {
                dscp: 'string',
                trafficClass: 'BROADCAST_VIDEO'
              },
              {
                dscp: 'string',
                trafficClass: 'OPS_ADMIN_MGMT'
              },
              {
                dscp: 'string',
                trafficClass: 'MULTIMEDIA_CONFERENCING'
              },
              {
                dscp: 'string',
                trafficClass: 'BEST_EFFORT'
              }
            ]
          }
        ]
      }
    ];
    describe('#createApplicationPolicyQueuingProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplicationPolicyQueuingProfile(applicationPolicyCreateApplicationPolicyQueuingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createApplicationPolicyQueuingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyDefault - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationPolicyDefault((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationPolicyDefault', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosDeviceInterfaceInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getQosDeviceInterfaceInfo(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getQosDeviceInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyUpdateQosDeviceInterfaceInfoBodyParam = [
      {
        id: 'string',
        name: 'string',
        excludedInterfaces: [
          'string',
          'string'
        ],
        networkDeviceId: 'string',
        qosDeviceInterfaceInfo: [
          {
            instanceId: 8,
            dmvpnRemoteSitesBw: [
              7,
              2,
              3,
              2,
              7,
              1,
              9
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 5
          },
          {
            instanceId: 8,
            dmvpnRemoteSitesBw: [
              8,
              4,
              6,
              8,
              1
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'DMVPN_HUB',
            uploadBW: 4
          }
        ]
      }
    ];
    describe('#updateQosDeviceInterfaceInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQosDeviceInterfaceInfo(applicationPolicyUpdateQosDeviceInterfaceInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'updateQosDeviceInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateQosDeviceInterfaceInfoBodyParam = [
      {
        name: 'string',
        excludedInterfaces: [
          'string',
          'string',
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        networkDeviceId: 'string',
        qosDeviceInterfaceInfo: [
          {
            dmvpnRemoteSitesBw: [
              10,
              9,
              5,
              5,
              2,
              10,
              2,
              2,
              10,
              5
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 7
          },
          {
            dmvpnRemoteSitesBw: [
              6,
              7,
              4,
              7,
              8,
              1,
              6,
              7,
              1,
              5
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 4
          },
          {
            dmvpnRemoteSitesBw: [
              8,
              6,
              2,
              3,
              9,
              8,
              6
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'DMVPN_SPOKE',
            uploadBW: 4
          },
          {
            dmvpnRemoteSitesBw: [
              1,
              1,
              9
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'DMVPN_SPOKE',
            uploadBW: 4
          },
          {
            dmvpnRemoteSitesBw: [
              2,
              3,
              6,
              4,
              4,
              10
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'DMVPN_SPOKE',
            uploadBW: 3
          },
          {
            dmvpnRemoteSitesBw: [
              5,
              1,
              5,
              5,
              5,
              10
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 5
          },
          {
            dmvpnRemoteSitesBw: [
              6,
              4,
              10,
              4
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 7
          },
          {
            dmvpnRemoteSitesBw: [
              4,
              5,
              3,
              6
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 3
          },
          {
            dmvpnRemoteSitesBw: [
              3
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'WAN',
            uploadBW: 8
          },
          {
            dmvpnRemoteSitesBw: [
              9,
              8,
              5,
              5,
              3,
              6,
              3,
              1
            ],
            interfaceId: 'string',
            interfaceName: 'string',
            label: 'string',
            role: 'DMVPN_SPOKE',
            uploadBW: 9
          }
        ]
      }
    ];
    describe('#createQosDeviceInterfaceInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createQosDeviceInterfaceInfo(applicationPolicyCreateQosDeviceInterfaceInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createQosDeviceInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationPolicy(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQosDeviceInterfaceInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteQosDeviceInterfaceInfo('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteQosDeviceInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationsV2('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyEditApplicationsV2BodyParam = [
      {
        name: 'string',
        networkApplications: [
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_url',
            dscp: 'string',
            appProtocol: 'TCP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'BROADCAST_VIDEO',
            categoryId: 'string',
            ignoreConflict: false,
            rank: 9,
            engineId: 7
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_url',
            dscp: 'string',
            appProtocol: 'TCP/UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'BULK_DATA',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 2,
            engineId: 7
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_server-ip',
            dscp: 'string',
            appProtocol: 'IP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'OPS_ADMIN_MGMT',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 8,
            engineId: 3
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_server-ip',
            dscp: 'string',
            appProtocol: 'TCP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'SCAVENGER',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 1,
            engineId: 3
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_servername',
            dscp: 'string',
            appProtocol: 'TCP/UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'BEST_EFFORT',
            categoryId: 'string',
            ignoreConflict: false,
            rank: 10,
            engineId: 5
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_servername',
            dscp: 'string',
            appProtocol: 'TCP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'NETWORK_CONTROL',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 5,
            engineId: 9
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_servername',
            dscp: 'string',
            appProtocol: 'TCP/UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'MULTIMEDIA_STREAMING',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 6,
            engineId: 9
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_url',
            dscp: 'string',
            appProtocol: 'UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'REAL_TIME_INTERACTIVE',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 4,
            engineId: 10
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_server-ip',
            dscp: 'string',
            appProtocol: 'TCP/UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'SCAVENGER',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 7,
            engineId: 3
          }
        ],
        parentScalableGroup: {
          idRef: 'string'
        },
        networkIdentity: [
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 7,
            upperPort: 1
          },
          {
            protocol: 'UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 10,
            upperPort: 1
          }
        ],
        indicativeNetworkIdentity: [
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string'
            ],
            lowerPort: 4,
            upperPort: 3
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 8,
            upperPort: 9
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string'
            ],
            lowerPort: 2,
            upperPort: 4
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 1,
            upperPort: 10
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 9,
            upperPort: 5
          },
          {
            protocol: 'TCP_OR_UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string'
            ],
            lowerPort: 6,
            upperPort: 9
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 10
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 2,
            upperPort: 8
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 8
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 10,
            upperPort: 4
          }
        ],
        scalableGroupType: 'string',
        type: 'string'
      }
    ];
    describe('#editApplicationsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editApplicationsV2(applicationPolicyEditApplicationsV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'editApplicationsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateApplicationV2BodyParam = [
      {
        name: 'string',
        networkApplications: [
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_servername',
            dscp: 'string',
            appProtocol: 'TCP/UDP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'SCAVENGER',
            categoryId: 'string',
            ignoreConflict: false,
            rank: 2,
            engineId: 3
          },
          {
            helpString: 'string',
            applicationType: 'CUSTOM',
            type: '_servername',
            dscp: 'string',
            appProtocol: 'TCP',
            serverName: 'string',
            url: 'string',
            trafficClass: 'MULTIMEDIA_STREAMING',
            categoryId: 'string',
            ignoreConflict: true,
            rank: 10,
            engineId: 2
          }
        ],
        parentScalableGroup: {
          idRef: 'string'
        },
        networkIdentity: [
          {
            protocol: 'UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 2
          },
          {
            protocol: 'UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 9,
            upperPort: 1
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string'
            ],
            lowerPort: 8,
            upperPort: 3
          },
          {
            protocol: 'TCP_OR_UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 3
          },
          {
            protocol: 'TCP_OR_UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 2,
            upperPort: 1
          },
          {
            protocol: 'TCP_OR_UDP',
            ports: 'string',
            ipv4Subnet: [
              'string'
            ],
            lowerPort: 9,
            upperPort: 4
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 3
          }
        ],
        indicativeNetworkIdentity: [
          {
            protocol: 'TCP_OR_UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 2,
            upperPort: 10
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 4,
            upperPort: 10
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            lowerPort: 7,
            upperPort: 9
          },
          {
            protocol: 'UDP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string'
            ],
            lowerPort: 2,
            upperPort: 7
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string'
            ],
            lowerPort: 8,
            upperPort: 2
          },
          {
            protocol: 'IP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string',
              'string',
              'string',
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string'
            ],
            lowerPort: 1,
            upperPort: 9
          },
          {
            protocol: 'TCP',
            ports: 'string',
            ipv4Subnet: [
              'string',
              'string'
            ],
            ipv6Subnet: [
              'string',
              'string'
            ],
            lowerPort: 10,
            upperPort: 10
          }
        ],
        scalableGroupType: 'string',
        type: 'string'
      }
    ];
    describe('#createApplicationV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplicationV2(applicationPolicyCreateApplicationV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createApplicationV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosDeviceInterfaceInfoCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getQosDeviceInterfaceInfoCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getQosDeviceInterfaceInfoCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetCountV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationSetCountV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationSetCountV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteApplicationV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyPostApplicationPolicyIntentBodyParam = {
      createList: [
        {
          name: 'string',
          deletePolicyStatus: 'RESTORED',
          policyScope: 'string',
          priority: 'string',
          advancedPolicyScope: {
            name: 'string',
            advancedPolicyScopeElement: [
              {
                groupId: [
                  'string'
                ],
                ssid: [
                  'string'
                ]
              }
            ]
          },
          exclusiveContract: {
            clause: [
              {
                type: 'APPLICATION_POLICY_KNOBS',
                relevanceLevel: 'BUSINESS_RELEVANT',
                deviceRemovalBehavior: 'RESTORE',
                hostTrackingEnabled: true
              }
            ]
          },
          contract: {
            idRef: 'string'
          },
          producer: {
            scalableGroup: [
              {
                idRef: 'string'
              }
            ]
          },
          consumer: {
            scalableGroup: [
              {
                idRef: 'string'
              }
            ]
          }
        }
      ],
      updateList: [
        {
          id: 'string',
          name: 'string',
          deletePolicyStatus: 'NONE',
          policyScope: 'string',
          priority: 'string',
          advancedPolicyScope: {
            id: 'string',
            name: 'string',
            advancedPolicyScopeElement: [
              {
                id: 'string',
                groupId: [
                  'string'
                ],
                ssid: [
                  'string'
                ]
              }
            ]
          },
          exclusiveContract: {
            id: 'string',
            clause: [
              {
                id: 'string',
                type: 'BUSINESS_RELEVANCE',
                relevanceLevel: 'BUSINESS_RELEVANT',
                deviceRemovalBehavior: 'DELETE',
                hostTrackingEnabled: true
              }
            ]
          },
          contract: {
            idRef: 'string'
          },
          producer: {
            id: 'string',
            scalableGroup: [
              {
                idRef: 'string'
              }
            ]
          },
          consumer: {
            id: 'string',
            scalableGroup: [
              {
                idRef: 'string'
              }
            ]
          }
        }
      ],
      deleteList: [
        'string'
      ]
    };
    describe('#postApplicationPolicyIntent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postApplicationPolicyIntent(applicationPolicyPostApplicationPolicyIntentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'postApplicationPolicyIntent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationSetV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationSetV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteApplicationSetV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCountV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationCountV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationCountV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyQueuingProfileCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationPolicyQueuingProfileCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationPolicyQueuingProfileCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationsCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationsCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationsCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplications(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyEditApplicationBodyParam = [
      {
        id: 'string',
        name: 'string',
        networkApplications: [
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            id: 'string',
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          }
        ],
        networkIdentity: [
          {
            id: 'string',
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            id: 'string',
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            id: 'string',
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          }
        ],
        applicationSet: {
          idRef: 'string'
        }
      }
    ];
    describe('#editApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editApplication(applicationPolicyEditApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'editApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateApplicationBodyParam = [
      {
        name: 'string',
        networkApplications: [
          {
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          },
          {
            appProtocol: 'string',
            applicationSubType: 'string',
            applicationType: 'string',
            categoryId: 'string',
            displayName: 'string',
            engineId: 'string',
            helpString: 'string',
            longDescription: 'string',
            name: 'string',
            popularity: 'string',
            rank: 'string',
            trafficClass: 'string',
            serverName: 'string',
            url: 'string',
            dscp: 'string',
            ignoreConflict: 'string'
          }
        ],
        networkIdentity: [
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          },
          {
            displayName: 'string',
            lowerPort: 'string',
            ports: 'string',
            protocol: 'string',
            upperPort: 'string'
          }
        ],
        applicationSet: {
          idRef: 'string'
        }
      }
    ];
    describe('#createApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplication(applicationPolicyCreateApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplication('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationSets(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationSets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPolicyCreateApplicationSetBodyParam = [
      {
        name: 'string'
      }
    ];
    describe('#createApplicationSet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplicationSet(applicationPolicyCreateApplicationSetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'createApplicationSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationSet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationSet('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'deleteApplicationSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetsCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationSetsCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationPolicy', 'getApplicationSetsCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAConfigureAccessPointsBodyParam = {
      apList: [
        {
          apName: 'string',
          macAddress: 'string',
          apNameNew: 'string'
        }
      ],
      configureAdminStatus: true,
      adminStatus: true,
      configureApMode: true,
      apMode: 5,
      configureFailoverPriority: false,
      failoverPriority: 5,
      configureLedStatus: true,
      ledStatus: true,
      configureLedBrightnessLevel: false,
      ledBrightnessLevel: 10,
      configureLocation: true,
      location: 'string',
      configureHAController: false,
      primaryControllerName: 'string',
      primaryIpAddress: {
        address: 'string'
      },
      secondaryControllerName: 'string',
      secondaryIpAddress: {
        address: 'string'
      },
      tertiaryControllerName: 'string',
      tertiaryIpAddress: {
        address: 'string'
      },
      radioConfigurations: [
        {
          configureRadioRoleAssignment: false,
          radioRoleAssignment: 'auto',
          radioBand: 'RADIO5',
          configureAdminStatus: true,
          adminStatus: true,
          configureAntennaPatternName: true,
          antennaPatternName: 'string',
          antennaGain: 2,
          configureAntennaCable: true,
          antennaCableName: 'string',
          cableLoss: 8,
          configureChannel: false,
          channelAssignmentMode: 10,
          channelNumber: 10,
          configureChannelWidth: false,
          channelWidth: 2,
          configurePower: true,
          powerAssignmentMode: 10,
          powerlevel: 7,
          radioType: 4
        }
      ],
      configureCleanAirSI24Ghz: true,
      cleanAirSI24: true,
      configureCleanAirSI5Ghz: true,
      cleanAirSI5: true,
      configureCleanAirSI6Ghz: false,
      cleanAirSI6: true,
      isAssignedSiteAsLocation: true
    };
    describe('#configureAccessPoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configureAccessPoints(nonFabricWirelessBETAConfigureAccessPointsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'configureAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETARebootAccessPointsBodyParam = {
      apMacAddresses: [
        'string'
      ]
    };
    describe('#rebootAccessPoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rebootAccessPoints(nonFabricWirelessBETARebootAccessPointsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'rebootAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccessPointConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getAccessPointConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointRebootTaskResult - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccessPointRebootTaskResult(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getAccessPointRebootTaskResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointConfigurationTaskResult - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccessPointConfigurationTaskResult('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getAccessPointConfigurationTaskResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveRFProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveRFProfiles(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'retrieveRFProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETACreateOrUpdateRFProfileBodyParam = {
      name: 'string',
      defaultRfProfile: false,
      enableRadioTypeA: true,
      enableRadioTypeB: true,
      channelWidth: 'string',
      enableCustom: true,
      enableBrownField: false,
      radioTypeAProperties: {
        parentProfile: 'string',
        radioChannels: 'string',
        dataRates: 'string',
        mandatoryDataRates: 'string',
        powerThresholdV1: 1,
        rxSopThreshold: 'string',
        minPowerLevel: 8,
        maxPowerLevel: 5
      },
      radioTypeBProperties: {
        parentProfile: 'string',
        radioChannels: 'string',
        dataRates: 'string',
        mandatoryDataRates: 'string',
        powerThresholdV1: 2,
        rxSopThreshold: 'string',
        minPowerLevel: 8,
        maxPowerLevel: 9
      },
      radioTypeCProperties: {
        parentProfile: 'string',
        radioChannels: 'string',
        dataRates: 'string',
        mandatoryDataRates: 'string',
        rxSopThreshold: 'string',
        minPowerLevel: 8,
        maxPowerLevel: 3,
        powerThresholdV1: 9
      },
      enableRadioTypeC: false
    };
    describe('#createOrUpdateRFProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrUpdateRFProfile(nonFabricWirelessBETACreateOrUpdateRFProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'createOrUpdateRFProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRFProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRFProfiles('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'deleteRFProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAPSKOverrideBodyParam = [
      {
        ssid: 'string',
        site: 'string',
        passPhrase: 'string',
        wlanProfileName: 'string'
      }
    ];
    describe('#pSKOverride - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pSKOverride(nonFabricWirelessBETAPSKOverrideBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'pSKOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWirelessProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWirelessProfile(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getWirelessProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAUpdateWirelessProfileBodyParam = {
      profileDetails: {
        name: 'string',
        sites: [
          'string'
        ],
        ssidDetails: [
          {
            name: 'string',
            enableFabric: true,
            flexConnect: {
              enableFlexConnect: true,
              localToVlan: 7
            },
            interfaceName: 'string',
            wlanProfileName: 'string',
            policyProfileName: 'string'
          }
        ]
      }
    };
    describe('#updateWirelessProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateWirelessProfile(nonFabricWirelessBETAUpdateWirelessProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'updateWirelessProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETACreateWirelessProfileBodyParam = {
      profileDetails: {
        name: 'string',
        sites: [
          'string'
        ],
        ssidDetails: [
          {
            name: 'string',
            enableFabric: false,
            flexConnect: {
              enableFlexConnect: false,
              localToVlan: 5
            },
            interfaceName: 'string',
            wlanProfileName: 'string',
            policyProfileName: 'string'
          }
        ]
      }
    };
    describe('#createWirelessProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createWirelessProfile(nonFabricWirelessBETACreateWirelessProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'createWirelessProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAProvisionUpdateBodyParam = [
      {
        deviceName: 'string',
        managedAPLocations: [
          'string',
          'string',
          'string'
        ],
        dynamicInterfaces: [
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 10,
            interfaceGateway: 'string',
            lagOrPortNumber: 9,
            vlanId: 3,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 4,
            interfaceGateway: 'string',
            lagOrPortNumber: 8,
            vlanId: 2,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 8,
            interfaceGateway: 'string',
            lagOrPortNumber: 6,
            vlanId: 10,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 2,
            interfaceGateway: 'string',
            lagOrPortNumber: 8,
            vlanId: 10,
            interfaceName: 'string'
          }
        ]
      }
    ];
    describe('#provisionUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.provisionUpdate(nonFabricWirelessBETAProvisionUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'provisionUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAProvisionBodyParam = [
      {
        deviceName: 'string',
        site: 'string',
        managedAPLocations: [
          'string',
          'string',
          'string',
          'string',
          'string'
        ],
        dynamicInterfaces: [
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 6,
            interfaceGateway: 'string',
            lagOrPortNumber: 3,
            vlanId: 5,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 4,
            interfaceGateway: 'string',
            lagOrPortNumber: 6,
            vlanId: 4,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 5,
            interfaceGateway: 'string',
            lagOrPortNumber: 10,
            vlanId: 2,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 9,
            interfaceGateway: 'string',
            lagOrPortNumber: 5,
            vlanId: 10,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 4,
            interfaceGateway: 'string',
            lagOrPortNumber: 8,
            vlanId: 5,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 4,
            interfaceGateway: 'string',
            lagOrPortNumber: 3,
            vlanId: 7,
            interfaceName: 'string'
          },
          {
            interfaceIPAddress: 'string',
            interfaceNetmaskInCIDR: 1,
            interfaceGateway: 'string',
            lagOrPortNumber: 6,
            vlanId: 9,
            interfaceName: 'string'
          }
        ]
      }
    ];
    describe('#provision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.provision(nonFabricWirelessBETAProvisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'provision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensorTestResults - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSensorTestResults(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getSensorTestResults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAUpdateEnterpriseSSIDBodyParam = {
      name: 'string',
      securityLevel: 'WPA2_ENTERPRISE',
      passphrase: 'string',
      enableFastLane: true,
      enableMACFiltering: true,
      trafficType: 'data',
      radioPolicy: '5GHz only',
      enableBroadcastSSID: true,
      fastTransition: 'Adaptive',
      enableSessionTimeOut: true,
      sessionTimeOut: 10,
      enableClientExclusion: true,
      clientExclusionTimeout: 4,
      enableBasicServiceSetMaxIdle: false,
      basicServiceSetClientIdleTimeout: 4,
      enableDirectedMulticastService: false,
      enableNeighborList: true,
      mfpClientProtection: 'Disabled',
      nasOptions: [
        'string'
      ],
      profileName: 'string',
      policyProfileName: 'string',
      aaaOverride: true,
      coverageHoleDetectionEnable: true,
      protectedManagementFrame: 'Disabled',
      multiPSKSettings: [
        {
          priority: 8,
          passphraseType: 'HEX',
          passphrase: 'string'
        }
      ],
      clientRateLimit: 5
    };
    describe('#updateEnterpriseSSID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateEnterpriseSSID(nonFabricWirelessBETAUpdateEnterpriseSSIDBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'updateEnterpriseSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDynamicInterface(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'getDynamicInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETACreateUpdateDynamicInterfaceBodyParam = {
      interfaceName: 'string',
      vlanId: 10
    };
    describe('#createUpdateDynamicInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUpdateDynamicInterface(nonFabricWirelessBETACreateUpdateDynamicInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'createUpdateDynamicInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETAAPProvisionBodyParam = [
      {
        rfProfile: 'string',
        deviceName: 'string',
        customApGroupName: 'string',
        customFlexGroupName: [
          'string',
          'string',
          'string'
        ],
        type: 'string',
        siteNameHierarchy: 'string'
      }
    ];
    describe('#aPProvision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.aPProvision(nonFabricWirelessBETAAPProvisionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'aPProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nonFabricWirelessBETADeleteWirelessProfileBodyParam = {};
    describe('#deleteWirelessProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWirelessProfile('fakedata', nonFabricWirelessBETADeleteWirelessProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'deleteWirelessProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDynamicInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDynamicInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NonFabricWirelessBETA', 'deleteDynamicInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createACloneOfTheGivenTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createACloneOfTheGivenTemplate('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'createACloneOfTheGivenTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateProgrammerimportTheTemplatesProvidedBodyParam = {
      author: 'string',
      composite: true,
      containingTemplates: [
        {
          composite: true,
          description: 'string',
          deviceTypes: [
            {
              productFamily: 'string',
              productSeries: 'string',
              productType: 'string'
            }
          ],
          id: 'string',
          language: 'string',
          name: 'string',
          projectName: 'string',
          rollbackTemplateParams: [
            {
              binding: 'string',
              customOrder: 5,
              dataType: 'INTEGER',
              defaultValue: 'string',
              description: 'string',
              displayName: 'string',
              group: 'string',
              id: 'string',
              instructionText: 'string',
              key: 'string',
              notParam: true,
              order: 8,
              paramArray: false,
              parameterName: 'string',
              provider: 'string',
              range: [
                {
                  id: 'string',
                  maxValue: 10,
                  minValue: 8
                }
              ],
              required: true,
              selection: {
                defaultSelectedValues: [
                  'string'
                ],
                id: 'string',
                selectionType: 'MULTI_SELECT',
                selectionValues: {}
              }
            }
          ],
          tags: [
            {
              id: 'string',
              name: 'string'
            }
          ],
          templateContent: 'string',
          templateParams: [
            {
              binding: 'string',
              customOrder: 1,
              dataType: 'IPADDRESS',
              defaultValue: 'string',
              description: 'string',
              displayName: 'string',
              group: 'string',
              id: 'string',
              instructionText: 'string',
              key: 'string',
              notParam: true,
              order: 6,
              paramArray: true,
              parameterName: 'string',
              provider: 'string',
              range: [
                {
                  id: 'string',
                  maxValue: 9,
                  minValue: 10
                }
              ],
              required: false,
              selection: {
                defaultSelectedValues: [
                  'string'
                ],
                id: 'string',
                selectionType: 'MULTI_SELECT',
                selectionValues: {}
              }
            }
          ],
          version: 'string'
        }
      ],
      createTime: 6,
      customParamsOrder: false,
      description: 'string',
      deviceTypes: [
        {
          productFamily: 'string',
          productSeries: 'string',
          productType: 'string'
        }
      ],
      failurePolicy: 'ROLLBACK_ON_ERROR',
      id: 'string',
      language: 'string',
      lastUpdateTime: 5,
      latestVersionTime: 3,
      name: 'string',
      parentTemplateId: 'string',
      projectAssociated: true,
      projectId: 'string',
      projectName: 'string',
      rollbackTemplateContent: 'string',
      rollbackTemplateParams: [
        {
          binding: 'string',
          customOrder: 10,
          dataType: 'INTEGER',
          defaultValue: 'string',
          description: 'string',
          displayName: 'string',
          group: 'string',
          id: 'string',
          instructionText: 'string',
          key: 'string',
          notParam: false,
          order: 2,
          paramArray: false,
          parameterName: 'string',
          provider: 'string',
          range: [
            {
              id: 'string',
              maxValue: 9,
              minValue: 1
            }
          ],
          required: false,
          selection: {
            defaultSelectedValues: [
              'string'
            ],
            id: 'string',
            selectionType: 'MULTI_SELECT',
            selectionValues: {}
          }
        }
      ],
      softwareType: 'string',
      softwareVariant: 'string',
      softwareVersion: 'string',
      tags: [
        {
          id: 'string',
          name: 'string'
        }
      ],
      templateContent: 'string',
      templateParams: [
        {
          binding: 'string',
          customOrder: 5,
          dataType: 'SECTIONDIVIDER',
          defaultValue: 'string',
          description: 'string',
          displayName: 'string',
          group: 'string',
          id: 'string',
          instructionText: 'string',
          key: 'string',
          notParam: false,
          order: 1,
          paramArray: true,
          parameterName: 'string',
          provider: 'string',
          range: [
            {
              id: 'string',
              maxValue: 8,
              minValue: 3
            }
          ],
          required: false,
          selection: {
            defaultSelectedValues: [
              'string'
            ],
            id: 'string',
            selectionType: 'SINGLE_SELECT',
            selectionValues: {}
          }
        }
      ],
      validationErrors: {
        rollbackTemplateErrors: {},
        templateErrors: {},
        templateId: 'string',
        templateVersion: 'string'
      },
      version: 'string',
      versionsInfo: [
        {
          author: 'string',
          description: 'string',
          id: 'string',
          version: 'string',
          versionComment: 'string',
          versionTime: 1
        }
      ]
    };
    describe('#importTheTemplatesProvided - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importTheTemplatesProvided('fakedata', null, templateProgrammerimportTheTemplatesProvidedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'importTheTemplatesProvided', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateProgrammerexportTheProjectsForAGivenCriteriaBodyParam = [
      {}
    ];
    describe('#exportTheProjectsForAGivenCriteria - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportTheProjectsForAGivenCriteria(templateProgrammerexportTheProjectsForAGivenCriteriaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'exportTheProjectsForAGivenCriteria', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheDetailsOfAGivenProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTheDetailsOfAGivenProject('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'getTheDetailsOfAGivenProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectDetailsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProjectDetailsV2(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'getProjectDetailsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDetailsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateDetailsV2(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'getTemplateDetailsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateProgrammerexportTheTemplatesForAGivenCriteriaBodyParam = [
      {}
    ];
    describe('#exportTheTemplatesForAGivenCriteria - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportTheTemplatesForAGivenCriteria(templateProgrammerexportTheTemplatesForAGivenCriteriaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'exportTheTemplatesForAGivenCriteria', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateProgrammerimportTheProjectsProvidedBodyParam = {};
    describe('#importTheProjectsProvided - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importTheProjectsProvided(null, templateProgrammerimportTheProjectsProvidedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateProgrammer', 'importTheProjectsProvided', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewsForAGivenViewGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getViewsForAGivenViewGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getViewsForAGivenViewGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewDetailsForAGivenViewGroupView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getViewDetailsForAGivenViewGroupView('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getViewDetailsForAGivenViewGroupView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAScheduledReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAScheduledReport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAScheduledReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAScheduledReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAScheduledReport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'deleteAScheduledReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfScheduledReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getListOfScheduledReports(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getListOfScheduledReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsCreateOrScheduleAReportBodyParam = {
      tags: [
        'string'
      ],
      deliveries: [
        {}
      ],
      name: 'string',
      schedule: {},
      view: {
        fieldGroups: [
          {
            fieldGroupDisplayName: 'string',
            fieldGroupName: 'string',
            fields: [
              {
                displayName: 'string',
                name: 'string'
              }
            ]
          }
        ],
        filters: [
          {
            displayName: 'string',
            name: 'string',
            type: 'string',
            value: {}
          }
        ],
        format: {
          formatType: 'string',
          name: 'string'
        },
        name: 'string',
        viewId: 'string'
      },
      viewGroupId: 'string',
      viewGroupVersion: 'string',
      dataCategory: 'string'
    };
    describe('#createOrScheduleAReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrScheduleAReport(reportsCreateOrScheduleAReportBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'createOrScheduleAReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllViewGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllViewGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllViewGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExecutionDetailsForAGivenReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllExecutionDetailsForAGivenReport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllExecutionDetailsForAGivenReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReportContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadReportContent('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'downloadReportContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChassisDetailsForDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getChassisDetailsForDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getChassisDetailsForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUserDefinedFields - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllUserDefinedFields(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllUserDefinedFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateUserDefinedFieldBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#createUserDefinedField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUserDefinedField(devicesCreateUserDefinedFieldBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createUserDefinedField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheDetailsOfPhysicalComponentsOfTheGivenDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTheDetailsOfPhysicalComponentsOfTheGivenDevice('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getTheDetailsOfPhysicalComponentsOfTheGivenDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnPOEInterfaceDetailsForTheDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.returnPOEInterfaceDetailsForTheDevice('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'returnPOEInterfaceDetailsForTheDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesClearMacAddressTableBodyParam = {
      operation: 'string',
      payload: {}
    };
    describe('#clearMacAddressTable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearMacAddressTable('fakedata', null, devicesClearMacAddressTableBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'clearMacAddressTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlannedAccessPointsForFloor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlannedAccessPointsForFloor('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getPlannedAccessPointsForFloor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdatePlannedAccessPointForFloorBodyParam = {
      attributes: {
        createDate: 7,
        domain: 'string',
        heirarchyName: 'string',
        id: 10,
        instanceUuid: 'string',
        macaddress: 'string',
        name: 'string',
        source: 'UNKNOWN',
        typeString: 'string'
      },
      isSensor: false,
      location: {
        altitude: 4,
        lattitude: 2,
        longtitude: 5
      },
      position: {
        x: 3,
        y: 10,
        z: 9
      },
      radioCount: 3,
      radios: [
        {
          antenna: {
            azimuthAngle: 9,
            elevationAngle: 3,
            gain: 5,
            mode: 'omni',
            name: 'string',
            type: 'internal'
          },
          attributes: {
            channel: 4,
            channelString: 'string',
            id: 6,
            ifMode: 'XorUnknown',
            ifTypeString: 'string',
            ifTypeSubband: 'Unknown',
            instanceUuid: 'string',
            slotId: 7
          },
          isSensor: true
        }
      ]
    };
    describe('#updatePlannedAccessPointForFloor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePlannedAccessPointForFloor('fakedata', devicesUpdatePlannedAccessPointForFloorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updatePlannedAccessPointForFloor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreatePlannedAccessPointForFloorBodyParam = {
      attributes: {
        createDate: 9,
        domain: 'string',
        heirarchyName: 'string',
        id: 1,
        instanceUuid: 'string',
        macaddress: 'string',
        name: 'string',
        source: 'EKAHAU',
        typeString: 'string'
      },
      isSensor: true,
      location: {
        altitude: 2,
        lattitude: 6,
        longtitude: 5
      },
      position: {
        x: 3,
        y: 8,
        z: 8
      },
      radioCount: 2,
      radios: [
        {
          antenna: {
            azimuthAngle: 6,
            elevationAngle: 2,
            gain: 8,
            mode: 'sector_a',
            name: 'string',
            type: 'linear'
          },
          attributes: {
            channel: 10,
            channelString: 'string',
            id: 5,
            ifMode: 'Monitor',
            ifTypeString: 'string',
            ifTypeSubband: 'Unknown',
            instanceUuid: 'string',
            slotId: 6
          },
          isSensor: true
        }
      ]
    };
    describe('#createPlannedAccessPointForFloor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPlannedAccessPointForFloor('fakedata', devicesCreatePlannedAccessPointForFloorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createPlannedAccessPointForFloor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceHealth(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinecardDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLinecardDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getLinecardDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInventoryInsightDeviceLinkMismatchAPI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInventoryInsightDeviceLinkMismatchAPI('fakedata', null, null, 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getInventoryInsightDeviceLinkMismatchAPI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePlannedAccessPointForFloor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePlannedAccessPointForFloor('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deletePlannedAccessPointForFloor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStackDetailsForDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getStackDetailsForDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getStackDetailsForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesGetDeviceInterfaceStatsInfoV2BodyParam = {
      startTime: 10,
      endTime: 8,
      query: {
        fields: [
          {}
        ],
        filters: [
          {
            key: 'string',
            operator: 'string',
            value: 'string'
          }
        ],
        page: {
          limit: 1,
          offset: 9,
          orderBy: [
            {
              name: 'string',
              order: 'string'
            }
          ]
        }
      }
    };
    describe('#getDeviceInterfaceStatsInfoV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceInterfaceStatsInfoV2('fakedata', devicesGetDeviceInterfaceStatsInfoV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceInterfaceStatsInfoV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateUserDefinedFieldBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#updateUserDefinedField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserDefinedField('fakedata', devicesUpdateUserDefinedFieldBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateUserDefinedField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserDefinedField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserDefinedField('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteUserDefinedField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateInterfaceDetailsBodyParam = {
      description: 'string',
      adminStatus: 'string',
      vlanId: 3,
      voiceVlanId: 3
    };
    describe('#updateInterfaceDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInterfaceDetails('fakedata', null, devicesUpdateInterfaceDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateInterfaceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLegitOperationsForInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLegitOperationsForInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getLegitOperationsForInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSupervisorCardDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSupervisorCardDetail('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getSupervisorCardDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesAddUserDefinedFieldToDeviceBodyParam = [
      {
        name: 'string',
        value: 'string'
      }
    ];
    describe('#addUserDefinedFieldToDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserDefinedFieldToDevice('fakedata', devicesAddUserDefinedFieldToDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'addUserDefinedFieldToDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserDefinedFieldFromDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserDefinedFieldFromDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'removeUserDefinedFieldFromDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectedDeviceDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConnectedDeviceDetail('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getConnectedDeviceDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDeviceManagementAddressBodyParam = {
      newIP: 'string'
    };
    describe('#updateDeviceManagementAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceManagementAddress('fakedata', devicesUpdateDeviceManagementAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDeviceManagementAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlannedAccessPointsForBuilding - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlannedAccessPointsForBuilding('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getPlannedAccessPointsForBuilding', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceEnrichmentDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceEnrichmentDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceEnrichmentDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGlobalCredentialsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllGlobalCredentialsV2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDiscovery', 'getAllGlobalCredentialsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkDiscoveryUpdateGlobalCredentialsV2BodyParam = {
      cliCredential: {
        description: 'string',
        username: 'string',
        password: 'string',
        enablePassword: 'string',
        id: 'string'
      },
      snmpV2cRead: {
        description: 'string',
        readCommunity: 'string',
        id: 'string'
      },
      snmpV2cWrite: {
        description: 'string',
        writeCommunity: 'string',
        id: 'string'
      },
      snmpV3: {
        authPassword: 'string',
        authType: 'string',
        snmpMode: 'string',
        privacyPassword: 'string',
        privacyType: 'string',
        username: 'string',
        description: 'string',
        id: 'string'
      },
      httpsRead: {
        name: 'string',
        username: 'string',
        password: 'string',
        port: 1,
        id: 'string'
      },
      httpsWrite: {
        name: 'string',
        username: 'string',
        password: 'string',
        port: 9,
        id: 'string'
      }
    };
    describe('#updateGlobalCredentialsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGlobalCredentialsV2(networkDiscoveryUpdateGlobalCredentialsV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDiscovery', 'updateGlobalCredentialsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkDiscoveryCreateGlobalCredentialsV2BodyParam = {
      cliCredential: [
        {
          description: 'string',
          username: 'string',
          password: 'string',
          enablePassword: 'string'
        }
      ],
      snmpV2cRead: [
        {
          description: 'string',
          readCommunity: 'string'
        }
      ],
      snmpV2cWrite: [
        {
          description: 'string',
          writeCommunity: 'string'
        }
      ],
      snmpV3: [
        {
          description: 'string',
          username: 'string',
          privacyType: 'string',
          privacyPassword: 'string',
          authType: 'string',
          authPassword: 'string',
          snmpMode: 'string'
        }
      ],
      httpsRead: [
        {
          name: 'string',
          username: 'string',
          password: 'string',
          port: 4
        }
      ],
      httpsWrite: [
        {
          name: 'string',
          username: 'string',
          password: 'string',
          port: 9
        }
      ]
    };
    describe('#createGlobalCredentialsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createGlobalCredentialsV2(networkDiscoveryCreateGlobalCredentialsV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDiscovery', 'createGlobalCredentialsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalCredentialV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGlobalCredentialV2('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDiscovery', 'deleteGlobalCredentialV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhookDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWebhookDestination(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getWebhookDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateWebhookDestinationBodyParam = {
      webhookId: 'string',
      name: 'string',
      description: 'string',
      url: 'string',
      method: 'PUT',
      trustCert: true,
      headers: [
        {
          name: 'string',
          value: 'string',
          defaultValue: 'string',
          encrypt: false
        }
      ],
      isProxyRoute: true
    };
    describe('#updateWebhookDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateWebhookDestination(eventManagementUpdateWebhookDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateWebhookDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateWebhookDestinationBodyParam = {
      webhookId: 'string',
      name: 'string',
      description: 'string',
      url: 'string',
      method: 'POST',
      trustCert: false,
      headers: [
        {
          name: 'string',
          value: 'string',
          defaultValue: 'string',
          encrypt: false
        }
      ],
      isProxyRoute: true
    };
    describe('#createWebhookDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createWebhookDestination(eventManagementCreateWebhookDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createWebhookDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSyslogDestination(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getSyslogDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateSyslogDestinationBodyParam = {
      configId: 'string',
      name: 'string',
      description: 'string',
      host: 'string',
      protocol: 'UDP',
      port: 'string'
    };
    describe('#updateSyslogDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSyslogDestination(eventManagementUpdateSyslogDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateSyslogDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateSyslogDestinationBodyParam = {
      configId: 'string',
      name: 'string',
      description: 'string',
      host: 'string',
      protocol: 'UDP',
      port: 'string'
    };
    describe('#createSyslogDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSyslogDestination(eventManagementCreateSyslogDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createSyslogDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateSNMPDestinationBodyParam = {
      errorMessage: {
        errors: [
          'string'
        ]
      },
      apiStatus: 'string',
      statusMessage: 'string'
    };
    describe('#updateSNMPDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSNMPDestination(eventManagementUpdateSNMPDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateSNMPDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateSNMPDestinationBodyParam = {
      name: 'string',
      description: 'string',
      ipAddress: 'string',
      port: 'string',
      snmpVersion: 'V2C',
      community: 'string',
      userName: 'string',
      snmpMode: 'NONE',
      snmpAuthType: 'NONE',
      authPassword: 'string',
      snmpPrivacyType: 'AES128',
      privacyPassword: 'string'
    };
    describe('#createSNMPDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSNMPDestination(eventManagementCreateSNMPDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createSNMPDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfNotifications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCountOfNotifications(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getCountOfNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCountOfEventSubscriptions('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getCountOfEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogSubscriptionDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSyslogSubscriptionDetails(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getSyslogSubscriptionDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailSubscriptionDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEmailSubscriptionDetails(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEmailSubscriptionDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEmailEventSubscriptions(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEmailEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateEmailEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string',
            'string',
            'string'
          ],
          severities: [
            10,
            3
          ],
          sources: [
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#updateEmailEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateEmailEventSubscription(eventManagementUpdateEmailEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateEmailEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateEmailEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string',
              fromEmailAddress: 'string',
              toEmailAddresses: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              subject: 'string',
              name: 'string',
              description: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string'
          ],
          categories: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          severities: [
            4,
            10,
            6,
            5,
            4,
            4,
            8
          ],
          sources: [
            'string'
          ],
          siteIds: [
            'string',
            'string',
            'string',
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#createEmailEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createEmailEventSubscription(eventManagementCreateEmailEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createEmailEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEvents(null, 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogSummary(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getAuditLogSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEventSubscriptions(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateEventSubscriptionsBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string'
          ],
          severities: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          sources: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#updateEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateEventSubscriptions(eventManagementUpdateEventSubscriptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateEventSubscriptionsBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          severities: [
            'string',
            'string',
            'string'
          ],
          sources: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string',
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#createEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createEventSubscriptions(eventManagementCreateEventSubscriptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEventSubscriptions('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'deleteEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSyslogEventSubscriptions(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getSyslogEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateSyslogEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string'
          ],
          severities: [
            'string'
          ],
          sources: [
            'string'
          ],
          siteIds: [
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#updateSyslogEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSyslogEventSubscription(eventManagementUpdateSyslogEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateSyslogEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateSyslogEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string'
          ],
          categories: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          severities: [
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          sources: [
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#createSyslogEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSyslogEventSubscription(eventManagementCreateSyslogEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createSyslogEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCountOfEvents(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getCountOfEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventArtifacts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEventArtifacts(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEventArtifacts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEmailDestination((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEmailDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateEmailDestinationBodyParam = {
      emailConfigId: 'string',
      primarySMTPConfig: {
        hostName: 'string',
        port: 'string',
        userName: 'string',
        password: 'string'
      },
      secondarySMTPConfig: {
        hostName: 'string',
        port: 'string',
        userName: 'string',
        password: 'string'
      },
      fromEmail: 'string',
      toEmail: 'string',
      subject: 'string'
    };
    describe('#updateEmailDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateEmailDestination(eventManagementUpdateEmailDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateEmailDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateEmailDestinationBodyParam = {
      emailConfigId: 'string',
      primarySMTPConfig: {
        hostName: 'string',
        port: 'string',
        userName: 'string',
        password: 'string'
      },
      secondarySMTPConfig: {
        hostName: 'string',
        port: 'string',
        userName: 'string',
        password: 'string'
      },
      fromEmail: 'string',
      toEmail: 'string',
      subject: 'string'
    };
    describe('#createEmailDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createEmailDestination(eventManagementCreateEmailDestinationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createEmailDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotifications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNotifications(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogRecords(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getAuditLogRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestWebhookEventSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestWebhookEventSubscriptions(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getRestWebhookEventSubscriptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementUpdateRestWebhookEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string',
            'string'
          ],
          severities: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          sources: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#updateRestWebhookEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRestWebhookEventSubscription(eventManagementUpdateRestWebhookEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'updateRestWebhookEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventManagementCreateRestWebhookEventSubscriptionBodyParam = [
      {
        subscriptionId: 'string',
        version: 'string',
        name: 'string',
        description: 'string',
        subscriptionEndpoints: [
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          },
          {
            instanceId: 'string',
            subscriptionDetails: {
              connectorType: 'string'
            }
          }
        ],
        filter: {
          eventIds: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          domainsSubdomains: [
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string'
              ]
            },
            {
              domain: 'string',
              subDomains: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ]
            }
          ],
          types: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          categories: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          severities: [
            'string',
            'string',
            'string'
          ],
          sources: [
            'string',
            'string',
            'string'
          ],
          siteIds: [
            'string',
            'string'
          ]
        }
      }
    ];
    describe('#createRestWebhookEventSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRestWebhookEventSubscription(eventManagementCreateRestWebhookEventSubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'createRestWebhookEventSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogParentRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogParentRecords(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getAuditLogParentRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventArtifactCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEventArtifactCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getEventArtifactCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConnectorTypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getConnectorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusAPIForEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getStatusAPIForEvents('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getStatusAPIForEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestWebhookSubscriptionDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestWebhookSubscriptionDetails(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getRestWebhookSubscriptionDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSNMPDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSNMPDestination(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventManagement', 'getSNMPDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lANAutomationLANAutomationStopAndUpdateDevicesBodyParam = [
      {
        deviceManagementIPAddress: 'string',
        newLoopback0IPAddress: 'string'
      }
    ];
    describe('#lANAutomationStopAndUpdateDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lANAutomationStopAndUpdateDevices('fakedata', lANAutomationLANAutomationStopAndUpdateDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'lANAutomationStopAndUpdateDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lANAutomationLANAutomationStopBodyParam = {};
    describe('#lANAutomationStop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lANAutomationStop('fakedata', lANAutomationLANAutomationStopBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'lANAutomationStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lANAutomationLANAutomationDeviceUpdateBodyParam = {
      loopbackUpdateDeviceList: [
        {
          deviceManagementIPAddress: 'string',
          newLoopback0IPAddress: 'string'
        }
      ],
      linkUpdate: {
        sourceDeviceManagementIPAddress: 'string',
        sourceDeviceInterfaceName: 'string',
        destinationDeviceManagementIPAddress: 'string',
        destinationDeviceInterfaceName: 'string',
        ipPoolName: 'string'
      }
    };
    describe('#lANAutomationDeviceUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lANAutomationDeviceUpdate('fakedata', lANAutomationLANAutomationDeviceUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'lANAutomationDeviceUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLogById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationLogById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationLogById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationStatusById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationStatusById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationStatusById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationLog(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lANAutomationLANAutomationStartBodyParam = [
      {
        discoveredDeviceSiteNameHierarchy: 'string',
        primaryDeviceManagmentIPAddress: 'string',
        peerDeviceManagmentIPAddress: 'string',
        primaryDeviceInterfaceNames: [
          'string',
          'string',
          'string'
        ],
        ipPools: [
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          },
          {
            ipPoolName: 'string',
            ipPoolRole: 'string'
          }
        ],
        mulitcastEnabled: true,
        hostNamePrefix: 'string',
        hostNameFileId: 'string',
        isisDomainPwd: 'string',
        redistributeIsisToBgp: true
      }
    ];
    describe('#lANAutomationStart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lANAutomationStart(lANAutomationLANAutomationStartBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'lANAutomationStart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationStatus(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationSessionCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationSessionCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationSessionCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLogsForIndividualDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationLogsForIndividualDevices('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationLogsForIndividualDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationActiveSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLANAutomationActiveSessions((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LANAutomation', 'getLANAutomationActiveSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemSettingsEditAuthenticationAndPolicyServerAccessConfigurationBodyParam = {
      authenticationPort: 'string',
      accountingPort: 'string',
      ciscoIseDtos: [
        {
          description: 'string',
          fqdn: 'string',
          password: 'string',
          sshkey: 'string',
          ipAddress: 'string',
          subscriberName: 'string',
          userName: 'string'
        }
      ],
      ipAddress: 'string',
      pxgridEnabled: false,
      useDnacCertForPxgrid: true,
      isIseEnabled: true,
      port: 'string',
      protocol: 'RADIUS_TACACS',
      retries: 'string',
      role: 'string',
      sharedSecret: samProps.authentication.password,
      timeoutSeconds: 'string',
      encryptionScheme: 'KEYWRAP',
      messageKey: 'string',
      encryptionKey: 'string',
      externalCiscoIseIpAddrDtos: [
        {
          externalCiscoIseIpAddresses: [
            {
              externalIpAddress: 'string'
            }
          ],
          type: 'string'
        }
      ]
    };
    describe('#editAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editAuthenticationAndPolicyServerAccessConfiguration('fakedata', systemSettingsEditAuthenticationAndPolicyServerAccessConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'editAuthenticationAndPolicyServerAccessConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthenticationAndPolicyServerAccessConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'deleteAuthenticationAndPolicyServerAccessConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomPromptSupport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomPromptSupport((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'getCustomPromptSupport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemSettingspostCustomPromptBodyParam = {
      usernamePrompt: 'string',
      passwordPrompt: 'string'
    };
    describe('#postCustomPrompt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCustomPrompt(systemSettingspostCustomPromptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'postCustomPrompt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationAndPolicyServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuthenticationAndPolicyServers(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'getAuthenticationAndPolicyServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemSettingsAddAuthenticationAndPolicyServerAccessConfigurationBodyParam = {
      authenticationPort: 'string',
      accountingPort: 'string',
      ciscoIseDtos: [
        {
          description: 'string',
          fqdn: 'string',
          password: 'string',
          sshkey: 'string',
          ipAddress: 'string',
          subscriberName: 'string',
          userName: 'string'
        }
      ],
      ipAddress: 'string',
      pxgridEnabled: false,
      useDnacCertForPxgrid: false,
      isIseEnabled: false,
      port: 'string',
      protocol: 'RADIUS',
      retries: 'string',
      role: 'string',
      sharedSecret: samProps.authentication.password,
      timeoutSeconds: 'string',
      encryptionScheme: 'KEYWRAP',
      messageKey: 'string',
      encryptionKey: 'string',
      externalCiscoIseIpAddrDtos: [
        {
          externalCiscoIseIpAddresses: [
            {
              externalIpAddress: 'string'
            }
          ],
          type: 'string'
        }
      ]
    };
    describe('#addAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAuthenticationAndPolicyServerAccessConfiguration(systemSettingsAddAuthenticationAndPolicyServerAccessConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'addAuthenticationAndPolicyServerAccessConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoISEServerIntegrationStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoISEServerIntegrationStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'getCiscoISEServerIntegrationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemSettingsAcceptCiscoISEServerCertificateForCiscoISEServerIntegrationBodyParam = {
      isCertAcceptedByUser: true
    };
    describe('#acceptCiscoISEServerCertificateForCiscoISEServerIntegration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.acceptCiscoISEServerCertificateForCiscoISEServerIntegration('fakedata', systemSettingsAcceptCiscoISEServerCertificateForCiscoISEServerIntegrationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'acceptCiscoISEServerCertificateForCiscoISEServerIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMIntegrationCreateITSMIntegrationSettingBodyParam = {
      name: 'string',
      description: 'string',
      data: {
        ConnectionSettings: {
          Url: 'string',
          Auth_UserName: 'string',
          Auth_Password: 'string'
        }
      },
      dypName: 'string'
    };
    describe('#createITSMIntegrationSetting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createITSMIntegrationSetting(iTSMIntegrationCreateITSMIntegrationSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSMIntegration', 'createITSMIntegrationSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMIntegrationGetITSMIntegrationSettingByIdBodyParam = {
      name: 'string',
      description: 'string',
      dypName: 'string',
      dypMajorVersion: 4,
      dypId: 'string',
      dypInstanceId: 'string',
      data: {
        ConnectionSettings: {
          Url: 'string',
          Auth_UserName: 'string',
          Auth_Password: 'string'
        }
      }
    };
    describe('#getITSMIntegrationSettingById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getITSMIntegrationSettingById('fakedata', iTSMIntegrationGetITSMIntegrationSettingByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSMIntegration', 'getITSMIntegrationSettingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMIntegrationUpdateITSMIntegrationSettingBodyParam = {
      name: 'string',
      description: 'string',
      data: {
        ConnectionSettings: {
          Url: 'string',
          Auth_UserName: 'string',
          Auth_Password: 'string'
        }
      },
      dypName: 'string'
    };
    describe('#updateITSMIntegrationSetting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateITSMIntegrationSetting('fakedata', iTSMIntegrationUpdateITSMIntegrationSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSMIntegration', 'updateITSMIntegrationSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteITSMIntegrationSetting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteITSMIntegrationSetting('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSMIntegration', 'deleteITSMIntegrationSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMIntegrationGetAllITSMIntegrationSettingsBodyParam = {
      name: 'string',
      description: 'string',
      dypName: 'string',
      dypMajorVersion: 4,
      dypId: 'string',
      dypInstanceId: 'string',
      data: {
        ConnectionSettings: {
          Url: 'string',
          Auth_UserName: 'string',
          Auth_Password: 'string'
        }
      }
    };
    describe('#getAllITSMIntegrationSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllITSMIntegrationSettings(iTSMIntegrationGetAllITSMIntegrationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSMIntegration', 'getAllITSMIntegrationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceFamilyIdentifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceFamilyIdentifiers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SWIM', 'getDeviceFamilyIdentifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sWIMTagAsGoldenImageBodyParam = {
      imageId: 'string',
      siteId: 'string',
      deviceRole: 'string',
      deviceFamilyIdentifier: 'string'
    };
    describe('#tagAsGoldenImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tagAsGoldenImage(sWIMTagAsGoldenImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SWIM', 'tagAsGoldenImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGoldenTagStatusOfAnImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGoldenTagStatusOfAnImage('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SWIM', 'getGoldenTagStatusOfAnImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGoldenTagForImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeGoldenTagForImage('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SWIM', 'removeGoldenTagForImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsAssignDeviceCredentialToSiteV2BodyParam = {
      cliId: 'string',
      snmpV2ReadId: 'string',
      snmpV2WriteId: 'string',
      snmpV3Id: 'string',
      httpRead: 'string',
      httpWrite: 'string'
    };
    describe('#assignDeviceCredentialToSiteV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignDeviceCredentialToSiteV2('fakedata', networkSettingsAssignDeviceCredentialToSiteV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'assignDeviceCredentialToSiteV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateNetworkV2BodyParam = {
      settings: {
        dhcpServer: [
          'string'
        ],
        dnsServer: {
          domainName: 'can only contain alphanumeric characters or hyphen',
          primaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255',
          secondaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255'
        },
        syslogServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        snmpServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        netflowcollector: {
          ipAddress: 'string',
          port: 3
        },
        ntpServer: [
          'string'
        ],
        timezone: 'string',
        messageOfTheday: {
          bannerMessage: 'string',
          retainExistingBanner: 'string'
        },
        network_aaa: {
          servers: 'Server type supported by ISE and AAA',
          ipAddress: 'Mandatory for ISE servers and for AAA consider this as additional IP.',
          network: 'For AAA server consider it as primary IP and For ISE consider as Network',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        },
        clientAndEndpoint_aaa: {
          servers: 'string',
          ipAddress: 'Mandatory for ISE servers.',
          network: 'string',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        }
      }
    };
    describe('#updateNetworkV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkV2('fakedata', networkSettingsUpdateNetworkV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateNetworkV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsCreateNetworkV2BodyParam = {
      settings: {
        dhcpServer: [
          'string'
        ],
        dnsServer: {
          domainName: 'can only contain alphanumeric characters or hyphen',
          primaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255',
          secondaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255'
        },
        syslogServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        snmpServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        netflowcollector: {
          ipAddress: 'string',
          port: 8
        },
        ntpServer: [
          'string'
        ],
        timezone: 'string',
        messageOfTheday: {
          bannerMessage: 'string',
          retainExistingBanner: 'string'
        },
        network_aaa: {
          servers: 'Server type supported by ISE and AAA',
          ipAddress: 'Mandatory for ISE servers and for AAA consider this as additional IP.',
          network: 'For AAA server consider it as primary IP and For ISE consider as Network',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        },
        clientAndEndpoint_aaa: {
          servers: 'string',
          ipAddress: 'Mandatory for ISE servers.',
          network: 'string',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        }
      }
    };
    describe('#createNetworkV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkV2('fakedata', networkSettingsCreateNetworkV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'createNetworkV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGlobalPool(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'getGlobalPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateGlobalPoolBodyParam = {
      settings: {
        ippool: [
          {
            ipPoolName: 'string',
            gateway: 'string',
            dhcpServerIps: [
              'string'
            ],
            dnsServerIps: [
              'string'
            ],
            id: 'string'
          }
        ]
      }
    };
    describe('#updateGlobalPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGlobalPool(networkSettingsUpdateGlobalPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateGlobalPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsCreateGlobalPoolBodyParam = {
      settings: {
        ippool: [
          {
            ipPoolName: 'string',
            type: 'Tunnel',
            ipPoolCidr: 'string',
            gateway: 'string',
            dhcpServerIps: [
              'string'
            ],
            dnsServerIps: [
              'string'
            ],
            IpAddressSpace: 'IPv6 or IPv4'
          }
        ]
      }
    };
    describe('#createGlobalPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createGlobalPool(networkSettingsCreateGlobalPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'createGlobalPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalIPPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGlobalIPPool('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'deleteGlobalIPPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceCredential - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceCredential('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'deleteDeviceCredential', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetwork(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'getNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateReserveIPSubpoolBodyParam = {
      name: 'string',
      ipv6AddressSpace: true,
      ipv4DhcpServers: [
        'string'
      ],
      ipv4DnsServers: [
        'string'
      ],
      ipv6GlobalPool: 'string',
      ipv6Prefix: false,
      ipv6PrefixLength: 4,
      ipv6Subnet: 'string',
      ipv6GateWay: 'string',
      ipv6DhcpServers: [
        'string'
      ],
      ipv6DnsServers: [
        'string'
      ],
      slaacSupport: false,
      ipv4GateWay: 'string'
    };
    describe('#updateReserveIPSubpool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateReserveIPSubpool('fakedata', 'fakedata', networkSettingsUpdateReserveIPSubpoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateReserveIPSubpool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsReserveIPSubpoolBodyParam = {
      name: 'string',
      type: 'LAN',
      ipv6AddressSpace: false,
      ipv4GlobalPool: 'string',
      ipv4Prefix: false,
      ipv4PrefixLength: 7,
      ipv4Subnet: 'string',
      ipv4GateWay: 'string',
      ipv4DhcpServers: [
        'string'
      ],
      ipv4DnsServers: [
        'string'
      ],
      ipv6GlobalPool: 'string',
      ipv6Prefix: false,
      ipv6PrefixLength: 1,
      ipv6Subnet: 'string',
      ipv6GateWay: 'string',
      ipv6DhcpServers: [
        'string'
      ],
      ipv6DnsServers: [
        'string'
      ],
      ipv4TotalHost: 6,
      ipv6TotalHost: 8,
      slaacSupport: true
    };
    describe('#reserveIPSubpool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reserveIPSubpool('fakedata', networkSettingsReserveIPSubpoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'reserveIPSubpool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReserveIPSubpool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getReserveIPSubpool('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'getReserveIPSubpool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSPProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSPProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'deleteSPProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsAssignDeviceCredentialToSiteBodyParam = {
      cliId: 'string',
      snmpV2ReadId: 'string',
      snmpV2WriteId: 'string',
      httpRead: 'string',
      httpWrite: 'string',
      snmpV3Id: 'string'
    };
    describe('#assignDeviceCredentialToSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignDeviceCredentialToSite('fakedata', networkSettingsAssignDeviceCredentialToSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'assignDeviceCredentialToSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCredentialDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceCredentialDetails(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'getDeviceCredentialDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateDeviceCredentialsBodyParam = {
      settings: {
        cliCredential: {
          description: 'string',
          username: 'string',
          password: 'string',
          enablePassword: 'string',
          id: 'string'
        },
        snmpV2cRead: {
          description: 'string',
          readCommunity: 'string',
          id: 'string'
        },
        snmpV2cWrite: {
          description: 'string',
          writeCommunity: 'string',
          id: 'string'
        },
        snmpV3: {
          authPassword: 'string',
          authType: 'string',
          snmpMode: 'string',
          privacyPassword: 'string',
          privacyType: 'string',
          username: 'string',
          description: 'string',
          id: 'string'
        },
        httpsRead: {
          name: 'string',
          username: 'string',
          password: 'string',
          port: 'string',
          id: 'string'
        },
        httpsWrite: {
          name: 'string',
          username: 'string',
          password: 'string',
          port: 'string',
          id: 'string'
        }
      }
    };
    describe('#updateDeviceCredentials - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceCredentials(networkSettingsUpdateDeviceCredentialsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateDeviceCredentials', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsCreateDeviceCredentialsBodyParam = {
      settings: {
        cliCredential: [
          {
            description: 'string',
            username: 'string',
            password: 'string',
            enablePassword: 'string'
          }
        ],
        snmpV2cRead: [
          {
            description: 'string',
            readCommunity: 'string'
          }
        ],
        snmpV2cWrite: [
          {
            description: 'string',
            writeCommunity: 'string'
          }
        ],
        snmpV3: [
          {
            description: 'string',
            username: 'string',
            privacyType: 'DES',
            privacyPassword: 'string',
            authType: 'SHA',
            authPassword: 'string',
            snmpMode: 'AUTHNOPRIV'
          }
        ],
        httpsRead: [
          {
            name: 'string',
            username: 'string',
            password: 'string',
            port: 4
          }
        ],
        httpsWrite: [
          {
            name: 'string',
            username: 'string',
            password: 'string',
            port: 4
          }
        ]
      }
    };
    describe('#createDeviceCredentials - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDeviceCredentials(networkSettingsCreateDeviceCredentialsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'createDeviceCredentials', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceProviderDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServiceProviderDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'getServiceProviderDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateSPProfileBodyParam = {
      settings: {
        qos: [
          {
            profileName: 'string',
            model: 'string',
            wanProvider: 'string',
            oldProfileName: 'string'
          }
        ]
      }
    };
    describe('#updateSPProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSPProfile(networkSettingsUpdateSPProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateSPProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsCreateSPProfileBodyParam = {
      settings: {
        qos: [
          {
            profileName: 'string',
            model: 'string',
            wanProvider: 'string'
          }
        ]
      }
    };
    describe('#createSPProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSPProfile(networkSettingsCreateSPProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'createSPProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsUpdateNetworkBodyParam = {
      settings: {
        dhcpServer: [
          'string'
        ],
        dnsServer: {
          domainName: 'can only contain alphanumeric characters or hyphen',
          primaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255',
          secondaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255'
        },
        syslogServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: false
        },
        snmpServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        netflowcollector: {
          ipAddress: 'string',
          port: 1
        },
        ntpServer: [
          'string'
        ],
        timezone: 'string',
        messageOfTheday: {
          bannerMessage: 'string',
          retainExistingBanner: 'string'
        },
        network_aaa: {
          servers: 'Server type supported by ISE and AAA',
          ipAddress: 'Mandatory for ISE servers and for AAA consider this as additional IP.',
          network: 'For AAA server consider it as primary IP and For ISE consider as Network',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        },
        clientAndEndpoint_aaa: {
          servers: 'string',
          ipAddress: 'Mandatory for ISE servers.',
          network: 'string',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        }
      }
    };
    describe('#updateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetwork('fakedata', networkSettingsUpdateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'updateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSettingsCreateNetworkBodyParam = {
      settings: {
        dhcpServer: [
          'string'
        ],
        dnsServer: {
          domainName: 'can only contain alphanumeric characters or hyphen',
          primaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255',
          secondaryIpAddress: 'valid range : 1.0.0.0 - 223.255.255.255'
        },
        syslogServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: false
        },
        snmpServer: {
          ipAddresses: [
            'string'
          ],
          configureDnacIP: true
        },
        netflowcollector: {
          ipAddress: 'string',
          port: 7
        },
        ntpServer: [
          'string'
        ],
        timezone: 'string',
        messageOfTheday: {
          bannerMessage: 'string',
          retainExistingBanner: 'string'
        },
        network_aaa: {
          servers: 'Server type supported by ISE and AAA',
          ipAddress: 'Mandatory for ISE servers and for AAA consider this as additional IP.',
          network: 'For AAA server consider it as primary IP and For ISE consider as Network',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        },
        clientAndEndpoint_aaa: {
          servers: 'string',
          ipAddress: 'Mandatory for ISE servers.',
          network: 'string',
          protocol: 'string',
          sharedSecret: samProps.authentication.password
        }
      }
    };
    describe('#createNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetwork('fakedata', networkSettingsCreateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'createNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseReserveIPSubpool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.releaseReserveIPSubpool('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSettings', 'releaseReserveIPSubpool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadFile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('File', 'uploadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientProximity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getClientProximity('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Clients', 'getClientProximity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientEnrichmentDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getClientEnrichmentDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Clients', 'getClientEnrichmentDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteCountV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteCountV2(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSiteCountV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveCancelAnImport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importMapArchiveCancelAnImport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'importMapArchiveCancelAnImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchivePerformImport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importMapArchivePerformImport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'importMapArchivePerformImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteV2(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSiteV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mapsSupportedAccessPoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.mapsSupportedAccessPoints((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'mapsSupportedAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesThatAreAssignedToASite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDevicesThatAreAssignedToASite('fakedata', null, null, 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getDevicesThatAreAssignedToASite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveStartImport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importMapArchiveStartImport((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'importMapArchiveStartImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveImportStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importMapArchiveImportStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'importMapArchiveImportStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesAssignDevicesToSiteBodyParam = {
      device: [
        {
          ip: 'string'
        }
      ]
    };
    describe('#assignDevicesToSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignDevicesToSite('fakedata', sitesAssignDevicesToSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'assignDevicesToSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesExportMapArchiveBodyParam = {};
    describe('#exportMapArchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportMapArchive('fakedata', sitesExportMapArchiveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'exportMapArchive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSite(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteCount(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getSiteCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembership - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMembership('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'getMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sitesUpdateSiteBodyParam = {
      type: 'floor',
      site: {
        area: {
          name: 'string',
          parentName: 'string'
        },
        building: {
          name: 'string',
          address: 'string',
          parentName: 'string',
          latitude: 6,
          longitude: 5
        },
        floor: {
          name: 'string',
          rfModel: 'Outdoor Open Space',
          width: 2,
          length: 1,
          height: 6
        }
      }
    };
    describe('#updateSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSite('fakedata', sitesUpdateSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'updateSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSite('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sites', 'deleteSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBusinessAPIExecutionDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBusinessAPIExecutionDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'getBusinessAPIExecutionDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoDNACenterSystemAuthorizeDeviceBodyParam = {
      deviceIdList: [
        'string'
      ]
    };
    describe('#authorizeDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeDevice(ciscoDNACenterSystemAuthorizeDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoDNACenterSystem', 'authorizeDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importCertificate(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'importCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificateP12 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importCertificateP12(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'importCertificateP12', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXStatusForAllDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEoXStatusForAllDevices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EoX', 'getEoXStatusForAllDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEoXSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EoX', 'getEoXSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXDetailsPerDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEoXDetailsPerDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EoX', 'getEoXDetailsPerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'associate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'disassociate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNFVProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNFVProfile('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'getNFVProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteProfileUpdateNFVProfileBodyParam = {
      device: [
        {
          deviceTag: 'string',
          directInternetAccessForFirewall: true,
          services: [
            {
              serviceType: 'fortinet',
              profileType: 'vEdge-samll',
              serviceName: 'string',
              imageName: 'string',
              vNicMapping: [
                {
                  networkType: 'GEO-2-1',
                  assignIpAddressToNetwork: 'string'
                }
              ],
              firewallMode: 'transparent'
            }
          ],
          customNetworks: [
            {
              networkName: 'string',
              servicesToConnect: [
                {
                  serviceName: 'string'
                }
              ],
              connectionType: 'GEO-2-2',
              vlanMode: 'trunk',
              vlanId: 5
            }
          ],
          vlanForL2: [
            {
              vlanType: 'access',
              vlanId: 3,
              vlanDescription: 'string'
            }
          ],
          customTemplate: [
            {
              deviceType: 'ASAV',
              template: 'string',
              templateType: 'Onboarding Template(s)'
            }
          ],
          currentDeviceTag: 'string'
        }
      ]
    };
    describe('#updateNFVProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNFVProfile('fakedata', null, siteProfileUpdateNFVProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'updateNFVProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNFVProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNFVProfile('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'deleteNFVProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteProfileNFVProvisioningDetailBodyParam = {
      device_ip: 'string'
    };
    describe('#nFVProvisioningDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.nFVProvisioningDetail(siteProfileNFVProvisioningDetailBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'nFVProvisioningDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteProfileCreateNFVProfileBodyParam = {
      profileName: 'string',
      device: [
        {
          deviceType: 'Cisco 5400 Enterprise Network Compute System',
          deviceTag: 'string',
          serviceProviderProfile: [
            {
              serviceProvider: 'string',
              linkType: 'GigabitEthernet',
              connect: true,
              connectDefaultGatewayOnWan: false
            }
          ],
          directInternetAccessForFirewall: false,
          services: [
            {
              serviceType: 'asa',
              profileType: 'vWASS-6000R-Original',
              serviceName: 'string',
              imageName: 'string',
              vNicMapping: [
                {
                  networkType: 'GEO-2-2',
                  assignIpAddressToNetwork: 'string'
                }
              ],
              firewallMode: 'routed'
            }
          ],
          customNetworks: [
            {
              networkName: 'string',
              servicesToConnect: [
                {
                  serviceName: 'string'
                }
              ],
              connectionType: 'GEO-3-2',
              vlanMode: 'trunk',
              vlanId: 8
            }
          ],
          vlanForL2: [
            {
              vlanType: 'trunk',
              vlanId: 10,
              vlanDescription: 'string'
            }
          ],
          customTemplate: [
            {
              deviceType: 'ASAV',
              template: 'string',
              templateType: 'Day-N-Template(s)'
            }
          ]
        }
      ]
    };
    describe('#createNFVProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNFVProfile(siteProfileCreateNFVProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteProfile', 'createNFVProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigurationArchiveDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConfigurationArchiveDetails(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationArchive', 'getConfigurationArchiveDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationArchiveExportDeviceConfigurationsBodyParam = {
      password: 'string',
      deviceId: [
        'string'
      ]
    };
    describe('#exportDeviceConfigurations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportDeviceConfigurations(configurationArchiveExportDeviceConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationArchive', 'exportDeviceConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAdvisoriesSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityAdvisories', 'getAdvisoriesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAdvisoriesList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityAdvisories', 'getAdvisoriesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesPerDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAdvisoriesPerDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityAdvisories', 'getAdvisoriesPerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoryIDsPerDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAdvisoryIDsPerDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityAdvisories', 'getAdvisoryIDsPerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesPerAdvisory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDevicesPerAdvisory('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityAdvisories', 'getDevicesPerAdvisory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetailCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComplianceDetailCount(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getComplianceDetailCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetailsOfDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComplianceDetailsOfDevice('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getComplianceDetailsOfDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const complianceCommitDeviceConfigurationBodyParam = {
      deviceId: [
        'string'
      ]
    };
    describe('#commitDeviceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.commitDeviceConfiguration(complianceCommitDeviceConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'commitDeviceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComplianceStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceComplianceStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getDeviceComplianceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceStatusCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComplianceStatusCount(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getComplianceStatusCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComplianceStatus(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getComplianceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComplianceDetail(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'getComplianceDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const complianceRunComplianceBodyParam = {
      triggerFull: true,
      categories: [
        'string'
      ],
      deviceUuids: [
        'string'
      ]
    };
    describe('#runCompliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.runCompliance(complianceRunComplianceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Compliance', 'runCompliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseUsageDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLicenseUsageDetails('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getLicenseUsageDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLicenseSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceLicenseSummary(555, 'fakedata', null, null, null, 555, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getDeviceLicenseSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualAccountDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVirtualAccountDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getVirtualAccountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesDeviceDeregistrationBodyParam = {
      device_uuids: [
        'string'
      ]
    };
    describe('#deviceDeregistration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deviceDeregistration(licensesDeviceDeregistrationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'deviceDeregistration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCountDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceCountDetails(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getDeviceCountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesDeviceRegistrationBodyParam = {
      device_uuids: [
        'string'
      ]
    };
    describe('#deviceRegistration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deviceRegistration('fakedata', licensesDeviceRegistrationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'deviceRegistration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAccountDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSmartAccountDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getSmartAccountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesChangeVirtualAccountBodyParam = {
      device_uuids: [
        'string'
      ]
    };
    describe('#changeVirtualAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeVirtualAccount('fakedata', 'fakedata', licensesChangeVirtualAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'changeVirtualAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLicenseDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceLicenseDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getDeviceLicenseDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseTermDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLicenseTermDetails('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getLicenseTermDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceReplacementDeployDeviceReplacementWorkflowBodyParam = {
      faultyDeviceSerialNumber: 'string',
      replacementDeviceSerialNumber: 'string'
    };
    describe('#deployDeviceReplacementWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployDeviceReplacementWorkflow(deviceReplacementDeployDeviceReplacementWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceReplacement', 'deployDeviceReplacementWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnListOfReplacementDevicesWithReplacementDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.returnListOfReplacementDevicesWithReplacementDetails(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceReplacement', 'returnListOfReplacementDevicesWithReplacementDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceReplacementUnMarkDeviceForReplacementBodyParam = [
      {
        creationTime: 1,
        family: 'string',
        faultyDeviceId: 'string',
        faultyDeviceName: 'string',
        faultyDevicePlatform: 'string',
        faultyDeviceSerialNumber: 'string',
        id: 'string',
        neighbourDeviceId: 'string',
        networkReadinessTaskId: 'string',
        replacementDevicePlatform: 'string',
        replacementDeviceSerialNumber: 'string',
        replacementStatus: 'string',
        replacementTime: 7,
        workflowId: 'string'
      }
    ];
    describe('#unMarkDeviceForReplacement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unMarkDeviceForReplacement(deviceReplacementUnMarkDeviceForReplacementBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceReplacement', 'unMarkDeviceForReplacement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceReplacementMarkDeviceForReplacementBodyParam = [
      {
        creationTime: 4,
        family: 'string',
        faultyDeviceId: 'string',
        faultyDeviceName: 'string',
        faultyDevicePlatform: 'string',
        faultyDeviceSerialNumber: 'string',
        id: 'string',
        neighbourDeviceId: 'string',
        networkReadinessTaskId: 'string',
        replacementDevicePlatform: 'string',
        replacementDeviceSerialNumber: 'string',
        replacementStatus: 'string',
        replacementTime: 9,
        workflowId: 'string'
      }
    ];
    describe('#markDeviceForReplacement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.markDeviceForReplacement(deviceReplacementMarkDeviceForReplacementBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceReplacement', 'markDeviceForReplacement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnReplacementDevicesCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.returnReplacementDevicesCount(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceReplacement', 'returnReplacementDevicesCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemHealthCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemHealthCount(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HealthAndPerformance', 'getSystemHealthCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemHealth(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HealthAndPerformance', 'getSystemHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemPerformanceHistorical - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemPerformanceHistorical(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HealthAndPerformance', 'getSystemPerformanceHistorical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemPerformanceAPI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemPerformanceAPI(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HealthAndPerformance', 'getSystemPerformanceAPI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAndRolesupdateRoleBodyParam = {
      roleId: 'string',
      description: 'string',
      resourceTypes: [
        {
          type: 'string',
          operations: [
            'string'
          ]
        }
      ]
    };
    describe('#updateRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRole(userAndRolesupdateRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'updateRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAndRolesaddRoleBodyParam = {
      role: 'string',
      description: 'string',
      resourceTypes: [
        {
          type: 'string',
          operations: [
            'string'
          ]
        }
      ]
    };
    describe('#addRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addRole(userAndRolesaddRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'addRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUsers('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAndRolesupdateUserBodyParam = {
      firstName: 'string',
      lastName: 'string',
      email: 'string',
      username: 'string',
      userId: 'string',
      roleList: [
        'string'
      ]
    };
    describe('#updateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUser(userAndRolesupdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAndRolesaddUserBodyParam = {
      firstName: 'string',
      lastName: 'string',
      username: 'string',
      password: 'string',
      email: 'string',
      roleList: [
        'string'
      ]
    };
    describe('#addUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUser(userAndRolesaddUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'addUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRoles((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'getRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPermissions((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'getPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalAuthenticationServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExternalAuthenticationServers('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'getExternalAuthenticationServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRole('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAndRoles', 'deleteRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterReleaseSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoDNACenterReleaseSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platform', 'getCiscoDNACenterReleaseSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterNodesConfigurationSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoDNACenterNodesConfigurationSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platform', 'getCiscoDNACenterNodesConfigurationSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterPackagesSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoDNACenterPackagesSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platform', 'getCiscoDNACenterPackagesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIssues(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issues', 'getIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueEnrichmentDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIssueEnrichmentDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issues', 'getIssueEnrichmentDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuesExecuteSuggestedActionsCommandsBodyParam = {
      entity_type: 'string',
      entity_value: 'string'
    };
    describe('#executeSuggestedActionsCommands - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.executeSuggestedActionsCommands(issuesExecuteSuggestedActionsCommandsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issues', 'executeSuggestedActionsCommands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSensors(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'getSensors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sensorsCreateSensorTestTemplateBodyParam = {
      ssids: [
        {
          ssid: 'string',
          profileName: 'string',
          authType: 'string',
          thirdParty: {
            selected: false
          },
          psk: 'string',
          tests: [
            {
              name: 'string',
              config: [
                {}
              ]
            }
          ],
          categories: [
            'string'
          ],
          qosPolicy: 'string'
        }
      ],
      name: 'string',
      connection: 'string',
      apCoverage: [
        {
          bands: 'string',
          numberOfApsToTest: 'string',
          rssiThreshold: 'string'
        }
      ],
      modelVersion: 2
    };
    describe('#createSensorTestTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSensorTestTemplate(sensorsCreateSensorTestTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'createSensorTestTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSensorTest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSensorTest(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'deleteSensorTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sensorsDuplicateSensorTestTemplateBodyParam = {
      templateName: 'string',
      newTemplateName: 'string'
    };
    describe('#duplicateSensorTestTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.duplicateSensorTestTemplate(sensorsDuplicateSensorTestTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'duplicateSensorTestTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sensorsEditSensorTestTemplateBodyParam = {
      templateName: 'string',
      locationInfoList: [
        {
          locationId: 'string',
          locationType: 'string',
          siteHierarchy: 'string',
          allSensors: false
        }
      ],
      schedule: {
        testScheduleMode: 'string',
        frequency: {
          unit: 'string',
          value: 7
        },
        scheduleRange: [
          {
            day: 'string',
            timeRange: [
              {
                from: 'string',
                to: 'string',
                frequency: {
                  unit: 'string',
                  value: 10
                }
              }
            ]
          }
        ]
      }
    };
    describe('#editSensorTestTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editSensorTestTemplate(sensorsEditSensorTestTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'editSensorTestTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sensorsRunNowSensorTestBodyParam = {
      templateName: 'string'
    };
    describe('#runNowSensorTest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.runNowSensorTest(sensorsRunNowSensorTestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sensors', 'runNowSensorTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fabricWirelessAddWLCToFabricDomainBodyParam = {
      deviceName: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#addWLCToFabricDomain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addWLCToFabricDomain(fabricWirelessAddWLCToFabricDomainBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FabricWireless', 'addWLCToFabricDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fabricWirelessRemoveWLCFromFabricDomainBodyParam = {
      deviceName: 'string',
      siteNameHierarchy: 'string'
    };
    describe('#removeWLCFromFabricDomain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeWLCFromFabricDomain('fakedata', fabricWirelessRemoveWLCFromFabricDomainBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FabricWireless', 'removeWLCFromFabricDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fabricWirelessGetSSIDToIPPoolMappingBodyParam = {
      vlanName: 'string',
      scalableGroupName: 'string',
      ssidNames: [
        'string'
      ],
      siteNameHierarchy: 'string'
    };
    describe('#getSSIDToIPPoolMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSSIDToIPPoolMapping('fakedata', 'fakedata', fabricWirelessGetSSIDToIPPoolMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FabricWireless', 'getSSIDToIPPoolMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fabricWirelessUpdateSSIDToIPPoolMappingBodyParam = {
      vlanName: 'string',
      scalableGroupName: 'string',
      ssidNames: [
        'string'
      ],
      siteNameHierarchy: 'string'
    };
    describe('#updateSSIDToIPPoolMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSSIDToIPPoolMapping(fabricWirelessUpdateSSIDToIPPoolMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FabricWireless', 'updateSSIDToIPPoolMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fabricWirelessAddSSIDToIPPoolMappingBodyParam = {
      vlanName: 'string',
      scalableGroupName: 'string',
      ssidNames: [
        'string'
      ],
      siteNameHierarchy: 'string'
    };
    describe('#addSSIDToIPPoolMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSSIDToIPPoolMapping(fabricWirelessAddSSIDToIPPoolMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FabricWireless', 'addSSIDToIPPoolMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationHealth(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'getApplicationHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMGetCMDBSyncStatusBodyParam = {};
    describe('#getCMDBSyncStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCMDBSyncStatus(null, null, iTSMGetCMDBSyncStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSM', 'getCMDBSyncStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFailedITSMEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFailedITSMEvents(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSM', 'getFailedITSMEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iTSMRetryIntegrationEventsBodyParam = [
      'string'
    ];
    describe('#retryIntegrationEvents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryIntegrationEvents(iTSMRetryIntegrationEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ITSM', 'retryIntegrationEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEnrichmentDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserEnrichmentDetails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-dna_center-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserEnrichmentDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
