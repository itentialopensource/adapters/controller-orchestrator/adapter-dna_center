/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-dna_center',
      type: 'DnaCenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const DnaCenter = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Dna_center Adapter Test', () => {
  describe('DnaCenter Class Tests', () => {
    const a = new DnaCenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('dna_center'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('dna_center'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('DnaCenter', pronghornDotJson.export);
          assert.equal('Dna_center', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-dna_center', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('dna_center'));
          assert.equal('DnaCenter', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-dna_center', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-dna_center', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#postDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should have a postDnaintentapiv1templateProgrammerproject function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1templateProgrammerproject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerproject(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammerproject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerproject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammerproject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should have a getDnaintentapiv1templateProgrammerproject function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1templateProgrammerproject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1templateProgrammerproject - errors', () => {
      it('should have a putDnaintentapiv1templateProgrammerproject function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1templateProgrammerproject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammerproject(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammerproject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammerproject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammerproject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammertemplate - errors', () => {
      it('should have a getDnaintentapiv1templateProgrammertemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1templateProgrammertemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1templateProgrammertemplate - errors', () => {
      it('should have a putDnaintentapiv1templateProgrammertemplate function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1templateProgrammertemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammertemplate(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammertemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammertemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammertemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1templateProgrammertemplatedeploy - errors', () => {
      it('should have a postDnaintentapiv1templateProgrammertemplatedeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1templateProgrammertemplatedeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammertemplatedeploy(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammertemplatedeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammertemplatedeploy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammertemplatedeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1templateProgrammertemplateversion - errors', () => {
      it('should have a postDnaintentapiv1templateProgrammertemplateversion function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1templateProgrammertemplateversion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammertemplateversion(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammertemplateversion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammertemplateversion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammertemplateversion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammertemplatetemplateId - errors', () => {
      it('should have a getDnaintentapiv1templateProgrammertemplatetemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1templateProgrammertemplatetemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getDnaintentapiv1templateProgrammertemplatetemplateId(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1templateProgrammertemplatetemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1templateProgrammertemplatetemplateId - errors', () => {
      it('should have a deleteDnaintentapiv1templateProgrammertemplatetemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1templateProgrammertemplatetemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteDnaintentapiv1templateProgrammertemplatetemplateId(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1templateProgrammertemplatetemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId - errors', () => {
      it('should have a getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentId', (done) => {
        try {
          a.getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId(null, (data, error) => {
            try {
              const displayE = 'deploymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1templateProgrammerprojectprojectId - errors', () => {
      it('should have a deleteDnaintentapiv1templateProgrammerprojectprojectId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1templateProgrammerprojectprojectId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteDnaintentapiv1templateProgrammerprojectprojectId(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1templateProgrammerprojectprojectId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1templateProgrammertemplateversiontemplateId - errors', () => {
      it('should have a getDnaintentapiv1templateProgrammertemplateversiontemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1templateProgrammertemplateversiontemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getDnaintentapiv1templateProgrammertemplateversiontemplateId(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1templateProgrammertemplateversiontemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1templateProgrammertemplatepreview - errors', () => {
      it('should have a putDnaintentapiv1templateProgrammertemplatepreview function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1templateProgrammertemplatepreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammertemplatepreview(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammertemplatepreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1templateProgrammertemplatepreview('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1templateProgrammertemplatepreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1templateProgrammerprojectprojectIdtemplate - errors', () => {
      it('should have a postDnaintentapiv1templateProgrammerprojectprojectIdtemplate function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1templateProgrammerprojectprojectIdtemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerprojectprojectIdtemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammerprojectprojectIdtemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerprojectprojectIdtemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammerprojectprojectIdtemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.postDnaintentapiv1templateProgrammerprojectprojectIdtemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1templateProgrammerprojectprojectIdtemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1tagidmember - errors', () => {
      it('should have a postDnaintentapiv1tagidmember function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1tagidmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1tagidmember(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1tagidmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1tagidmember('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1tagidmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postDnaintentapiv1tagidmember('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1tagidmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagidmember - errors', () => {
      it('should have a getDnaintentapiv1tagidmember function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tagidmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1tagidmember(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tagidmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberType', (done) => {
        try {
          a.getDnaintentapiv1tagidmember('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'memberType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tagidmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1tag - errors', () => {
      it('should have a postDnaintentapiv1tag function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1tag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1tag(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1tag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1tag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1tag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1tag - errors', () => {
      it('should have a putDnaintentapiv1tag function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1tag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1tag(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1tag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1tag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1tag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tag - errors', () => {
      it('should have a getDnaintentapiv1tag function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagidmembercount - errors', () => {
      it('should have a getDnaintentapiv1tagidmembercount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tagidmembercount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1tagidmembercount(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tagidmembercount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberType', (done) => {
        try {
          a.getDnaintentapiv1tagidmembercount('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'memberType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tagidmembercount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagmembertype - errors', () => {
      it('should have a getDnaintentapiv1tagmembertype function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tagmembertype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1tagmember - errors', () => {
      it('should have a putDnaintentapiv1tagmember function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1tagmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1tagmember(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1tagmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1tagmember('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1tagmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1tagid - errors', () => {
      it('should have a deleteDnaintentapiv1tagid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1tagid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1tagid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1tagid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagid - errors', () => {
      it('should have a getDnaintentapiv1tagid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tagid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1tagid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tagid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tagcount - errors', () => {
      it('should have a getDnaintentapiv1tagcount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tagcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1tagidmembermemberId - errors', () => {
      it('should have a deleteDnaintentapiv1tagidmembermemberId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1tagidmembermemberId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1tagidmembermemberId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1tagidmembermemberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberId', (done) => {
        try {
          a.deleteDnaintentapiv1tagidmembermemberId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'memberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1tagidmembermemberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoverycount - errors', () => {
      it('should have a getDnaintentapiv1discoverycount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoverycount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialnetconf - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialnetconf function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialnetconf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialnetconf(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialnetconf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialnetconf('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialnetconf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialnetconf - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialnetconf function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialnetconf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialnetconf(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialnetconf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialnetconf('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialnetconf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialsnmpv2WriteCommunity - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialsnmpv2WriteCommunity function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialsnmpv2WriteCommunity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv2WriteCommunity(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv2WriteCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv2WriteCommunity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv2WriteCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialsnmpv2WriteCommunity - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialsnmpv2WriteCommunity function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialsnmpv2WriteCommunity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv2WriteCommunity(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv2WriteCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv2WriteCommunity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv2WriteCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialsnmpv3 - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialsnmpv3 function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialsnmpv3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv3(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv3('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialsnmpv3 - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialsnmpv3 function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialsnmpv3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv3(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv3('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidsummary - errors', () => {
      it('should have a getDnaintentapiv1discoveryidsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryidsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryidsummary(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialhttpWrite - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialhttpWrite function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialhttpWrite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialhttpWrite(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialhttpWrite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialhttpWrite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialhttpWrite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialhttpWrite - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialhttpWrite function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialhttpWrite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialhttpWrite(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialhttpWrite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialhttpWrite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialhttpWrite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1snmpProperty - errors', () => {
      it('should have a getDnaintentapiv1snmpProperty function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1snmpProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1snmpProperty - errors', () => {
      it('should have a postDnaintentapiv1snmpProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1snmpProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1snmpProperty(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1snmpProperty', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1snmpProperty('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1snmpProperty', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1discoveryid - errors', () => {
      it('should have a deleteDnaintentapiv1discoveryid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1discoveryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1discoveryid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1discoveryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryid - errors', () => {
      it('should have a getDnaintentapiv1discoveryid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1discovery - errors', () => {
      it('should have a postDnaintentapiv1discovery function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1discovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1discovery(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1discovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1discovery('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1discovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1discovery - errors', () => {
      it('should have a putDnaintentapiv1discovery function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1discovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1discovery(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1discovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1discovery('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1discovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1discovery - errors', () => {
      it('should have a deleteDnaintentapiv1discovery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1discovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialsnmpv2ReadCommunity - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialsnmpv2ReadCommunity function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialsnmpv2ReadCommunity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv2ReadCommunity(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv2ReadCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialsnmpv2ReadCommunity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialsnmpv2ReadCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialsnmpv2ReadCommunity - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialsnmpv2ReadCommunity function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialsnmpv2ReadCommunity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv2ReadCommunity(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv2ReadCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialsnmpv2ReadCommunity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialsnmpv2ReadCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoverystartIndexrecordsToReturn - errors', () => {
      it('should have a getDnaintentapiv1discoverystartIndexrecordsToReturn function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoverystartIndexrecordsToReturn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getDnaintentapiv1discoverystartIndexrecordsToReturn(null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoverystartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordsToReturn', (done) => {
        try {
          a.getDnaintentapiv1discoverystartIndexrecordsToReturn('fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordsToReturn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoverystartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialglobalCredentialId - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialglobalCredentialId function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialglobalCredentialId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialglobalCredentialId(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialglobalCredentialId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialglobalCredentialId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialglobalCredentialId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing globalCredentialId', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialglobalCredentialId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'globalCredentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialglobalCredentialId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1globalCredentialglobalCredentialId - errors', () => {
      it('should have a deleteDnaintentapiv1globalCredentialglobalCredentialId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1globalCredentialglobalCredentialId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing globalCredentialId', (done) => {
        try {
          a.deleteDnaintentapiv1globalCredentialglobalCredentialId(null, (data, error) => {
            try {
              const displayE = 'globalCredentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1globalCredentialglobalCredentialId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1globalCredentialid - errors', () => {
      it('should have a getDnaintentapiv1globalCredentialid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1globalCredentialid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1globalCredentialid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1globalCredentialid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialcli - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialcli function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialcli === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialcli(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialcli', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialcli('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialcli', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialcli - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialcli function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialcli === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialcli(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialcli', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialcli('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialcli', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1globalCredentialhttpRead - errors', () => {
      it('should have a putDnaintentapiv1globalCredentialhttpRead function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1globalCredentialhttpRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialhttpRead(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialhttpRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1globalCredentialhttpRead('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1globalCredentialhttpRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1globalCredentialhttpRead - errors', () => {
      it('should have a postDnaintentapiv1globalCredentialhttpRead function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1globalCredentialhttpRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialhttpRead(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialhttpRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1globalCredentialhttpRead('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1globalCredentialhttpRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryjob - errors', () => {
      it('should have a getDnaintentapiv1discoveryjob function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryjob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getDnaintentapiv1discoveryjob('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryjob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidnetworkDevicecount - errors', () => {
      it('should have a getDnaintentapiv1discoveryidnetworkDevicecount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryidnetworkDevicecount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicecount(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidnetworkDevicecount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidjob - errors', () => {
      it('should have a getDnaintentapiv1discoveryidjob function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryidjob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryidjob(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidjob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn - errors', () => {
      it('should have a getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordsToReturn', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'recordsToReturn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1discoverystartIndexrecordsToDelete - errors', () => {
      it('should have a deleteDnaintentapiv1discoverystartIndexrecordsToDelete function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1discoverystartIndexrecordsToDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.deleteDnaintentapiv1discoverystartIndexrecordsToDelete(null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1discoverystartIndexrecordsToDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordsToDelete', (done) => {
        try {
          a.deleteDnaintentapiv1discoverystartIndexrecordsToDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordsToDelete is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1discoverystartIndexrecordsToDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1discoveryidnetworkDevice - errors', () => {
      it('should have a getDnaintentapiv1discoveryidnetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1discoveryidnetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1discoveryidnetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1discoveryidnetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1globalCredential - errors', () => {
      it('should have a getDnaintentapiv1globalCredential function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1globalCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDeviceunclaim - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDeviceunclaim function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDeviceunclaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceunclaim(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceunclaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceunclaim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceunclaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult(null, null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should have a putDnaintentapiv1onboardingpnpDeviceid function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1onboardingpnpDeviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpDeviceid(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpDeviceid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpDeviceid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpDeviceid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpDeviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDeviceid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1onboardingpnpDeviceid - errors', () => {
      it('should have a deleteDnaintentapiv1onboardingpnpDeviceid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1onboardingpnpDeviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1onboardingpnpDeviceid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1onboardingpnpDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpSettingssavacct - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpSettingssavacct function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpSettingssavacct === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpSettingssavacct(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpSettingssavacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpSettingssavacct('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpSettingssavacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1onboardingpnpSettingssavacct - errors', () => {
      it('should have a putDnaintentapiv1onboardingpnpSettingssavacct function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1onboardingpnpSettingssavacct === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpSettingssavacct(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpSettingssavacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpSettingssavacct('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpSettingssavacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDeviceimport - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDeviceimport function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDeviceimport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceimport(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceimport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
      it('should have a putDnaintentapiv1onboardingpnpWorkflowid function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1onboardingpnpWorkflowid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpWorkflowid(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpWorkflowid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpWorkflowid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpWorkflowid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpWorkflowid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpWorkflowid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpWorkflowid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpWorkflowid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpWorkflowid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpWorkflowid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1onboardingpnpWorkflowid - errors', () => {
      it('should have a deleteDnaintentapiv1onboardingpnpWorkflowid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1onboardingpnpWorkflowid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1onboardingpnpWorkflowid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1onboardingpnpWorkflowid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDevicesiteClaim - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDevicesiteClaim function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDevicesiteClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicesiteClaim(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicesiteClaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicesiteClaim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicesiteClaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1onboardingpnpSettingsvacct - errors', () => {
      it('should have a deleteDnaintentapiv1onboardingpnpSettingsvacct function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1onboardingpnpSettingsvacct === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.deleteDnaintentapiv1onboardingpnpSettingsvacct(null, null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1onboardingpnpSettingsvacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteDnaintentapiv1onboardingpnpSettingsvacct('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1onboardingpnpSettingsvacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpSettingssacct - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpSettingssacct function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpSettingssacct === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpWorkflowcount - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpWorkflowcount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpWorkflowcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpSettingssacctdomainvacct - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpSettingssacctdomainvacct function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpSettingssacctdomainvacct === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpSettingssacctdomainvacct(null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpSettingssacctdomainvacct', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1onboardingpnpSettings - errors', () => {
      it('should have a putDnaintentapiv1onboardingpnpSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1onboardingpnpSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpSettings(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1onboardingpnpSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1onboardingpnpSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpSettings - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDevicereset - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDevicereset function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDevicereset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicereset(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicereset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicereset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicereset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpWorkflow - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpWorkflow - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDevicevacctSync - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDevicevacctSync function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDevicevacctSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicevacctSync(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicevacctSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicevacctSync('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicevacctSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDevicecount - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpDevicecount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpDevicecount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDevicehistory - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpDevicehistory function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpDevicehistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getDnaintentapiv1onboardingpnpDevicehistory(null, null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1onboardingpnpDevicehistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDevicesiteConfigPreview - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDevicesiteConfigPreview function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDevicesiteConfigPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicesiteConfigPreview(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicesiteConfigPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevicesiteConfigPreview('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevicesiteConfigPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDeviceclaim - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDeviceclaim function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDeviceclaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceclaim(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceclaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDeviceclaim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDeviceclaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1onboardingpnpDevice - errors', () => {
      it('should have a getDnaintentapiv1onboardingpnpDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1onboardingpnpDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1onboardingpnpDevice - errors', () => {
      it('should have a postDnaintentapiv1onboardingpnpDevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1onboardingpnpDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevice(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1onboardingpnpDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1onboardingpnpDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1imageimportation - errors', () => {
      it('should have a getDnaintentapiv1imageimportation function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1imageimportation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1imageimportationsourcefile - errors', () => {
      it('should have a postDnaintentapiv1imageimportationsourcefile function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1imageimportationsourcefile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1imageimportationsourcefile(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imageimportationsourcefile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1imagedistribution - errors', () => {
      it('should have a postDnaintentapiv1imagedistribution function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1imagedistribution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1imagedistribution(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imagedistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1imagedistribution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imagedistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1imageimportationsourceurl - errors', () => {
      it('should have a postDnaintentapiv1imageimportationsourceurl function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1imageimportationsourceurl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1imageimportationsourceurl(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imageimportationsourceurl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1imageimportationsourceurl('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imageimportationsourceurl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1imageactivationdevice - errors', () => {
      it('should have a postDnaintentapiv1imageactivationdevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1imageactivationdevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1imageactivationdevice(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imageactivationdevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1imageactivationdevice('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1imageactivationdevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicemoduleid - errors', () => {
      it('should have a getDnaintentapiv1networkDevicemoduleid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicemoduleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemoduleid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicemoduleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevice - errors', () => {
      it('should have a getDnaintentapiv1networkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceWithOptions - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceWithOptions function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceWithOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1networkDevice - errors', () => {
      it('should have a postDnaintentapiv1networkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1networkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1networkDevice(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1networkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1networkDevice - errors', () => {
      it('should have a putDnaintentapiv1networkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1networkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1networkDevice(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1networkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidvlan - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceidvlan function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceidvlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidvlan(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceidvlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn - errors', () => {
      it('should have a getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordsToReturn', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordsToReturn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicecollectionScheduleglobal - errors', () => {
      it('should have a getDnaintentapiv1networkDevicecollectionScheduleglobal function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicecollectionScheduleglobal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacecount - errors', () => {
      it('should have a getDnaintentapiv1interfacecount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfacecount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1networkDeviceid - errors', () => {
      it('should have a deleteDnaintentapiv1networkDeviceid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1networkDeviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDnaintentapiv1networkDeviceid(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1networkDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceid - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1networkDevicesync - errors', () => {
      it('should have a putDnaintentapiv1networkDevicesync function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1networkDevicesync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1networkDevicesync(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevicesync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1networkDevicesync('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevicesync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicecount - errors', () => {
      it('should have a getDnaintentapiv1networkDevicecount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicecount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceconfigcount - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceconfigcount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceconfigcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidcollectionSchedule - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceidcollectionSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceidcollectionSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidcollectionSchedule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceidcollectionSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName - errors', () => {
      it('should have a getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicemodulecount - errors', () => {
      it('should have a getDnaintentapiv1networkDevicemodulecount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicemodulecount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemodulecount(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicemodulecount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidmerakiOrganization - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceidmerakiOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceidmerakiOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidmerakiOrganization(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceidmerakiOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceIdcount - errors', () => {
      it('should have a getDnaintentapiv1interfacenetworkDevicedeviceIdcount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfacenetworkDevicedeviceIdcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceIdcount(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceIdcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidbrief - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceidbrief function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceidbrief === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidbrief(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceidbrief', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicenetworkDeviceIdconfig - errors', () => {
      it('should have a getDnaintentapiv1networkDevicenetworkDeviceIdconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicenetworkDeviceIdconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkDeviceId', (done) => {
        try {
          a.getDnaintentapiv1networkDevicenetworkDeviceIdconfig(null, (data, error) => {
            try {
              const displayE = 'networkDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicenetworkDeviceIdconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceospf - errors', () => {
      it('should have a getDnaintentapiv1interfaceospf function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfaceospf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicefunctionalCapabilityid - errors', () => {
      it('should have a getDnaintentapiv1networkDevicefunctionalCapabilityid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicefunctionalCapabilityid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDevicefunctionalCapabilityid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicefunctionalCapabilityid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceisis - errors', () => {
      it('should have a getDnaintentapiv1interfaceisis function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfaceisis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceconfig - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceid - errors', () => {
      it('should have a getDnaintentapiv1interfaceid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfaceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1interfaceid(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfaceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicefunctionalCapability - errors', () => {
      it('should have a getDnaintentapiv1networkDevicefunctionalCapability function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicefunctionalCapability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfacenetworkDevicedeviceId - errors', () => {
      it('should have a getDnaintentapiv1interfacenetworkDevicedeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfacenetworkDevicedeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1interfacenetworkDevicedeviceId(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfacenetworkDevicedeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDnaintentapiv1networkDevicebrief - errors', () => {
      it('should have a putDnaintentapiv1networkDevicebrief function', (done) => {
        try {
          assert.equal(true, typeof a.putDnaintentapiv1networkDevicebrief === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.putDnaintentapiv1networkDevicebrief(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevicebrief', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.putDnaintentapiv1networkDevicebrief('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-putDnaintentapiv1networkDevicebrief', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1networkDevicefile - errors', () => {
      it('should have a postDnaintentapiv1networkDevicefile function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1networkDevicefile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1networkDevicefile(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevicefile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1networkDevicefile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevicefile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicetenantinfomacaddress - errors', () => {
      it('should have a getDnaintentapiv1networkDevicetenantinfomacaddress function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicetenantinfomacaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interfaceipAddressipAddress - errors', () => {
      it('should have a getDnaintentapiv1interfaceipAddressipAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interfaceipAddressipAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getDnaintentapiv1interfaceipAddressipAddress(null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1interfaceipAddressipAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceserialNumberserialNumber - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceserialNumberserialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceserialNumberserialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceserialNumberserialNumber(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceserialNumberserialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceipAddressipAddress - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceipAddressipAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceipAddressipAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceipAddressipAddress(null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceipAddressipAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicemodule - errors', () => {
      it('should have a getDnaintentapiv1networkDevicemodule function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicemodule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDnaintentapiv1networkDevicemodule(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicemodule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicestartIndexrecordsToReturn - errors', () => {
      it('should have a getDnaintentapiv1networkDevicestartIndexrecordsToReturn function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicestartIndexrecordsToReturn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getDnaintentapiv1networkDevicestartIndexrecordsToReturn(null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicestartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordsToReturn', (done) => {
        try {
          a.getDnaintentapiv1networkDevicestartIndexrecordsToReturn('fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordsToReturn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDevicestartIndexrecordsToReturn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1deviceDetail - errors', () => {
      it('should have a getDnaintentapiv1deviceDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1deviceDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getDnaintentapiv1deviceDetail(null, null, null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1deviceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchBy', (done) => {
        try {
          a.getDnaintentapiv1deviceDetail('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'searchBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1deviceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identifier', (done) => {
        try {
          a.getDnaintentapiv1deviceDetail('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'identifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1deviceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1interface - errors', () => {
      it('should have a getDnaintentapiv1interface function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1interface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceautocomplete - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceautocomplete function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceautocomplete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDeviceidwirelessInfo - errors', () => {
      it('should have a getDnaintentapiv1networkDeviceidwirelessInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDeviceidwirelessInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDnaintentapiv1networkDeviceidwirelessInfo(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkDeviceidwirelessInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1siteHealth - errors', () => {
      it('should have a getDnaintentapiv1siteHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1siteHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getDnaintentapiv1siteHealth(null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1siteHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1sitesiteIddevice - errors', () => {
      it('should have a postDnaintentapiv1sitesiteIddevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1sitesiteIddevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1sitesiteIddevice(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Runsync', (done) => {
        try {
          a.postDnaintentapiv1sitesiteIddevice('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'Runsync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Persistbapioutput', (done) => {
        try {
          a.postDnaintentapiv1sitesiteIddevice('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'Persistbapioutput is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.postDnaintentapiv1sitesiteIddevice('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnasystemapiv1sitesiteIddevice - errors', () => {
      it('should have a postDnasystemapiv1sitesiteIddevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnasystemapiv1sitesiteIddevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnasystemapiv1sitesiteIddevice(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Runsync', (done) => {
        try {
          a.postDnasystemapiv1sitesiteIddevice('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'Runsync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Persistbapioutput', (done) => {
        try {
          a.postDnasystemapiv1sitesiteIddevice('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'Persistbapioutput is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.postDnasystemapiv1sitesiteIddevice('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1sitesiteIddevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1site - errors', () => {
      it('should have a postDnaintentapiv1site function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1site === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1site(null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Runsync', (done) => {
        try {
          a.postDnaintentapiv1site('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'Runsync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Persistbapioutput', (done) => {
        try {
          a.postDnaintentapiv1site('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'Persistbapioutput is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnasystemapiv1site - errors', () => {
      it('should have a postDnasystemapiv1site function', (done) => {
        try {
          assert.equal(true, typeof a.postDnasystemapiv1site === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnasystemapiv1site(null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Runsync', (done) => {
        try {
          a.postDnasystemapiv1site('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'Runsync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Persistbapioutput', (done) => {
        try {
          a.postDnasystemapiv1site('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'Persistbapioutput is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1site', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1taskcount - errors', () => {
      it('should have a getDnaintentapiv1taskcount function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1taskcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tasktaskId - errors', () => {
      it('should have a getDnaintentapiv1tasktaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tasktaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getDnaintentapiv1tasktaskId(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tasktaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1task - errors', () => {
      it('should have a getDnaintentapiv1task function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1task === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1tasktaskIdtree - errors', () => {
      it('should have a getDnaintentapiv1tasktaskIdtree function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1tasktaskIdtree === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getDnaintentapiv1tasktaskIdtree(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1tasktaskIdtree', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1taskoperationoperationIdoffsetlimit - errors', () => {
      it('should have a getDnaintentapiv1taskoperationoperationIdoffsetlimit function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1taskoperationoperationIdoffsetlimit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operationId', (done) => {
        try {
          a.getDnaintentapiv1taskoperationoperationIdoffsetlimit(null, null, null, (data, error) => {
            try {
              const displayE = 'operationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1taskoperationoperationIdoffsetlimit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getDnaintentapiv1taskoperationoperationIdoffsetlimit('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1taskoperationoperationIdoffsetlimit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getDnaintentapiv1taskoperationoperationIdoffsetlimit('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1taskoperationoperationIdoffsetlimit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1filenamespace - errors', () => {
      it('should have a getDnaintentapiv1filenamespace function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1filenamespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1filenamespacenameSpace - errors', () => {
      it('should have a getDnaintentapiv1filenamespacenameSpace function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1filenamespacenameSpace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nameSpace', (done) => {
        try {
          a.getDnaintentapiv1filenamespacenameSpace(null, (data, error) => {
            try {
              const displayE = 'nameSpace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1filenamespacenameSpace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1filefileId - errors', () => {
      it('should have a getDnaintentapiv1filefileId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1filefileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDnaintentapiv1filefileId(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1filefileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkDevicePollerclilegitReads - errors', () => {
      it('should have a getDnaintentapiv1networkDevicePollerclilegitReads function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkDevicePollerclilegitReads === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1networkDevicePollerclireadRequest - errors', () => {
      it('should have a postDnaintentapiv1networkDevicePollerclireadRequest function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1networkDevicePollerclireadRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1networkDevicePollerclireadRequest(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevicePollerclireadRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1networkDevicePollerclireadRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1networkDevicePollerclireadRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyvlanvlanNames - errors', () => {
      it('should have a getDnaintentapiv1topologyvlanvlanNames function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1topologyvlanvlanNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologysiteTopology - errors', () => {
      it('should have a getDnaintentapiv1topologysiteTopology function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1topologysiteTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyphysicalTopology - errors', () => {
      it('should have a getDnaintentapiv1topologyphysicalTopology function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1topologyphysicalTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyl2vlanID - errors', () => {
      it('should have a getDnaintentapiv1topologyl2vlanID function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1topologyl2vlanID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlanID', (done) => {
        try {
          a.getDnaintentapiv1topologyl2vlanID(null, (data, error) => {
            try {
              const displayE = 'vlanID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1topologyl2vlanID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1topologyl3topologyType - errors', () => {
      it('should have a getDnaintentapiv1topologyl3topologyType function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1topologyl3topologyType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyType', (done) => {
        try {
          a.getDnaintentapiv1topologyl3topologyType(null, (data, error) => {
            try {
              const displayE = 'topologyType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1topologyl3topologyType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1networkHealth - errors', () => {
      it('should have a getDnaintentapiv1networkHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1networkHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getDnaintentapiv1networkHealth(null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1networkHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1flowAnalysis - errors', () => {
      it('should have a getDnaintentapiv1flowAnalysis function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1flowAnalysis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1flowAnalysis - errors', () => {
      it('should have a postDnaintentapiv1flowAnalysis function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1flowAnalysis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1flowAnalysis(null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1flowAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnaintentapiv1flowAnalysis('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1flowAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1flowAnalysisflowAnalysisId - errors', () => {
      it('should have a getDnaintentapiv1flowAnalysisflowAnalysisId function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1flowAnalysisflowAnalysisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowAnalysisId', (done) => {
        try {
          a.getDnaintentapiv1flowAnalysisflowAnalysisId(null, (data, error) => {
            try {
              const displayE = 'flowAnalysisId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1flowAnalysisflowAnalysisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1flowAnalysisflowAnalysisId - errors', () => {
      it('should have a deleteDnaintentapiv1flowAnalysisflowAnalysisId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1flowAnalysisflowAnalysisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowAnalysisId', (done) => {
        try {
          a.deleteDnaintentapiv1flowAnalysisflowAnalysisId(null, (data, error) => {
            try {
              const displayE = 'flowAnalysisId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1flowAnalysisflowAnalysisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1businessnfvprovisioningDetail - errors', () => {
      it('should have a getDnaintentapiv1businessnfvprovisioningDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1businessnfvprovisioningDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIp', (done) => {
        try {
          a.getDnaintentapiv1businessnfvprovisioningDetail(null, (data, error) => {
            try {
              const displayE = 'deviceIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1businessnfvprovisioningDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1businessnfv - errors', () => {
      it('should have a postDnaintentapiv1businessnfv function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1businessnfv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1businessnfv(null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businessnfv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Runsync', (done) => {
        try {
          a.postDnaintentapiv1businessnfv('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'Runsync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businessnfv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Persistbapioutput', (done) => {
        try {
          a.postDnaintentapiv1businessnfv('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'Persistbapioutput is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businessnfv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnasystemapiv1authtoken - errors', () => {
      it('should have a postDnasystemapiv1authtoken function', (done) => {
        try {
          assert.equal(true, typeof a.postDnasystemapiv1authtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ContentType', (done) => {
        try {
          a.postDnasystemapiv1authtoken(null, null, (data, error) => {
            try {
              const displayE = 'ContentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1authtoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Authorization', (done) => {
        try {
          a.postDnasystemapiv1authtoken('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Authorization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnasystemapiv1authtoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1businessssidssidNamemanagedAPLocations - errors', () => {
      it('should have a deleteDnaintentapiv1businessssidssidNamemanagedAPLocations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1businessssidssidNamemanagedAPLocations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssidName', (done) => {
        try {
          a.deleteDnaintentapiv1businessssidssidNamemanagedAPLocations(null, null, (data, error) => {
            try {
              const displayE = 'ssidName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1businessssidssidNamemanagedAPLocations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedAPLocations', (done) => {
        try {
          a.deleteDnaintentapiv1businessssidssidNamemanagedAPLocations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'managedAPLocations is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1businessssidssidNamemanagedAPLocations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1businessssid - errors', () => {
      it('should have a postDnaintentapiv1businessssid function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1businessssid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1businessssid(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businessssid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1enterpriseSsid - errors', () => {
      it('should have a getDnaintentapiv1enterpriseSsid function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1enterpriseSsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1enterpriseSsid - errors', () => {
      it('should have a postDnaintentapiv1enterpriseSsid function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1enterpriseSsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1enterpriseSsid(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1enterpriseSsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1enterpriseSsidssidName - errors', () => {
      it('should have a deleteDnaintentapiv1enterpriseSsidssidName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1enterpriseSsidssidName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssidName', (done) => {
        try {
          a.deleteDnaintentapiv1enterpriseSsidssidName(null, (data, error) => {
            try {
              const displayE = 'ssidName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1enterpriseSsidssidName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1clientDetail - errors', () => {
      it('should have a getDnaintentapiv1clientDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1clientDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getDnaintentapiv1clientDetail(null, null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1clientDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macAddress', (done) => {
        try {
          a.getDnaintentapiv1clientDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'macAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1clientDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1clientHealth - errors', () => {
      it('should have a getDnaintentapiv1clientHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1clientHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getDnaintentapiv1clientHealth(null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1clientHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnaintentapiv1businesssdaborderDevicedeviceIpAddress - errors', () => {
      it('should have a getDnaintentapiv1businesssdaborderDevicedeviceIpAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getDnaintentapiv1businesssdaborderDevicedeviceIpAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdaborderdevice', (done) => {
        try {
          a.getDnaintentapiv1businesssdaborderDevicedeviceIpAddress(null, null, (data, error) => {
            try {
              const displayE = 'sdaborderdevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1businesssdaborderDevicedeviceIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceipaddress', (done) => {
        try {
          a.getDnaintentapiv1businesssdaborderDevicedeviceIpAddress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceipaddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDnaintentapiv1businesssdaborderDevicedeviceIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress - errors', () => {
      it('should have a deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdaborderdevice', (done) => {
        try {
          a.deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sdaborderdevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceipaddress', (done) => {
        try {
          a.deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceipaddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDnaintentapiv1businesssdaborderDevice - errors', () => {
      it('should have a postDnaintentapiv1businesssdaborderDevice function', (done) => {
        try {
          assert.equal(true, typeof a.postDnaintentapiv1businesssdaborderDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDnaintentapiv1businesssdaborderDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businesssdaborderDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdaborderdevice', (done) => {
        try {
          a.postDnaintentapiv1businesssdaborderDevice('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sdaborderdevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postDnaintentapiv1businesssdaborderDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should have a deletePortAssignmentForAccessPointInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortAssignmentForAccessPointInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deletePortAssignmentForAccessPointInSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePortAssignmentForAccessPointInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.deletePortAssignmentForAccessPointInSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePortAssignmentForAccessPointInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should have a getPortAssignmentForAccessPointInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getPortAssignmentForAccessPointInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getPortAssignmentForAccessPointInSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPortAssignmentForAccessPointInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.getPortAssignmentForAccessPointInSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPortAssignmentForAccessPointInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPortAssignmentForAccessPointInSDAFabric - errors', () => {
      it('should have a addPortAssignmentForAccessPointInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addPortAssignmentForAccessPointInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addPortAssignmentForAccessPointInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addPortAssignmentForAccessPointInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInfoFromSDAFabric - errors', () => {
      it('should have a getDeviceInfoFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceInfoFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getDeviceInfoFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceInfoFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitPeerNetworkInfo - errors', () => {
      it('should have a getTransitPeerNetworkInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitPeerNetworkInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitPeerNetworkName', (done) => {
        try {
          a.getTransitPeerNetworkInfo(null, (data, error) => {
            try {
              const displayE = 'transitPeerNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getTransitPeerNetworkInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTransitPeerNetwork - errors', () => {
      it('should have a addTransitPeerNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.addTransitPeerNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addTransitPeerNetwork(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addTransitPeerNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitPeerNetwork - errors', () => {
      it('should have a deleteTransitPeerNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitPeerNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitPeerNetworkName', (done) => {
        try {
          a.deleteTransitPeerNetwork(null, (data, error) => {
            try {
              const displayE = 'transitPeerNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteTransitPeerNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEdgeDeviceFromSDAFabric - errors', () => {
      it('should have a deleteEdgeDeviceFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEdgeDeviceFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deleteEdgeDeviceFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteEdgeDeviceFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEdgeDeviceFromSDAFabric - errors', () => {
      it('should have a getEdgeDeviceFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getEdgeDeviceFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getEdgeDeviceFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getEdgeDeviceFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addEdgeDeviceInSDAFabric - errors', () => {
      it('should have a addEdgeDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addEdgeDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addEdgeDeviceInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addEdgeDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIPPoolInSDAVirtualNetwork - errors', () => {
      it('should have a addIPPoolInSDAVirtualNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.addIPPoolInSDAVirtualNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addIPPoolInSDAVirtualNetwork(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addIPPoolInSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPPoolFromSDAVirtualNetwork - errors', () => {
      it('should have a deleteIPPoolFromSDAVirtualNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPPoolFromSDAVirtualNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.deleteIPPoolFromSDAVirtualNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.deleteIPPoolFromSDAVirtualNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipPoolName', (done) => {
        try {
          a.deleteIPPoolFromSDAVirtualNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipPoolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPPoolFromSDAVirtualNetwork - errors', () => {
      it('should have a getIPPoolFromSDAVirtualNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getIPPoolFromSDAVirtualNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getIPPoolFromSDAVirtualNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.getIPPoolFromSDAVirtualNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipPoolName', (done) => {
        try {
          a.getIPPoolFromSDAVirtualNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipPoolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getIPPoolFromSDAVirtualNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMulticastFromSDAFabric - errors', () => {
      it('should have a deleteMulticastFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMulticastFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.deleteMulticastFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteMulticastFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMulticastDetailsFromSDAFabric - errors', () => {
      it('should have a getMulticastDetailsFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getMulticastDetailsFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getMulticastDetailsFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getMulticastDetailsFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMulticastInSDAFabric - errors', () => {
      it('should have a addMulticastInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addMulticastInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addMulticastInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addMulticastInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkFromSDAFabric - errors', () => {
      it('should have a getVirtualNetworkFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualNetworkFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.getVirtualNetworkFromSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getVirtualNetworkFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getVirtualNetworkFromSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getVirtualNetworkFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVNInFabric - errors', () => {
      it('should have a addVNInFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addVNInFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addVNInFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addVNInFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVNFromSDAFabric - errors', () => {
      it('should have a deleteVNFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVNFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.deleteVNFromSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteVNFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.deleteVNFromSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteVNFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultAuthenticationProfileFromSDAFabric - errors', () => {
      it('should have a deleteDefaultAuthenticationProfileFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultAuthenticationProfileFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.deleteDefaultAuthenticationProfileFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDefaultAuthenticationProfileFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDefaultAuthenticationProfileInSDAFabric - errors', () => {
      it('should have a updateDefaultAuthenticationProfileInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.updateDefaultAuthenticationProfileInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateDefaultAuthenticationProfileInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateDefaultAuthenticationProfileInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultAuthenticationProfileFromSDAFabric - errors', () => {
      it('should have a getDefaultAuthenticationProfileFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultAuthenticationProfileFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getDefaultAuthenticationProfileFromSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDefaultAuthenticationProfileFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDefaultAuthenticationTemplateInSDAFabric - errors', () => {
      it('should have a addDefaultAuthenticationTemplateInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addDefaultAuthenticationTemplateInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addDefaultAuthenticationTemplateInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addDefaultAuthenticationTemplateInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteFromSDAFabric - errors', () => {
      it('should have a deleteSiteFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSiteFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.deleteSiteFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteSiteFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteFromSDAFabric - errors', () => {
      it('should have a getSiteFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getSiteFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getSiteFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSiteInSDAFabric - errors', () => {
      it('should have a addSiteInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addSiteInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addSiteInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addSiteInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reProvisionWiredDevice - errors', () => {
      it('should have a reProvisionWiredDevice function', (done) => {
        try {
          assert.equal(true, typeof a.reProvisionWiredDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.reProvisionWiredDevice(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-reProvisionWiredDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionWiredDevice - errors', () => {
      it('should have a provisionWiredDevice function', (done) => {
        try {
          assert.equal(true, typeof a.provisionWiredDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.provisionWiredDevice(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-provisionWiredDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProvisionedWiredDevice - errors', () => {
      it('should have a getProvisionedWiredDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getProvisionedWiredDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getProvisionedWiredDevice(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getProvisionedWiredDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProvisionedWiredDevice - errors', () => {
      it('should have a deleteProvisionedWiredDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProvisionedWiredDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deleteProvisionedWiredDevice(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteProvisionedWiredDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkSummary - errors', () => {
      it('should have a getVirtualNetworkSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualNetworkSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getVirtualNetworkSummary(null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getVirtualNetworkSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceRoleInSDAFabric - errors', () => {
      it('should have a getDeviceRoleInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceRoleInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getDeviceRoleInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceRoleInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should have a addPortAssignmentForUserDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addPortAssignmentForUserDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addPortAssignmentForUserDeviceInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addPortAssignmentForUserDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should have a getPortAssignmentForUserDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getPortAssignmentForUserDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getPortAssignmentForUserDeviceInSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPortAssignmentForUserDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.getPortAssignmentForUserDeviceInSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPortAssignmentForUserDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortAssignmentForUserDeviceInSDAFabric - errors', () => {
      it('should have a deletePortAssignmentForUserDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortAssignmentForUserDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deletePortAssignmentForUserDeviceInSDAFabric(null, null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePortAssignmentForUserDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.deletePortAssignmentForUserDeviceInSDAFabric('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePortAssignmentForUserDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBorderDeviceDetailFromSDAFabric - errors', () => {
      it('should have a getBorderDeviceDetailFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getBorderDeviceDetailFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getBorderDeviceDetailFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getBorderDeviceDetailFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBorderDeviceInSDAFabric - errors', () => {
      it('should have a addBorderDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addBorderDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addBorderDeviceInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addBorderDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBorderDeviceFromSDAFabric - errors', () => {
      it('should have a deleteBorderDeviceFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBorderDeviceFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deleteBorderDeviceFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteBorderDeviceFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControlPlaneDeviceFromSDAFabric - errors', () => {
      it('should have a getControlPlaneDeviceFromSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.getControlPlaneDeviceFromSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.getControlPlaneDeviceFromSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getControlPlaneDeviceFromSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addControlPlaneDeviceInSDAFabric - errors', () => {
      it('should have a addControlPlaneDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.addControlPlaneDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addControlPlaneDeviceInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addControlPlaneDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControlPlaneDeviceInSDAFabric - errors', () => {
      it('should have a deleteControlPlaneDeviceInSDAFabric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteControlPlaneDeviceInSDAFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceManagementIpAddress', (done) => {
        try {
          a.deleteControlPlaneDeviceInSDAFabric(null, (data, error) => {
            try {
              const displayE = 'deviceManagementIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteControlPlaneDeviceInSDAFabric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVirtualNetworkWithScalableGroups - errors', () => {
      it('should have a updateVirtualNetworkWithScalableGroups function', (done) => {
        try {
          assert.equal(true, typeof a.updateVirtualNetworkWithScalableGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateVirtualNetworkWithScalableGroups(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateVirtualNetworkWithScalableGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualNetworkWithScalableGroups - errors', () => {
      it('should have a deleteVirtualNetworkWithScalableGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualNetworkWithScalableGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.deleteVirtualNetworkWithScalableGroups(null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteVirtualNetworkWithScalableGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualNetworkWithScalableGroups - errors', () => {
      it('should have a getVirtualNetworkWithScalableGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualNetworkWithScalableGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualNetworkName', (done) => {
        try {
          a.getVirtualNetworkWithScalableGroups(null, (data, error) => {
            try {
              const displayE = 'virtualNetworkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getVirtualNetworkWithScalableGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVirtualNetworkWithScalableGroups - errors', () => {
      it('should have a addVirtualNetworkWithScalableGroups function', (done) => {
        try {
          assert.equal(true, typeof a.addVirtualNetworkWithScalableGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addVirtualNetworkWithScalableGroups(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addVirtualNetworkWithScalableGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetsV2 - errors', () => {
      it('should have a getApplicationSetsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationSetsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.getApplicationSetsV2(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationSetsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getApplicationSetsV2('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationSetsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getApplicationSetsV2('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationSetsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationSetsV2 - errors', () => {
      it('should have a createApplicationSetsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationSetsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationSetsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createApplicationSetsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationPolicyQueuingProfile - errors', () => {
      it('should have a deleteApplicationPolicyQueuingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationPolicyQueuingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApplicationPolicyQueuingProfile(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplicationPolicyQueuingProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteApplicationPolicyQueuingProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplicationPolicyQueuingProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyQueuingProfile - errors', () => {
      it('should have a getApplicationPolicyQueuingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationPolicyQueuingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationPolicyQueuingProfile - errors', () => {
      it('should have a updateApplicationPolicyQueuingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationPolicyQueuingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApplicationPolicyQueuingProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateApplicationPolicyQueuingProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationPolicyQueuingProfile - errors', () => {
      it('should have a createApplicationPolicyQueuingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationPolicyQueuingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationPolicyQueuingProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createApplicationPolicyQueuingProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyDefault - errors', () => {
      it('should have a getApplicationPolicyDefault function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationPolicyDefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosDeviceInterfaceInfo - errors', () => {
      it('should have a getQosDeviceInterfaceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getQosDeviceInterfaceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQosDeviceInterfaceInfo - errors', () => {
      it('should have a updateQosDeviceInterfaceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.updateQosDeviceInterfaceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateQosDeviceInterfaceInfo(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateQosDeviceInterfaceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createQosDeviceInterfaceInfo - errors', () => {
      it('should have a createQosDeviceInterfaceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.createQosDeviceInterfaceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createQosDeviceInterfaceInfo(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createQosDeviceInterfaceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicy - errors', () => {
      it('should have a getApplicationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQosDeviceInterfaceInfo - errors', () => {
      it('should have a deleteQosDeviceInterfaceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteQosDeviceInterfaceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteQosDeviceInterfaceInfo(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteQosDeviceInterfaceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationsV2 - errors', () => {
      it('should have a getApplicationsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.getApplicationsV2(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getApplicationsV2('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getApplicationsV2('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editApplicationsV2 - errors', () => {
      it('should have a editApplicationsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.editApplicationsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editApplicationsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-editApplicationsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationV2 - errors', () => {
      it('should have a createApplicationV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createApplicationV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosDeviceInterfaceInfoCount - errors', () => {
      it('should have a getQosDeviceInterfaceInfoCount function', (done) => {
        try {
          assert.equal(true, typeof a.getQosDeviceInterfaceInfoCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetCountV2 - errors', () => {
      it('should have a getApplicationSetCountV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationSetCountV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scalableGroupType', (done) => {
        try {
          a.getApplicationSetCountV2(null, (data, error) => {
            try {
              const displayE = 'scalableGroupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationSetCountV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationV2 - errors', () => {
      it('should have a deleteApplicationV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApplicationV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplicationV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApplicationPolicyIntent - errors', () => {
      it('should have a postApplicationPolicyIntent function', (done) => {
        try {
          assert.equal(true, typeof a.postApplicationPolicyIntent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postApplicationPolicyIntent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postApplicationPolicyIntent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationSetV2 - errors', () => {
      it('should have a deleteApplicationSetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationSetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApplicationSetV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplicationSetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCountV2 - errors', () => {
      it('should have a getApplicationCountV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationCountV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scalableGroupType', (done) => {
        try {
          a.getApplicationCountV2(null, (data, error) => {
            try {
              const displayE = 'scalableGroupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getApplicationCountV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationPolicyQueuingProfileCount - errors', () => {
      it('should have a getApplicationPolicyQueuingProfileCount function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationPolicyQueuingProfileCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationsCount - errors', () => {
      it('should have a getApplicationsCount function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplications - errors', () => {
      it('should have a getApplications function', (done) => {
        try {
          assert.equal(true, typeof a.getApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editApplication - errors', () => {
      it('should have a editApplication function', (done) => {
        try {
          assert.equal(true, typeof a.editApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-editApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplication - errors', () => {
      it('should have a createApplication function', (done) => {
        try {
          assert.equal(true, typeof a.createApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplication - errors', () => {
      it('should have a deleteApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApplication(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSets - errors', () => {
      it('should have a getApplicationSets function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationSet - errors', () => {
      it('should have a createApplicationSet function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationSet(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createApplicationSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationSet - errors', () => {
      it('should have a deleteApplicationSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteApplicationSet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteApplicationSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationSetsCount - errors', () => {
      it('should have a getApplicationSetsCount function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationSetsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configureAccessPoints - errors', () => {
      it('should have a configureAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.configureAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.configureAccessPoints(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-configureAccessPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootAccessPoints - errors', () => {
      it('should have a rebootAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.rebootAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rebootAccessPoints(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-rebootAccessPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointConfiguration - errors', () => {
      it('should have a getAccessPointConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPointConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getAccessPointConfiguration(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAccessPointConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointRebootTaskResult - errors', () => {
      it('should have a getAccessPointRebootTaskResult function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPointRebootTaskResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPointConfigurationTaskResult - errors', () => {
      it('should have a getAccessPointConfigurationTaskResult function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPointConfigurationTaskResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getAccessPointConfigurationTaskResult(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAccessPointConfigurationTaskResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveRFProfiles - errors', () => {
      it('should have a retrieveRFProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveRFProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateRFProfile - errors', () => {
      it('should have a createOrUpdateRFProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateRFProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOrUpdateRFProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createOrUpdateRFProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRFProfiles - errors', () => {
      it('should have a deleteRFProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRFProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rfProfileName', (done) => {
        try {
          a.deleteRFProfiles(null, (data, error) => {
            try {
              const displayE = 'rfProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteRFProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pSKOverride - errors', () => {
      it('should have a pSKOverride function', (done) => {
        try {
          assert.equal(true, typeof a.pSKOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pSKOverride(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-pSKOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWirelessProfile - errors', () => {
      it('should have a getWirelessProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getWirelessProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWirelessProfile - errors', () => {
      it('should have a updateWirelessProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateWirelessProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateWirelessProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWirelessProfile - errors', () => {
      it('should have a createWirelessProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createWirelessProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createWirelessProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionUpdate - errors', () => {
      it('should have a provisionUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.provisionUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.provisionUpdate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-provisionUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provision - errors', () => {
      it('should have a provision function', (done) => {
        try {
          assert.equal(true, typeof a.provision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.provision(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-provision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensorTestResults - errors', () => {
      it('should have a getSensorTestResults function', (done) => {
        try {
          assert.equal(true, typeof a.getSensorTestResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEnterpriseSSID - errors', () => {
      it('should have a updateEnterpriseSSID function', (done) => {
        try {
          assert.equal(true, typeof a.updateEnterpriseSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateEnterpriseSSID(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateEnterpriseSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicInterface - errors', () => {
      it('should have a getDynamicInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getDynamicInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUpdateDynamicInterface - errors', () => {
      it('should have a createUpdateDynamicInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createUpdateDynamicInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUpdateDynamicInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createUpdateDynamicInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPProvision - errors', () => {
      it('should have a aPProvision function', (done) => {
        try {
          assert.equal(true, typeof a.aPProvision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.aPProvision(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-aPProvision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWirelessProfile - errors', () => {
      it('should have a deleteWirelessProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWirelessProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wirelessProfileName', (done) => {
        try {
          a.deleteWirelessProfile(null, null, (data, error) => {
            try {
              const displayE = 'wirelessProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteWirelessProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDynamicInterface - errors', () => {
      it('should have a deleteDynamicInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDynamicInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.deleteDynamicInterface(null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDynamicInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createACloneOfTheGivenTemplate - errors', () => {
      it('should have a createACloneOfTheGivenTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createACloneOfTheGivenTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createACloneOfTheGivenTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createACloneOfTheGivenTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.createACloneOfTheGivenTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createACloneOfTheGivenTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importTheTemplatesProvided - errors', () => {
      it('should have a importTheTemplatesProvided function', (done) => {
        try {
          assert.equal(true, typeof a.importTheTemplatesProvided === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectName', (done) => {
        try {
          a.importTheTemplatesProvided(null, null, null, (data, error) => {
            try {
              const displayE = 'projectName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importTheTemplatesProvided', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importTheTemplatesProvided('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importTheTemplatesProvided', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportTheProjectsForAGivenCriteria - errors', () => {
      it('should have a exportTheProjectsForAGivenCriteria function', (done) => {
        try {
          assert.equal(true, typeof a.exportTheProjectsForAGivenCriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.exportTheProjectsForAGivenCriteria(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-exportTheProjectsForAGivenCriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheDetailsOfAGivenProject - errors', () => {
      it('should have a getTheDetailsOfAGivenProject function', (done) => {
        try {
          assert.equal(true, typeof a.getTheDetailsOfAGivenProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getTheDetailsOfAGivenProject(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getTheDetailsOfAGivenProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectDetailsV2 - errors', () => {
      it('should have a getProjectDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDetailsV2 - errors', () => {
      it('should have a getTemplateDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportTheTemplatesForAGivenCriteria - errors', () => {
      it('should have a exportTheTemplatesForAGivenCriteria function', (done) => {
        try {
          assert.equal(true, typeof a.exportTheTemplatesForAGivenCriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.exportTheTemplatesForAGivenCriteria(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-exportTheTemplatesForAGivenCriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importTheProjectsProvided - errors', () => {
      it('should have a importTheProjectsProvided function', (done) => {
        try {
          assert.equal(true, typeof a.importTheProjectsProvided === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importTheProjectsProvided('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importTheProjectsProvided', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewsForAGivenViewGroup - errors', () => {
      it('should have a getViewsForAGivenViewGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getViewsForAGivenViewGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewGroupId', (done) => {
        try {
          a.getViewsForAGivenViewGroup(null, (data, error) => {
            try {
              const displayE = 'viewGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getViewsForAGivenViewGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewDetailsForAGivenViewGroupView - errors', () => {
      it('should have a getViewDetailsForAGivenViewGroupView function', (done) => {
        try {
          assert.equal(true, typeof a.getViewDetailsForAGivenViewGroupView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewGroupId', (done) => {
        try {
          a.getViewDetailsForAGivenViewGroupView(null, null, (data, error) => {
            try {
              const displayE = 'viewGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getViewDetailsForAGivenViewGroupView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewId', (done) => {
        try {
          a.getViewDetailsForAGivenViewGroupView('fakeparam', null, (data, error) => {
            try {
              const displayE = 'viewId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getViewDetailsForAGivenViewGroupView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAScheduledReport - errors', () => {
      it('should have a getAScheduledReport function', (done) => {
        try {
          assert.equal(true, typeof a.getAScheduledReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.getAScheduledReport(null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAScheduledReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAScheduledReport - errors', () => {
      it('should have a deleteAScheduledReport function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAScheduledReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.deleteAScheduledReport(null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteAScheduledReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfScheduledReports - errors', () => {
      it('should have a getListOfScheduledReports function', (done) => {
        try {
          assert.equal(true, typeof a.getListOfScheduledReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrScheduleAReport - errors', () => {
      it('should have a createOrScheduleAReport function', (done) => {
        try {
          assert.equal(true, typeof a.createOrScheduleAReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOrScheduleAReport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createOrScheduleAReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllViewGroups - errors', () => {
      it('should have a getAllViewGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAllViewGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExecutionDetailsForAGivenReport - errors', () => {
      it('should have a getAllExecutionDetailsForAGivenReport function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExecutionDetailsForAGivenReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.getAllExecutionDetailsForAGivenReport(null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAllExecutionDetailsForAGivenReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReportContent - errors', () => {
      it('should have a downloadReportContent function', (done) => {
        try {
          assert.equal(true, typeof a.downloadReportContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.downloadReportContent(null, null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-downloadReportContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.downloadReportContent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-downloadReportContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChassisDetailsForDevice - errors', () => {
      it('should have a getChassisDetailsForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getChassisDetailsForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getChassisDetailsForDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getChassisDetailsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUserDefinedFields - errors', () => {
      it('should have a getAllUserDefinedFields function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUserDefinedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUserDefinedField - errors', () => {
      it('should have a createUserDefinedField function', (done) => {
        try {
          assert.equal(true, typeof a.createUserDefinedField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUserDefinedField(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createUserDefinedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheDetailsOfPhysicalComponentsOfTheGivenDevice - errors', () => {
      it('should have a getTheDetailsOfPhysicalComponentsOfTheGivenDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getTheDetailsOfPhysicalComponentsOfTheGivenDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getTheDetailsOfPhysicalComponentsOfTheGivenDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getTheDetailsOfPhysicalComponentsOfTheGivenDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnPOEInterfaceDetailsForTheDevice - errors', () => {
      it('should have a returnPOEInterfaceDetailsForTheDevice function', (done) => {
        try {
          assert.equal(true, typeof a.returnPOEInterfaceDetailsForTheDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.returnPOEInterfaceDetailsForTheDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-returnPOEInterfaceDetailsForTheDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearMacAddressTable - errors', () => {
      it('should have a clearMacAddressTable function', (done) => {
        try {
          assert.equal(true, typeof a.clearMacAddressTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceUuid', (done) => {
        try {
          a.clearMacAddressTable(null, null, null, (data, error) => {
            try {
              const displayE = 'interfaceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-clearMacAddressTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.clearMacAddressTable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-clearMacAddressTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlannedAccessPointsForFloor - errors', () => {
      it('should have a getPlannedAccessPointsForFloor function', (done) => {
        try {
          assert.equal(true, typeof a.getPlannedAccessPointsForFloor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorId', (done) => {
        try {
          a.getPlannedAccessPointsForFloor(null, null, null, null, (data, error) => {
            try {
              const displayE = 'floorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPlannedAccessPointsForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePlannedAccessPointForFloor - errors', () => {
      it('should have a updatePlannedAccessPointForFloor function', (done) => {
        try {
          assert.equal(true, typeof a.updatePlannedAccessPointForFloor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorId', (done) => {
        try {
          a.updatePlannedAccessPointForFloor(null, null, (data, error) => {
            try {
              const displayE = 'floorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updatePlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePlannedAccessPointForFloor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updatePlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPlannedAccessPointForFloor - errors', () => {
      it('should have a createPlannedAccessPointForFloor function', (done) => {
        try {
          assert.equal(true, typeof a.createPlannedAccessPointForFloor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorId', (done) => {
        try {
          a.createPlannedAccessPointForFloor(null, null, (data, error) => {
            try {
              const displayE = 'floorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createPlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPlannedAccessPointForFloor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createPlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHealth - errors', () => {
      it('should have a getDeviceHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinecardDetails - errors', () => {
      it('should have a getLinecardDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getLinecardDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getLinecardDetails(null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLinecardDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInventoryInsightDeviceLinkMismatchAPI - errors', () => {
      it('should have a getInventoryInsightDeviceLinkMismatchAPI function', (done) => {
        try {
          assert.equal(true, typeof a.getInventoryInsightDeviceLinkMismatchAPI === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getInventoryInsightDeviceLinkMismatchAPI(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getInventoryInsightDeviceLinkMismatchAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.getInventoryInsightDeviceLinkMismatchAPI('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getInventoryInsightDeviceLinkMismatchAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePlannedAccessPointForFloor - errors', () => {
      it('should have a deletePlannedAccessPointForFloor function', (done) => {
        try {
          assert.equal(true, typeof a.deletePlannedAccessPointForFloor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorId', (done) => {
        try {
          a.deletePlannedAccessPointForFloor(null, null, (data, error) => {
            try {
              const displayE = 'floorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plannedAccessPointUuid', (done) => {
        try {
          a.deletePlannedAccessPointForFloor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'plannedAccessPointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deletePlannedAccessPointForFloor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStackDetailsForDevice - errors', () => {
      it('should have a getStackDetailsForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getStackDetailsForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getStackDetailsForDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getStackDetailsForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInterfaceStatsInfoV2 - errors', () => {
      it('should have a getDeviceInterfaceStatsInfoV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceInterfaceStatsInfoV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceInterfaceStatsInfoV2(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceInterfaceStatsInfoV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getDeviceInterfaceStatsInfoV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceInterfaceStatsInfoV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserDefinedField - errors', () => {
      it('should have a updateUserDefinedField function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserDefinedField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateUserDefinedField(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateUserDefinedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUserDefinedField('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateUserDefinedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserDefinedField - errors', () => {
      it('should have a deleteUserDefinedField function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserDefinedField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUserDefinedField(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteUserDefinedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInterfaceDetails - errors', () => {
      it('should have a updateInterfaceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.updateInterfaceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceUuid', (done) => {
        try {
          a.updateInterfaceDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'interfaceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateInterfaceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInterfaceDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateInterfaceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLegitOperationsForInterface - errors', () => {
      it('should have a getLegitOperationsForInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getLegitOperationsForInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceUuid', (done) => {
        try {
          a.getLegitOperationsForInterface(null, (data, error) => {
            try {
              const displayE = 'interfaceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLegitOperationsForInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSupervisorCardDetail - errors', () => {
      it('should have a getSupervisorCardDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getSupervisorCardDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getSupervisorCardDetail(null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getSupervisorCardDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserDefinedFieldToDevice - errors', () => {
      it('should have a addUserDefinedFieldToDevice function', (done) => {
        try {
          assert.equal(true, typeof a.addUserDefinedFieldToDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.addUserDefinedFieldToDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addUserDefinedFieldToDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUserDefinedFieldToDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addUserDefinedFieldToDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserDefinedFieldFromDevice - errors', () => {
      it('should have a removeUserDefinedFieldFromDevice function', (done) => {
        try {
          assert.equal(true, typeof a.removeUserDefinedFieldFromDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.removeUserDefinedFieldFromDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeUserDefinedFieldFromDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.removeUserDefinedFieldFromDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeUserDefinedFieldFromDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectedDeviceDetail - errors', () => {
      it('should have a getConnectedDeviceDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectedDeviceDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getConnectedDeviceDetail(null, null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getConnectedDeviceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceUuid', (done) => {
        try {
          a.getConnectedDeviceDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getConnectedDeviceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES - errors', () => {
      it('should have a returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES function', (done) => {
        try {
          assert.equal(true, typeof a.returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceManagementAddress - errors', () => {
      it('should have a updateDeviceManagementAddress function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceManagementAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceid', (done) => {
        try {
          a.updateDeviceManagementAddress(null, null, (data, error) => {
            try {
              const displayE = 'deviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateDeviceManagementAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceManagementAddress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateDeviceManagementAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlannedAccessPointsForBuilding - errors', () => {
      it('should have a getPlannedAccessPointsForBuilding function', (done) => {
        try {
          assert.equal(true, typeof a.getPlannedAccessPointsForBuilding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing buildingId', (done) => {
        try {
          a.getPlannedAccessPointsForBuilding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'buildingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getPlannedAccessPointsForBuilding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceEnrichmentDetails - errors', () => {
      it('should have a getDeviceEnrichmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceEnrichmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGlobalCredentialsV2 - errors', () => {
      it('should have a getAllGlobalCredentialsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllGlobalCredentialsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGlobalCredentialsV2 - errors', () => {
      it('should have a updateGlobalCredentialsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateGlobalCredentialsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGlobalCredentialsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateGlobalCredentialsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGlobalCredentialsV2 - errors', () => {
      it('should have a createGlobalCredentialsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createGlobalCredentialsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGlobalCredentialsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createGlobalCredentialsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalCredentialV2 - errors', () => {
      it('should have a deleteGlobalCredentialV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGlobalCredentialV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteGlobalCredentialV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteGlobalCredentialV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhookDestination - errors', () => {
      it('should have a getWebhookDestination function', (done) => {
        try {
          assert.equal(true, typeof a.getWebhookDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWebhookDestination - errors', () => {
      it('should have a updateWebhookDestination function', (done) => {
        try {
          assert.equal(true, typeof a.updateWebhookDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateWebhookDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateWebhookDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWebhookDestination - errors', () => {
      it('should have a createWebhookDestination function', (done) => {
        try {
          assert.equal(true, typeof a.createWebhookDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createWebhookDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createWebhookDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogDestination - errors', () => {
      it('should have a getSyslogDestination function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSyslogDestination - errors', () => {
      it('should have a updateSyslogDestination function', (done) => {
        try {
          assert.equal(true, typeof a.updateSyslogDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSyslogDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSyslogDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSyslogDestination - errors', () => {
      it('should have a createSyslogDestination function', (done) => {
        try {
          assert.equal(true, typeof a.createSyslogDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSyslogDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createSyslogDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSNMPDestination - errors', () => {
      it('should have a updateSNMPDestination function', (done) => {
        try {
          assert.equal(true, typeof a.updateSNMPDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSNMPDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSNMPDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSNMPDestination - errors', () => {
      it('should have a createSNMPDestination function', (done) => {
        try {
          assert.equal(true, typeof a.createSNMPDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSNMPDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createSNMPDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfNotifications - errors', () => {
      it('should have a getCountOfNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.getCountOfNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfEventSubscriptions - errors', () => {
      it('should have a getCountOfEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.getCountOfEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventIds', (done) => {
        try {
          a.getCountOfEventSubscriptions(null, (data, error) => {
            try {
              const displayE = 'eventIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getCountOfEventSubscriptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogSubscriptionDetails - errors', () => {
      it('should have a getSyslogSubscriptionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogSubscriptionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailSubscriptionDetails - errors', () => {
      it('should have a getEmailSubscriptionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getEmailSubscriptionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailEventSubscriptions - errors', () => {
      it('should have a getEmailEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.getEmailEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEmailEventSubscription - errors', () => {
      it('should have a updateEmailEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.updateEmailEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateEmailEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateEmailEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEmailEventSubscription - errors', () => {
      it('should have a createEmailEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.createEmailEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createEmailEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createEmailEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEvents - errors', () => {
      it('should have a getEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tags', (done) => {
        try {
          a.getEvents('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tags is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogSummary - errors', () => {
      it('should have a getAuditLogSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventSubscriptions - errors', () => {
      it('should have a getEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.getEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEventSubscriptions - errors', () => {
      it('should have a updateEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.updateEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateEventSubscriptions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateEventSubscriptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEventSubscriptions - errors', () => {
      it('should have a createEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.createEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createEventSubscriptions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createEventSubscriptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventSubscriptions - errors', () => {
      it('should have a deleteEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscriptions', (done) => {
        try {
          a.deleteEventSubscriptions(null, (data, error) => {
            try {
              const displayE = 'subscriptions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteEventSubscriptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogEventSubscriptions - errors', () => {
      it('should have a getSyslogEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSyslogEventSubscription - errors', () => {
      it('should have a updateSyslogEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.updateSyslogEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSyslogEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSyslogEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSyslogEventSubscription - errors', () => {
      it('should have a createSyslogEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.createSyslogEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSyslogEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createSyslogEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountOfEvents - errors', () => {
      it('should have a getCountOfEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getCountOfEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tags', (done) => {
        try {
          a.getCountOfEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tags is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getCountOfEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventArtifacts - errors', () => {
      it('should have a getEventArtifacts function', (done) => {
        try {
          assert.equal(true, typeof a.getEventArtifacts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmailDestination - errors', () => {
      it('should have a getEmailDestination function', (done) => {
        try {
          assert.equal(true, typeof a.getEmailDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEmailDestination - errors', () => {
      it('should have a updateEmailDestination function', (done) => {
        try {
          assert.equal(true, typeof a.updateEmailDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateEmailDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateEmailDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEmailDestination - errors', () => {
      it('should have a createEmailDestination function', (done) => {
        try {
          assert.equal(true, typeof a.createEmailDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createEmailDestination(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createEmailDestination', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotifications - errors', () => {
      it('should have a getNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.getNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogRecords - errors', () => {
      it('should have a getAuditLogRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestWebhookEventSubscriptions - errors', () => {
      it('should have a getRestWebhookEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestWebhookEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRestWebhookEventSubscription - errors', () => {
      it('should have a updateRestWebhookEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.updateRestWebhookEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRestWebhookEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateRestWebhookEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRestWebhookEventSubscription - errors', () => {
      it('should have a createRestWebhookEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.createRestWebhookEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRestWebhookEventSubscription(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createRestWebhookEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogParentRecords - errors', () => {
      it('should have a getAuditLogParentRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogParentRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventArtifactCount - errors', () => {
      it('should have a getEventArtifactCount function', (done) => {
        try {
          assert.equal(true, typeof a.getEventArtifactCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTypes - errors', () => {
      it('should have a getConnectorTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectorTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusAPIForEvents - errors', () => {
      it('should have a getStatusAPIForEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getStatusAPIForEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getStatusAPIForEvents(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getStatusAPIForEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestWebhookSubscriptionDetails - errors', () => {
      it('should have a getRestWebhookSubscriptionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getRestWebhookSubscriptionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSNMPDestination - errors', () => {
      it('should have a getSNMPDestination function', (done) => {
        try {
          assert.equal(true, typeof a.getSNMPDestination === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lANAutomationStopAndUpdateDevices - errors', () => {
      it('should have a lANAutomationStopAndUpdateDevices function', (done) => {
        try {
          assert.equal(true, typeof a.lANAutomationStopAndUpdateDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.lANAutomationStopAndUpdateDevices(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationStopAndUpdateDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.lANAutomationStopAndUpdateDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationStopAndUpdateDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lANAutomationStop - errors', () => {
      it('should have a lANAutomationStop function', (done) => {
        try {
          assert.equal(true, typeof a.lANAutomationStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.lANAutomationStop(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.lANAutomationStop('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lANAutomationDeviceUpdate - errors', () => {
      it('should have a lANAutomationDeviceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.lANAutomationDeviceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing feature', (done) => {
        try {
          a.lANAutomationDeviceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'feature is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationDeviceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.lANAutomationDeviceUpdate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationDeviceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLogById - errors', () => {
      it('should have a getLANAutomationLogById function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationLogById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLANAutomationLogById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLANAutomationLogById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationStatusById - errors', () => {
      it('should have a getLANAutomationStatusById function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationStatusById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLANAutomationStatusById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLANAutomationStatusById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLog - errors', () => {
      it('should have a getLANAutomationLog function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lANAutomationStart - errors', () => {
      it('should have a lANAutomationStart function', (done) => {
        try {
          assert.equal(true, typeof a.lANAutomationStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.lANAutomationStart(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-lANAutomationStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationStatus - errors', () => {
      it('should have a getLANAutomationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationSessionCount - errors', () => {
      it('should have a getLANAutomationSessionCount function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationSessionCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationLogsForIndividualDevices - errors', () => {
      it('should have a getLANAutomationLogsForIndividualDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationLogsForIndividualDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLANAutomationLogsForIndividualDevices(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLANAutomationLogsForIndividualDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getLANAutomationLogsForIndividualDevices('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLANAutomationLogsForIndividualDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANAutomationActiveSessions - errors', () => {
      it('should have a getLANAutomationActiveSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getLANAutomationActiveSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should have a editAuthenticationAndPolicyServerAccessConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.editAuthenticationAndPolicyServerAccessConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.editAuthenticationAndPolicyServerAccessConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-editAuthenticationAndPolicyServerAccessConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAuthenticationAndPolicyServerAccessConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-editAuthenticationAndPolicyServerAccessConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should have a deleteAuthenticationAndPolicyServerAccessConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticationAndPolicyServerAccessConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAuthenticationAndPolicyServerAccessConfiguration(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteAuthenticationAndPolicyServerAccessConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomPromptSupport - errors', () => {
      it('should have a getCustomPromptSupport function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomPromptSupport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCustomPrompt - errors', () => {
      it('should have a postCustomPrompt function', (done) => {
        try {
          assert.equal(true, typeof a.postCustomPrompt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCustomPrompt(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-postCustomPrompt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationAndPolicyServers - errors', () => {
      it('should have a getAuthenticationAndPolicyServers function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationAndPolicyServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAuthenticationAndPolicyServerAccessConfiguration - errors', () => {
      it('should have a addAuthenticationAndPolicyServerAccessConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.addAuthenticationAndPolicyServerAccessConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAuthenticationAndPolicyServerAccessConfiguration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addAuthenticationAndPolicyServerAccessConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoISEServerIntegrationStatus - errors', () => {
      it('should have a getCiscoISEServerIntegrationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoISEServerIntegrationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptCiscoISEServerCertificateForCiscoISEServerIntegration - errors', () => {
      it('should have a acceptCiscoISEServerCertificateForCiscoISEServerIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.acceptCiscoISEServerCertificateForCiscoISEServerIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.acceptCiscoISEServerCertificateForCiscoISEServerIntegration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-acceptCiscoISEServerCertificateForCiscoISEServerIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.acceptCiscoISEServerCertificateForCiscoISEServerIntegration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-acceptCiscoISEServerCertificateForCiscoISEServerIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createITSMIntegrationSetting - errors', () => {
      it('should have a createITSMIntegrationSetting function', (done) => {
        try {
          assert.equal(true, typeof a.createITSMIntegrationSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createITSMIntegrationSetting(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createITSMIntegrationSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getITSMIntegrationSettingById - errors', () => {
      it('should have a getITSMIntegrationSettingById function', (done) => {
        try {
          assert.equal(true, typeof a.getITSMIntegrationSettingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getITSMIntegrationSettingById(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getITSMIntegrationSettingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getITSMIntegrationSettingById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getITSMIntegrationSettingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateITSMIntegrationSetting - errors', () => {
      it('should have a updateITSMIntegrationSetting function', (done) => {
        try {
          assert.equal(true, typeof a.updateITSMIntegrationSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.updateITSMIntegrationSetting(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateITSMIntegrationSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateITSMIntegrationSetting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateITSMIntegrationSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteITSMIntegrationSetting - errors', () => {
      it('should have a deleteITSMIntegrationSetting function', (done) => {
        try {
          assert.equal(true, typeof a.deleteITSMIntegrationSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.deleteITSMIntegrationSetting(null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteITSMIntegrationSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllITSMIntegrationSettings - errors', () => {
      it('should have a getAllITSMIntegrationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllITSMIntegrationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAllITSMIntegrationSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAllITSMIntegrationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceFamilyIdentifiers - errors', () => {
      it('should have a getDeviceFamilyIdentifiers function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceFamilyIdentifiers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagAsGoldenImage - errors', () => {
      it('should have a tagAsGoldenImage function', (done) => {
        try {
          assert.equal(true, typeof a.tagAsGoldenImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.tagAsGoldenImage(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-tagAsGoldenImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGoldenTagStatusOfAnImage - errors', () => {
      it('should have a getGoldenTagStatusOfAnImage function', (done) => {
        try {
          assert.equal(true, typeof a.getGoldenTagStatusOfAnImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getGoldenTagStatusOfAnImage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getGoldenTagStatusOfAnImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceFamilyIdentifier', (done) => {
        try {
          a.getGoldenTagStatusOfAnImage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceFamilyIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getGoldenTagStatusOfAnImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceRole', (done) => {
        try {
          a.getGoldenTagStatusOfAnImage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceRole is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getGoldenTagStatusOfAnImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.getGoldenTagStatusOfAnImage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getGoldenTagStatusOfAnImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGoldenTagForImage - errors', () => {
      it('should have a removeGoldenTagForImage function', (done) => {
        try {
          assert.equal(true, typeof a.removeGoldenTagForImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.removeGoldenTagForImage(null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeGoldenTagForImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceFamilyIdentifier', (done) => {
        try {
          a.removeGoldenTagForImage('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceFamilyIdentifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeGoldenTagForImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceRole', (done) => {
        try {
          a.removeGoldenTagForImage('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceRole is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeGoldenTagForImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.removeGoldenTagForImage('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeGoldenTagForImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignDeviceCredentialToSiteV2 - errors', () => {
      it('should have a assignDeviceCredentialToSiteV2 function', (done) => {
        try {
          assert.equal(true, typeof a.assignDeviceCredentialToSiteV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.assignDeviceCredentialToSiteV2(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDeviceCredentialToSiteV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignDeviceCredentialToSiteV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDeviceCredentialToSiteV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkV2 - errors', () => {
      it('should have a updateNetworkV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.updateNetworkV2(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNetworkV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetworkV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNetworkV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkV2 - errors', () => {
      it('should have a createNetworkV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.createNetworkV2(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createNetworkV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createNetworkV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalPool - errors', () => {
      it('should have a getGlobalPool function', (done) => {
        try {
          assert.equal(true, typeof a.getGlobalPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGlobalPool - errors', () => {
      it('should have a updateGlobalPool function', (done) => {
        try {
          assert.equal(true, typeof a.updateGlobalPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGlobalPool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateGlobalPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGlobalPool - errors', () => {
      it('should have a createGlobalPool function', (done) => {
        try {
          assert.equal(true, typeof a.createGlobalPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGlobalPool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createGlobalPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalIPPool - errors', () => {
      it('should have a deleteGlobalIPPool function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGlobalIPPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteGlobalIPPool(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteGlobalIPPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceCredential - errors', () => {
      it('should have a deleteDeviceCredential function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDeviceCredential(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteDeviceCredential', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should have a getNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReserveIPSubpool - errors', () => {
      it('should have a updateReserveIPSubpool function', (done) => {
        try {
          assert.equal(true, typeof a.updateReserveIPSubpool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.updateReserveIPSubpool(null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateReserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateReserveIPSubpool('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateReserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateReserveIPSubpool('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateReserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reserveIPSubpool - errors', () => {
      it('should have a reserveIPSubpool function', (done) => {
        try {
          assert.equal(true, typeof a.reserveIPSubpool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.reserveIPSubpool(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-reserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.reserveIPSubpool('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-reserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReserveIPSubpool - errors', () => {
      it('should have a getReserveIPSubpool function', (done) => {
        try {
          assert.equal(true, typeof a.getReserveIPSubpool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getReserveIPSubpool(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getReserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSPProfile - errors', () => {
      it('should have a deleteSPProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSPProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spProfileName', (done) => {
        try {
          a.deleteSPProfile(null, (data, error) => {
            try {
              const displayE = 'spProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteSPProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignDeviceCredentialToSite - errors', () => {
      it('should have a assignDeviceCredentialToSite function', (done) => {
        try {
          assert.equal(true, typeof a.assignDeviceCredentialToSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.assignDeviceCredentialToSite(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDeviceCredentialToSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignDeviceCredentialToSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDeviceCredentialToSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCredentialDetails - errors', () => {
      it('should have a getDeviceCredentialDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCredentialDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceCredentials - errors', () => {
      it('should have a updateDeviceCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceCredentials(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateDeviceCredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceCredentials - errors', () => {
      it('should have a createDeviceCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceCredentials(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createDeviceCredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceProviderDetails - errors', () => {
      it('should have a getServiceProviderDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceProviderDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSPProfile - errors', () => {
      it('should have a updateSPProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateSPProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSPProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSPProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSPProfile - errors', () => {
      it('should have a createSPProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createSPProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSPProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createSPProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetwork - errors', () => {
      it('should have a updateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.updateNetwork(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetwork - errors', () => {
      it('should have a createNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.createNetwork(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseReserveIPSubpool - errors', () => {
      it('should have a releaseReserveIPSubpool function', (done) => {
        try {
          assert.equal(true, typeof a.releaseReserveIPSubpool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.releaseReserveIPSubpool(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-releaseReserveIPSubpool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should have a uploadFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nameSpace', (done) => {
        try {
          a.uploadFile(null, (data, error) => {
            try {
              const displayE = 'nameSpace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientProximity - errors', () => {
      it('should have a getClientProximity function', (done) => {
        try {
          assert.equal(true, typeof a.getClientProximity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getClientProximity(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getClientProximity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientEnrichmentDetails - errors', () => {
      it('should have a getClientEnrichmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getClientEnrichmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteCountV2 - errors', () => {
      it('should have a getSiteCountV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteCountV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveCancelAnImport - errors', () => {
      it('should have a importMapArchiveCancelAnImport function', (done) => {
        try {
          assert.equal(true, typeof a.importMapArchiveCancelAnImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing importContextUuid', (done) => {
        try {
          a.importMapArchiveCancelAnImport(null, (data, error) => {
            try {
              const displayE = 'importContextUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importMapArchiveCancelAnImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchivePerformImport - errors', () => {
      it('should have a importMapArchivePerformImport function', (done) => {
        try {
          assert.equal(true, typeof a.importMapArchivePerformImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing importContextUuid', (done) => {
        try {
          a.importMapArchivePerformImport(null, (data, error) => {
            try {
              const displayE = 'importContextUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importMapArchivePerformImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteV2 - errors', () => {
      it('should have a getSiteV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mapsSupportedAccessPoints - errors', () => {
      it('should have a mapsSupportedAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.mapsSupportedAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesThatAreAssignedToASite - errors', () => {
      it('should have a getDevicesThatAreAssignedToASite function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesThatAreAssignedToASite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDevicesThatAreAssignedToASite(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDevicesThatAreAssignedToASite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberType', (done) => {
        try {
          a.getDevicesThatAreAssignedToASite('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'memberType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDevicesThatAreAssignedToASite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveStartImport - errors', () => {
      it('should have a importMapArchiveStartImport function', (done) => {
        try {
          assert.equal(true, typeof a.importMapArchiveStartImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMapArchiveImportStatus - errors', () => {
      it('should have a importMapArchiveImportStatus function', (done) => {
        try {
          assert.equal(true, typeof a.importMapArchiveImportStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing importContextUuid', (done) => {
        try {
          a.importMapArchiveImportStatus(null, (data, error) => {
            try {
              const displayE = 'importContextUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-importMapArchiveImportStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignDevicesToSite - errors', () => {
      it('should have a assignDevicesToSite function', (done) => {
        try {
          assert.equal(true, typeof a.assignDevicesToSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.assignDevicesToSite(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDevicesToSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignDevicesToSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-assignDevicesToSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportMapArchive - errors', () => {
      it('should have a exportMapArchive function', (done) => {
        try {
          assert.equal(true, typeof a.exportMapArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteHierarchyUuid', (done) => {
        try {
          a.exportMapArchive(null, null, (data, error) => {
            try {
              const displayE = 'siteHierarchyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-exportMapArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.exportMapArchive('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-exportMapArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSite - errors', () => {
      it('should have a getSite function', (done) => {
        try {
          assert.equal(true, typeof a.getSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteCount - errors', () => {
      it('should have a getSiteCount function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembership - errors', () => {
      it('should have a getMembership function', (done) => {
        try {
          assert.equal(true, typeof a.getMembership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getMembership(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getMembership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSite - errors', () => {
      it('should have a updateSite function', (done) => {
        try {
          assert.equal(true, typeof a.updateSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.updateSite(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSite - errors', () => {
      it('should have a deleteSite function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.deleteSite(null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBusinessAPIExecutionDetails - errors', () => {
      it('should have a getBusinessAPIExecutionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getBusinessAPIExecutionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getBusinessAPIExecutionDetails(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getBusinessAPIExecutionDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeDevice - errors', () => {
      it('should have a authorizeDevice function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.authorizeDevice(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-authorizeDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificate - errors', () => {
      it('should have a importCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.importCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCertificateP12 - errors', () => {
      it('should have a importCertificateP12 function', (done) => {
        try {
          assert.equal(true, typeof a.importCertificateP12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXStatusForAllDevices - errors', () => {
      it('should have a getEoXStatusForAllDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getEoXStatusForAllDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXSummary - errors', () => {
      it('should have a getEoXSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getEoXSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEoXDetailsPerDevice - errors', () => {
      it('should have a getEoXDetailsPerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getEoXDetailsPerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getEoXDetailsPerDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getEoXDetailsPerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associate - errors', () => {
      it('should have a associate function', (done) => {
        try {
          assert.equal(true, typeof a.associate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileId', (done) => {
        try {
          a.associate(null, null, (data, error) => {
            try {
              const displayE = 'networkProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-associate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.associate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-associate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociate - errors', () => {
      it('should have a disassociate function', (done) => {
        try {
          assert.equal(true, typeof a.disassociate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkProfileId', (done) => {
        try {
          a.disassociate(null, null, (data, error) => {
            try {
              const displayE = 'networkProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-disassociate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.disassociate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-disassociate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNFVProfile - errors', () => {
      it('should have a getNFVProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getNFVProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNFVProfile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getNFVProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNFVProfile - errors', () => {
      it('should have a updateNFVProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateNFVProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateNFVProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNFVProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNFVProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateNFVProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNFVProfile - errors', () => {
      it('should have a deleteNFVProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNFVProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteNFVProfile(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteNFVProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#nFVProvisioningDetail - errors', () => {
      it('should have a nFVProvisioningDetail function', (done) => {
        try {
          assert.equal(true, typeof a.nFVProvisioningDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.nFVProvisioningDetail(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-nFVProvisioningDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNFVProfile - errors', () => {
      it('should have a createNFVProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createNFVProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNFVProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createNFVProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigurationArchiveDetails - errors', () => {
      it('should have a getConfigurationArchiveDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigurationArchiveDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDeviceConfigurations - errors', () => {
      it('should have a exportDeviceConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.exportDeviceConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.exportDeviceConfigurations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-exportDeviceConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesSummary - errors', () => {
      it('should have a getAdvisoriesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAdvisoriesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesList - errors', () => {
      it('should have a getAdvisoriesList function', (done) => {
        try {
          assert.equal(true, typeof a.getAdvisoriesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoriesPerDevice - errors', () => {
      it('should have a getAdvisoriesPerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAdvisoriesPerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getAdvisoriesPerDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAdvisoriesPerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvisoryIDsPerDevice - errors', () => {
      it('should have a getAdvisoryIDsPerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAdvisoryIDsPerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getAdvisoryIDsPerDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getAdvisoryIDsPerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesPerAdvisory - errors', () => {
      it('should have a getDevicesPerAdvisory function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesPerAdvisory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing advisoryId', (done) => {
        try {
          a.getDevicesPerAdvisory(null, (data, error) => {
            try {
              const displayE = 'advisoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDevicesPerAdvisory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetailCount - errors', () => {
      it('should have a getComplianceDetailCount function', (done) => {
        try {
          assert.equal(true, typeof a.getComplianceDetailCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetailsOfDevice - errors', () => {
      it('should have a getComplianceDetailsOfDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getComplianceDetailsOfDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getComplianceDetailsOfDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getComplianceDetailsOfDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitDeviceConfiguration - errors', () => {
      it('should have a commitDeviceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.commitDeviceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.commitDeviceConfiguration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-commitDeviceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComplianceStatus - errors', () => {
      it('should have a getDeviceComplianceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceComplianceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getDeviceComplianceStatus(null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceComplianceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceStatusCount - errors', () => {
      it('should have a getComplianceStatusCount function', (done) => {
        try {
          assert.equal(true, typeof a.getComplianceStatusCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceStatus - errors', () => {
      it('should have a getComplianceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getComplianceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComplianceDetail - errors', () => {
      it('should have a getComplianceDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getComplianceDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runCompliance - errors', () => {
      it('should have a runCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.runCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runCompliance(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-runCompliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseUsageDetails - errors', () => {
      it('should have a getLicenseUsageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseUsageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smartAccountId', (done) => {
        try {
          a.getLicenseUsageDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'smartAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualAccountName', (done) => {
        try {
          a.getLicenseUsageDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualAccountName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.getLicenseUsageDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseUsageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLicenseSummary - errors', () => {
      it('should have a getDeviceLicenseSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceLicenseSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.getDeviceLicenseSummary(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceLicenseSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing order', (done) => {
        try {
          a.getDeviceLicenseSummary('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'order is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceLicenseSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getDeviceLicenseSummary('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceLicenseSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualAccountDetails - errors', () => {
      it('should have a getVirtualAccountDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualAccountDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smartAccountId', (done) => {
        try {
          a.getVirtualAccountDetails(null, (data, error) => {
            try {
              const displayE = 'smartAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getVirtualAccountDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deviceDeregistration - errors', () => {
      it('should have a deviceDeregistration function', (done) => {
        try {
          assert.equal(true, typeof a.deviceDeregistration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deviceDeregistration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deviceDeregistration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCountDetails - errors', () => {
      it('should have a getDeviceCountDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCountDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deviceRegistration - errors', () => {
      it('should have a deviceRegistration function', (done) => {
        try {
          assert.equal(true, typeof a.deviceRegistration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualAccountName', (done) => {
        try {
          a.deviceRegistration(null, null, (data, error) => {
            try {
              const displayE = 'virtualAccountName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deviceRegistration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deviceRegistration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deviceRegistration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAccountDetails - errors', () => {
      it('should have a getSmartAccountDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getSmartAccountDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeVirtualAccount - errors', () => {
      it('should have a changeVirtualAccount function', (done) => {
        try {
          assert.equal(true, typeof a.changeVirtualAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smartAccountId', (done) => {
        try {
          a.changeVirtualAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'smartAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-changeVirtualAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualAccountName', (done) => {
        try {
          a.changeVirtualAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualAccountName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-changeVirtualAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeVirtualAccount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-changeVirtualAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLicenseDetails - errors', () => {
      it('should have a getDeviceLicenseDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceLicenseDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUuid', (done) => {
        try {
          a.getDeviceLicenseDetails(null, (data, error) => {
            try {
              const displayE = 'deviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getDeviceLicenseDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseTermDetails - errors', () => {
      it('should have a getLicenseTermDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseTermDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smartAccountId', (done) => {
        try {
          a.getLicenseTermDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'smartAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseTermDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualAccountName', (done) => {
        try {
          a.getLicenseTermDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualAccountName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseTermDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.getLicenseTermDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getLicenseTermDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployDeviceReplacementWorkflow - errors', () => {
      it('should have a deployDeviceReplacementWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deployDeviceReplacementWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deployDeviceReplacementWorkflow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deployDeviceReplacementWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnListOfReplacementDevicesWithReplacementDetails - errors', () => {
      it('should have a returnListOfReplacementDevicesWithReplacementDetails function', (done) => {
        try {
          assert.equal(true, typeof a.returnListOfReplacementDevicesWithReplacementDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unMarkDeviceForReplacement - errors', () => {
      it('should have a unMarkDeviceForReplacement function', (done) => {
        try {
          assert.equal(true, typeof a.unMarkDeviceForReplacement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.unMarkDeviceForReplacement(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-unMarkDeviceForReplacement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markDeviceForReplacement - errors', () => {
      it('should have a markDeviceForReplacement function', (done) => {
        try {
          assert.equal(true, typeof a.markDeviceForReplacement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.markDeviceForReplacement(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-markDeviceForReplacement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnReplacementDevicesCount - errors', () => {
      it('should have a returnReplacementDevicesCount function', (done) => {
        try {
          assert.equal(true, typeof a.returnReplacementDevicesCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemHealthCount - errors', () => {
      it('should have a getSystemHealthCount function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemHealthCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemHealth - errors', () => {
      it('should have a getSystemHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemPerformanceHistorical - errors', () => {
      it('should have a getSystemPerformanceHistorical function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemPerformanceHistorical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemPerformanceAPI - errors', () => {
      it('should have a getSystemPerformanceAPI function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemPerformanceAPI === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRole - errors', () => {
      it('should have a updateRole function', (done) => {
        try {
          assert.equal(true, typeof a.updateRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRole - errors', () => {
      it('should have a addRole function', (done) => {
        try {
          assert.equal(true, typeof a.addRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should have a getUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing invokeSource', (done) => {
        try {
          a.getUsers(null, null, (data, error) => {
            try {
              const displayE = 'invokeSource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUser - errors', () => {
      it('should have a addUser function', (done) => {
        try {
          assert.equal(true, typeof a.addUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should have a getRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissions - errors', () => {
      it('should have a getPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalAuthenticationServers - errors', () => {
      it('should have a getExternalAuthenticationServers function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalAuthenticationServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing invokeSource', (done) => {
        try {
          a.getExternalAuthenticationServers(null, (data, error) => {
            try {
              const displayE = 'invokeSource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getExternalAuthenticationServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRole - errors', () => {
      it('should have a deleteRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.deleteRole(null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-deleteRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterReleaseSummary - errors', () => {
      it('should have a getCiscoDNACenterReleaseSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoDNACenterReleaseSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterNodesConfigurationSummary - errors', () => {
      it('should have a getCiscoDNACenterNodesConfigurationSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoDNACenterNodesConfigurationSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoDNACenterPackagesSummary - errors', () => {
      it('should have a getCiscoDNACenterPackagesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoDNACenterPackagesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssues - errors', () => {
      it('should have a getIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueEnrichmentDetails - errors', () => {
      it('should have a getIssueEnrichmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueEnrichmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#executeSuggestedActionsCommands - errors', () => {
      it('should have a executeSuggestedActionsCommands function', (done) => {
        try {
          assert.equal(true, typeof a.executeSuggestedActionsCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.executeSuggestedActionsCommands(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-executeSuggestedActionsCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensors - errors', () => {
      it('should have a getSensors function', (done) => {
        try {
          assert.equal(true, typeof a.getSensors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSensorTestTemplate - errors', () => {
      it('should have a createSensorTestTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createSensorTestTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSensorTestTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-createSensorTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSensorTest - errors', () => {
      it('should have a deleteSensorTest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSensorTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#duplicateSensorTestTemplate - errors', () => {
      it('should have a duplicateSensorTestTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.duplicateSensorTestTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.duplicateSensorTestTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-duplicateSensorTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSensorTestTemplate - errors', () => {
      it('should have a editSensorTestTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.editSensorTestTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSensorTestTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-editSensorTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runNowSensorTest - errors', () => {
      it('should have a runNowSensorTest function', (done) => {
        try {
          assert.equal(true, typeof a.runNowSensorTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runNowSensorTest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-runNowSensorTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addWLCToFabricDomain - errors', () => {
      it('should have a addWLCToFabricDomain function', (done) => {
        try {
          assert.equal(true, typeof a.addWLCToFabricDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addWLCToFabricDomain(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addWLCToFabricDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeWLCFromFabricDomain - errors', () => {
      it('should have a removeWLCFromFabricDomain function', (done) => {
        try {
          assert.equal(true, typeof a.removeWLCFromFabricDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIPAddress', (done) => {
        try {
          a.removeWLCFromFabricDomain(null, null, (data, error) => {
            try {
              const displayE = 'deviceIPAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeWLCFromFabricDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeWLCFromFabricDomain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-removeWLCFromFabricDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSIDToIPPoolMapping - errors', () => {
      it('should have a getSSIDToIPPoolMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getSSIDToIPPoolMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlanName', (done) => {
        try {
          a.getSSIDToIPPoolMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'vlanName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getSSIDToIPPoolMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteNameHierarchy', (done) => {
        try {
          a.getSSIDToIPPoolMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteNameHierarchy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getSSIDToIPPoolMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getSSIDToIPPoolMapping('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getSSIDToIPPoolMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSSIDToIPPoolMapping - errors', () => {
      it('should have a updateSSIDToIPPoolMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateSSIDToIPPoolMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSSIDToIPPoolMapping(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-updateSSIDToIPPoolMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSSIDToIPPoolMapping - errors', () => {
      it('should have a addSSIDToIPPoolMapping function', (done) => {
        try {
          assert.equal(true, typeof a.addSSIDToIPPoolMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSSIDToIPPoolMapping(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-addSSIDToIPPoolMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationHealth - errors', () => {
      it('should have a getApplicationHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCMDBSyncStatus - errors', () => {
      it('should have a getCMDBSyncStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCMDBSyncStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getCMDBSyncStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-getCMDBSyncStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFailedITSMEvents - errors', () => {
      it('should have a getFailedITSMEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getFailedITSMEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retryIntegrationEvents - errors', () => {
      it('should have a retryIntegrationEvents function', (done) => {
        try {
          assert.equal(true, typeof a.retryIntegrationEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.retryIntegrationEvents(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-dna_center-adapter-retryIntegrationEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEnrichmentDetails - errors', () => {
      it('should have a getUserEnrichmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getUserEnrichmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
