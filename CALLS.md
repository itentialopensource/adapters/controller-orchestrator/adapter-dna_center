## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco DNA Center. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco DNA Center.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco DNA Center. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1templateProgrammerproject(request, ContentType, callback)</td>
    <td style="padding:15px">Create Project</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1templateProgrammerproject(name, callback)</td>
    <td style="padding:15px">Get Projects</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1templateProgrammerproject(request, ContentType, callback)</td>
    <td style="padding:15px">Update Project</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1templateProgrammertemplate(projectId, softwareType, softwareVersion, productFamily, productSeries, productType, filterConflictingTemplates, callback)</td>
    <td style="padding:15px">Gets the templates available</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1templateProgrammertemplate(request, ContentType, callback)</td>
    <td style="padding:15px">Update Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1templateProgrammertemplatedeploy(request, ContentType, callback)</td>
    <td style="padding:15px">Deploy Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1templateProgrammertemplateversion(request, ContentType, callback)</td>
    <td style="padding:15px">Version Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1templateProgrammertemplatetemplateId(templateId, latestVersion, callback)</td>
    <td style="padding:15px">Get Template Details</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1templateProgrammertemplatetemplateId(templateId, callback)</td>
    <td style="padding:15px">Delete Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1templateProgrammertemplatedeploystatusdeploymentId(deploymentId, callback)</td>
    <td style="padding:15px">Get Template deployment status</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/deploy/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1templateProgrammerprojectprojectId(projectId, callback)</td>
    <td style="padding:15px">Delete Project</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1templateProgrammertemplateversiontemplateId(templateId, callback)</td>
    <td style="padding:15px">Get Template Versions</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/version/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1templateProgrammertemplatepreview(request, ContentType, callback)</td>
    <td style="padding:15px">Preview Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1templateProgrammerprojectprojectIdtemplate(request, ContentType, projectId, callback)</td>
    <td style="padding:15px">Create Template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/{pathv1}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1tagidmember(request, ContentType, id, callback)</td>
    <td style="padding:15px">Add members to the tag</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tagidmember(id, memberType, offset, limit, memberAssociationType, level, callback)</td>
    <td style="padding:15px">Get Tag members by Id</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1tag(request, ContentType, callback)</td>
    <td style="padding:15px">Create Tag</td>
    <td style="padding:15px">{base_path}/{version}/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1tag(request, ContentType, callback)</td>
    <td style="padding:15px">Update Tag</td>
    <td style="padding:15px">{base_path}/{version}/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tag(name, additionalInfonameSpace, additionalInfoattributes, level, offset, limit, size, field, sortBy, order, systemTag, callback)</td>
    <td style="padding:15px">Get Tag</td>
    <td style="padding:15px">{base_path}/{version}/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tagidmembercount(id, memberType, memberAssociationType, level, callback)</td>
    <td style="padding:15px">Get Tag Member count</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}/member/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tagmembertype(callback)</td>
    <td style="padding:15px">Get Tag resource types</td>
    <td style="padding:15px">{base_path}/{version}/tag/member/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1tagmember(request, ContentType, callback)</td>
    <td style="padding:15px">Updates tag membership</td>
    <td style="padding:15px">{base_path}/{version}/tag/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1tagid(id, callback)</td>
    <td style="padding:15px">Delete Tag</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tagid(id, callback)</td>
    <td style="padding:15px">Get Tag by Id</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tagcount(name, nameSpace, attributeName, level, size, systemTag, callback)</td>
    <td style="padding:15px">Get Tag Count</td>
    <td style="padding:15px">{base_path}/{version}/tag/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1tagidmembermemberId(id, memberId, callback)</td>
    <td style="padding:15px">Remove Tag member</td>
    <td style="padding:15px">{base_path}/{version}/tag/{pathv1}/member/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoverycount(callback)</td>
    <td style="padding:15px">Get count of all discovery jobs</td>
    <td style="padding:15px">{base_path}/{version}/discovery/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialnetconf(request, ContentType, callback)</td>
    <td style="padding:15px">Create Netconf credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/netconf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialnetconf(request, ContentType, callback)</td>
    <td style="padding:15px">Update Netconf credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/netconf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialsnmpv2WriteCommunity(request, ContentType, callback)</td>
    <td style="padding:15px">Update SNMP write community</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv2-write-community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialsnmpv2WriteCommunity(request, ContentType, callback)</td>
    <td style="padding:15px">Create SNMP write community</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv2-write-community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialsnmpv3(request, ContentType, callback)</td>
    <td style="padding:15px">Update SNMPv3 credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialsnmpv3(request, ContentType, callback)</td>
    <td style="padding:15px">Create SNMPv3 credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryidsummary(id, taskId, sortBy, sortOrder, ipAddress, pingStatus, snmpStatus, cliStatus, netconfStatus, httpStatus, callback)</td>
    <td style="padding:15px">Get network devices from Discovery</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialhttpWrite(request, ContentType, callback)</td>
    <td style="padding:15px">Create HTTP write credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/http-write?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialhttpWrite(request, ContentType, callback)</td>
    <td style="padding:15px">Update HTTP write credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/http-write?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1snmpProperty(callback)</td>
    <td style="padding:15px">Get SNMP properties</td>
    <td style="padding:15px">{base_path}/{version}/snmp-property?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1snmpProperty(request, ContentType, callback)</td>
    <td style="padding:15px">Create/Update SNMP properties</td>
    <td style="padding:15px">{base_path}/{version}/snmp-property?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1discoveryid(id, callback)</td>
    <td style="padding:15px">Delete discovery by Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryid(id, callback)</td>
    <td style="padding:15px">Get Discovery by Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1discovery(request, ContentType, callback)</td>
    <td style="padding:15px">Start discovery</td>
    <td style="padding:15px">{base_path}/{version}/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1discovery(request, ContentType, callback)</td>
    <td style="padding:15px">Updates an existing discovery by specified Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1discovery(callback)</td>
    <td style="padding:15px">Delete all discovery</td>
    <td style="padding:15px">{base_path}/{version}/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialsnmpv2ReadCommunity(request, ContentType, callback)</td>
    <td style="padding:15px">Update SNMP read community</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv2-read-community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialsnmpv2ReadCommunity(request, ContentType, callback)</td>
    <td style="padding:15px">Create SNMP read community</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/snmpv2-read-community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoverystartIndexrecordsToReturn(startIndex, recordsToReturn, callback)</td>
    <td style="padding:15px">Get Discoveries by range</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialglobalCredentialId(request, ContentType, globalCredentialId, callback)</td>
    <td style="padding:15px">Update global credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1globalCredentialglobalCredentialId(globalCredentialId, callback)</td>
    <td style="padding:15px">Delete global credentials by Id</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1globalCredentialid(id, callback)</td>
    <td style="padding:15px">Get Credential sub type by credential Id</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialcli(request, ContentType, callback)</td>
    <td style="padding:15px">Create CLI credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialcli(request, ContentType, callback)</td>
    <td style="padding:15px">Update CLI credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1globalCredentialhttpRead(request, ContentType, callback)</td>
    <td style="padding:15px">Update HTTP read credential</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/http-read?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1globalCredentialhttpRead(request, ContentType, callback)</td>
    <td style="padding:15px">Create HTTP read credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/http-read?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryjob(offset, limit, ipAddress, name, callback)</td>
    <td style="padding:15px">Get Discovery jobs by IP</td>
    <td style="padding:15px">{base_path}/{version}/discovery/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryidnetworkDevicecount(id, taskId, callback)</td>
    <td style="padding:15px">Get Devices discovered by Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/network-device/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryidjob(id, offset, limit, ipAddress, callback)</td>
    <td style="padding:15px">Get list of discoveries by discovery Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryidnetworkDevicestartIndexrecordsToReturn(id, startIndex, recordsToReturn, taskId, callback)</td>
    <td style="padding:15px">Get Discovered devices by range</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/network-device/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1discoverystartIndexrecordsToDelete(startIndex, recordsToDelete, callback)</td>
    <td style="padding:15px">Delete discovery by specified range</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1discoveryidnetworkDevice(id, taskId, callback)</td>
    <td style="padding:15px">Get Discovered network devices by discovery Id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1globalCredential(credentialSubType, sortBy, order, callback)</td>
    <td style="padding:15px">Get Global credentials</td>
    <td style="padding:15px">{base_path}/{version}/global-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDeviceunclaim(request, ContentType, callback)</td>
    <td style="padding:15px">Un-Claim Device</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/unclaim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpDevicesacctdomainvacctnamesyncResult(domain, name, callback)</td>
    <td style="padding:15px">Get Sync Result for Virtual Account</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/sacct/{pathv1}/vacct/{pathv2}/sync-result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1onboardingpnpDeviceid(request, ContentType, id, callback)</td>
    <td style="padding:15px">Update Device</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpDeviceid(id, callback)</td>
    <td style="padding:15px">Get Device by Id</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1onboardingpnpDeviceid(id, callback)</td>
    <td style="padding:15px">Delete Device by Id from PnP</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpSettingssavacct(request, ContentType, callback)</td>
    <td style="padding:15px">Add Virtual Account</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings/savacct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1onboardingpnpSettingssavacct(request, ContentType, callback)</td>
    <td style="padding:15px">Update PnP Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings/savacct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDeviceimport(request, ContentType, callback)</td>
    <td style="padding:15px">Import Devices in bulk</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1onboardingpnpWorkflowid(request, ContentType, id, callback)</td>
    <td style="padding:15px">Update Workflow</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpWorkflowid(id, callback)</td>
    <td style="padding:15px">Get Workflow by Id</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1onboardingpnpWorkflowid(id, callback)</td>
    <td style="padding:15px">Delete Workflow By Id</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDevicesiteClaim(request, ContentType, callback)</td>
    <td style="padding:15px">Claim a Device to a Site</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/site-claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1onboardingpnpSettingsvacct(domain, name, callback)</td>
    <td style="padding:15px">Deregister Virtual Account</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings/vacct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpSettingssacct(callback)</td>
    <td style="padding:15px">Get Smart Account List</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings/sacct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpWorkflowcount(name, callback)</td>
    <td style="padding:15px">Get Workflow Count</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpSettingssacctdomainvacct(domain, callback)</td>
    <td style="padding:15px">Get Virtual Account List</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings/sacct/{pathv1}/vacct?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1onboardingpnpSettings(request, ContentType, callback)</td>
    <td style="padding:15px">Update PnP global settings</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpSettings(callback)</td>
    <td style="padding:15px">Get PnP global settings</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDevicereset(request, ContentType, callback)</td>
    <td style="padding:15px">Reset Device</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpWorkflow(request, ContentType, callback)</td>
    <td style="padding:15px">Add a Workflow</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpWorkflow(limit, offset, sort, sortOrder, type, name, callback)</td>
    <td style="padding:15px">Get Workflows</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDevicevacctSync(request, ContentType, callback)</td>
    <td style="padding:15px">Sync Virtual Account Devices</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/vacct-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpDevicecount(serialNumber, state, onbState, cmState, name, pid, source, projectId, workflowId, projectName, workflowName, smartAccountId, virtualAccountId, lastContact, callback)</td>
    <td style="padding:15px">Get Device Count</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpDevicehistory(serialNumber, sort, sortOrder, callback)</td>
    <td style="padding:15px">Get Device History</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDevicesiteConfigPreview(request, ContentType, callback)</td>
    <td style="padding:15px">Preview Config</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/site-config-preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDeviceclaim(request, ContentType, callback)</td>
    <td style="padding:15px">Claim Device</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1onboardingpnpDevice(limit, offset, sort, sortOrder, serialNumber, state, onbState, cmState, name, pid, source, projectId, workflowId, projectName, workflowName, smartAccountId, virtualAccountId, lastContact, callback)</td>
    <td style="padding:15px">Get Device list</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1onboardingpnpDevice(request, ContentType, callback)</td>
    <td style="padding:15px">Add Device</td>
    <td style="padding:15px">{base_path}/{version}/onboarding/pnp-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1imageimportation(imageUuid, name, family, applicationType, imageIntegrityStatus, version, imageSeries, imageName, isTaggedGolden, isCCORecommended, isCCOLatest, createdTime, imageSizeGreaterThan, imageSizeLesserThan, sortBy, sortOrder, limit, offset, callback)</td>
    <td style="padding:15px">Get software image details</td>
    <td style="padding:15px">{base_path}/{version}/image/importation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1imageimportationsourcefile(ContentType, isThirdParty, thirdPartyVendor, thirdPartyImageFamily, thirdPartyApplicationType, callback)</td>
    <td style="padding:15px">Import local software image</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/source/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1imagedistribution(request, ContentType, callback)</td>
    <td style="padding:15px">Trigger software image distribution</td>
    <td style="padding:15px">{base_path}/{version}/image/distribution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1imageimportationsourceurl(request, ContentType, scheduleAt, scheduleDesc, scheduleOrigin, callback)</td>
    <td style="padding:15px">Import software image via URL</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/source/url?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1imageactivationdevice(request, ContentType, ClientType, ClientUrl, scheduleValidate, callback)</td>
    <td style="padding:15px">Trigger software image activation</td>
    <td style="padding:15px">{base_path}/{version}/image/activation/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicemoduleid(id, callback)</td>
    <td style="padding:15px">Get Module Info by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/module/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceWithOptions(hostname, managementIpAddress, macAddress, locationName, serialNumber, location, family, type, series, collectionStatus, collectionInterval, notSyncedForMinutes, errorCode, errorDescription, softwareVersion, softwareType, platformId, role, reachabilityStatus, upTime, associatedWlcIp, licensename, licensetype, licensestatus, modulename, moduleequpimenttype, moduleservicestate, modulevendorequipmenttype, modulepartnumber, moduleoperationstatecode, id, offset, limit, callback)</td>
    <td style="padding:15px">Get Device list</td>
    <td style="padding:15px">{base_path}/{version}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1networkDevice(request, ContentType, callback)</td>
    <td style="padding:15px">Add Device</td>
    <td style="padding:15px">{base_path}/{version}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1networkDevice(request, ContentType, callback)</td>
    <td style="padding:15px">Sync Devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceidvlan(id, interfaceType, callback)</td>
    <td style="padding:15px">Get Device Interface VLANs</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfacenetworkDevicedeviceIdstartIndexrecordsToReturn(deviceId, startIndex, recordsToReturn, callback)</td>
    <td style="padding:15px">Get Device Interfaces by specified range</td>
    <td style="padding:15px">{base_path}/{version}/interface/network-device/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicecollectionScheduleglobal(callback)</td>
    <td style="padding:15px">Get Polling Interval for all devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device/collection-schedule/global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfacecount(callback)</td>
    <td style="padding:15px">Get Device Interface Count</td>
    <td style="padding:15px">{base_path}/{version}/interface/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1networkDeviceid(id, isForceDelete, callback)</td>
    <td style="padding:15px">Delete Device by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceid(id, callback)</td>
    <td style="padding:15px">Get Device by ID</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1networkDevicesync(request, ContentType, forceSync, callback)</td>
    <td style="padding:15px">Sync Devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicecount(callback)</td>
    <td style="padding:15px">Get Device Count</td>
    <td style="padding:15px">{base_path}/{version}/network-device/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceconfigcount(callback)</td>
    <td style="padding:15px">Get Device Config Count</td>
    <td style="padding:15px">{base_path}/{version}/network-device/config/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceidcollectionSchedule(id, callback)</td>
    <td style="padding:15px">Get Polling Interval by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/collection-schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfacenetworkDevicedeviceIdinterfaceName(deviceId, name, callback)</td>
    <td style="padding:15px">Get Interface details by device Id and interface name</td>
    <td style="padding:15px">{base_path}/{version}/interface/network-device/{pathv1}/interface-name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicemodulecount(deviceId, nameList, vendorEquipmentTypeList, partNumberList, operationalStateCodeList, callback)</td>
    <td style="padding:15px">Get Module count</td>
    <td style="padding:15px">{base_path}/{version}/network-device/module/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceidmerakiOrganization(id, callback)</td>
    <td style="padding:15px">Get Organization list for Meraki</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/meraki-organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfacenetworkDevicedeviceIdcount(deviceId, callback)</td>
    <td style="padding:15px">Get Device Interface count</td>
    <td style="padding:15px">{base_path}/{version}/interface/network-device/{pathv1}/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceidbrief(id, callback)</td>
    <td style="padding:15px">Get Device Summary</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/brief?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicenetworkDeviceIdconfig(networkDeviceId, callback)</td>
    <td style="padding:15px">Get Device Config by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfaceospf(callback)</td>
    <td style="padding:15px">Get OSPF interfaces</td>
    <td style="padding:15px">{base_path}/{version}/interface/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicefunctionalCapabilityid(id, callback)</td>
    <td style="padding:15px">Get Functional Capability by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/functional-capability/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfaceisis(callback)</td>
    <td style="padding:15px">Get ISIS interfaces</td>
    <td style="padding:15px">{base_path}/{version}/interface/isis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceconfig(callback)</td>
    <td style="padding:15px">Get Device Config for all devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfaceid(id, callback)</td>
    <td style="padding:15px">Get Interface by Id</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicefunctionalCapability(deviceId, functionName, callback)</td>
    <td style="padding:15px">Get Functional Capability for devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device/functional-capability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfacenetworkDevicedeviceId(deviceId, callback)</td>
    <td style="padding:15px">Get Interface info by Id</td>
    <td style="padding:15px">{base_path}/{version}/interface/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnaintentapiv1networkDevicebrief(request, ContentType, callback)</td>
    <td style="padding:15px">Update Device role</td>
    <td style="padding:15px">{base_path}/{version}/network-device/brief?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1networkDevicefile(request, ContentType, callback)</td>
    <td style="padding:15px">Export Device list</td>
    <td style="padding:15px">{base_path}/{version}/network-device/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicetenantinfomacaddress(serialNumber, macaddress, callback)</td>
    <td style="padding:15px">Register device for WSA</td>
    <td style="padding:15px">{base_path}/{version}/network-device/tenantinfo/macaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interfaceipAddressipAddress(ipAddress, callback)</td>
    <td style="padding:15px">Get Interface by IP</td>
    <td style="padding:15px">{base_path}/{version}/interface/ip-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceserialNumberserialNumber(serialNumber, callback)</td>
    <td style="padding:15px">Get Device by Serial number</td>
    <td style="padding:15px">{base_path}/{version}/network-device/serial-number/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceipAddressipAddress(ipAddress, callback)</td>
    <td style="padding:15px">Get Network Device by IP</td>
    <td style="padding:15px">{base_path}/{version}/network-device/ip-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicemodule(deviceId, limit, offset, nameList, vendorEquipmentTypeList, partNumberList, operationalStateCodeList, callback)</td>
    <td style="padding:15px">Get Modules</td>
    <td style="padding:15px">{base_path}/{version}/network-device/module?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicestartIndexrecordsToReturn(startIndex, recordsToReturn, callback)</td>
    <td style="padding:15px">Get Network Device by pagination range</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1deviceDetail(timestamp, searchBy, identifier, callback)</td>
    <td style="padding:15px">Get Device Detail</td>
    <td style="padding:15px">{base_path}/{version}/device-detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1interface(callback)</td>
    <td style="padding:15px">Get all interfaces</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceautocomplete(vrfName, managementIpAddress, hostname, macAddress, family, collectionStatus, collectionInterval, softwareVersion, softwareType, reachabilityStatus, reachabilityFailureReason, errorCode, platformId, series, type, serialNumber, upTime, role, roleSource, associatedWlcIp, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves all network devices</td>
    <td style="padding:15px">{base_path}/{version}/network-device/autocomplete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDeviceidwirelessInfo(id, callback)</td>
    <td style="padding:15px">Get wireless lan controller details by Id</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/wireless-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1siteHealth(timestamp, callback)</td>
    <td style="padding:15px">Get Site Health</td>
    <td style="padding:15px">{base_path}/{version}/site-health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1sitesiteIddevice(request, Runsync, Persistbapioutput, Timeout, siteId, callback)</td>
    <td style="padding:15px">Assign Device To Site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnasystemapiv1sitesiteIddevice(request, Runsync, Persistbapioutput, Timeout, siteId, callback)</td>
    <td style="padding:15px">Assign Device To Site</td>
    <td style="padding:15px">/dna/system/api/{version}/site/{pathv1}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1site(request, Runsync, Timeout, Persistbapioutput, callback)</td>
    <td style="padding:15px">Create Site</td>
    <td style="padding:15px">{base_path}/{version}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnasystemapiv1site(request, Runsync, Timeout, Persistbapioutput, callback)</td>
    <td style="padding:15px">Create Site</td>
    <td style="padding:15px">/dna/system/api/{version}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1taskcount(startTime, endTime, data, errorCode, serviceType, username, progress, isError, failureReason, parentId, callback)</td>
    <td style="padding:15px">Get task count</td>
    <td style="padding:15px">{base_path}/{version}/task/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tasktaskId(taskId, callback)</td>
    <td style="padding:15px">Get task by Id</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1task(startTime, endTime, data, errorCode, serviceType, username, progress, isError, failureReason, parentId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get tasks</td>
    <td style="padding:15px">{base_path}/{version}/task?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1tasktaskIdtree(taskId, callback)</td>
    <td style="padding:15px">Get task tree</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}/tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1taskoperationoperationIdoffsetlimit(operationId, offset, limit, callback)</td>
    <td style="padding:15px">Get task by OperationId</td>
    <td style="padding:15px">{base_path}/{version}/task/operation/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1filenamespace(callback)</td>
    <td style="padding:15px">Get list of available namespaces</td>
    <td style="padding:15px">{base_path}/{version}/file/namespace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1filenamespacenameSpace(nameSpace, callback)</td>
    <td style="padding:15px">Get list of files</td>
    <td style="padding:15px">{base_path}/{version}/file/namespace/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1filefileId(fileId, callback)</td>
    <td style="padding:15px">Download a file by fileId</td>
    <td style="padding:15px">{base_path}/{version}/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkDevicePollerclilegitReads(callback)</td>
    <td style="padding:15px">Get all keywords of CLIs accepted by command runner</td>
    <td style="padding:15px">{base_path}/{version}/network-device-poller/cli/legit-reads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1networkDevicePollerclireadRequest(request, ContentType, callback)</td>
    <td style="padding:15px">Run read-only commands on devices to get their real-time configuration</td>
    <td style="padding:15px">{base_path}/{version}/network-device-poller/cli/read-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1topologyvlanvlanNames(callback)</td>
    <td style="padding:15px">Get VLAN details</td>
    <td style="padding:15px">{base_path}/{version}/topology/vlan/vlan-names?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1topologysiteTopology(callback)</td>
    <td style="padding:15px">Get Site Topology</td>
    <td style="padding:15px">{base_path}/{version}/topology/site-topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1topologyphysicalTopology(nodeType, callback)</td>
    <td style="padding:15px">Get Physical Topology</td>
    <td style="padding:15px">{base_path}/{version}/topology/physical-topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1topologyl2vlanID(vlanID, callback)</td>
    <td style="padding:15px">Get topology details</td>
    <td style="padding:15px">{base_path}/{version}/topology/l2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1topologyl3topologyType(topologyType, callback)</td>
    <td style="padding:15px">Get L3 Topology Details</td>
    <td style="padding:15px">{base_path}/{version}/topology/l3/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1networkHealth(timestamp, callback)</td>
    <td style="padding:15px">Get Overall Network Health</td>
    <td style="padding:15px">{base_path}/{version}/network-health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1flowAnalysis(periodicRefresh, sourceIP, destIP, sourcePort, destPort, gtCreateTime, ltCreateTime, protocol, status, taskId, lastUpdateTime, limit, offset, order, sortBy, callback)</td>
    <td style="padding:15px">Retrives all previous Pathtraces summary</td>
    <td style="padding:15px">{base_path}/{version}/flow-analysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1flowAnalysis(request, ContentType, callback)</td>
    <td style="padding:15px">Initiate a new Pathtrace</td>
    <td style="padding:15px">{base_path}/{version}/flow-analysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1flowAnalysisflowAnalysisId(flowAnalysisId, callback)</td>
    <td style="padding:15px">Retrieves previous Pathtrace</td>
    <td style="padding:15px">{base_path}/{version}/flow-analysis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1flowAnalysisflowAnalysisId(flowAnalysisId, callback)</td>
    <td style="padding:15px">Deletes Pathtrace by Id</td>
    <td style="padding:15px">{base_path}/{version}/flow-analysis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1businessnfvprovisioningDetail(deviceIp, callback)</td>
    <td style="padding:15px">**BETA** - Get Device details by IP</td>
    <td style="padding:15px">{base_path}/{version}/business/nfv/provisioningDetail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1businessnfv(request, Runsync, Persistbapioutput, Timeout, callback)</td>
    <td style="padding:15px">Provision NFV</td>
    <td style="padding:15px">{base_path}/{version}/business/nfv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnasystemapiv1authtoken(ContentType, Authorization, callback)</td>
    <td style="padding:15px">Authentication API</td>
    <td style="padding:15px">/dna/system/api/{version}/auth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1businessssidssidNamemanagedAPLocations(ssidName, managedAPLocations, callback)</td>
    <td style="padding:15px">**BETA** - Delete and provision SSID</td>
    <td style="padding:15px">{base_path}/{version}/business/ssid/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1businessssid(request, callback)</td>
    <td style="padding:15px">**BETA** - Create and Provision SSID</td>
    <td style="padding:15px">{base_path}/{version}/business/ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1enterpriseSsid(ssidName, callback)</td>
    <td style="padding:15px">**BETA** - Get Enterprise SSID</td>
    <td style="padding:15px">{base_path}/{version}/enterprise-ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1enterpriseSsid(request, callback)</td>
    <td style="padding:15px">**BETA** - Create Enterprise SSID</td>
    <td style="padding:15px">{base_path}/{version}/enterprise-ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1enterpriseSsidssidName(ssidName, callback)</td>
    <td style="padding:15px">**BETA** - Delete Enterprise SSID</td>
    <td style="padding:15px">{base_path}/{version}/enterprise-ssid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1clientDetail(timestamp, macAddress, callback)</td>
    <td style="padding:15px">Get Client Detail</td>
    <td style="padding:15px">{base_path}/{version}/client-detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1clientHealth(timestamp, callback)</td>
    <td style="padding:15px">Get Overall Client Health</td>
    <td style="padding:15px">{base_path}/{version}/client-health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnaintentapiv1businesssdaborderDevicedeviceIpAddress(sdaborderdevice, deviceipaddress, callback)</td>
    <td style="padding:15px">**BETA** - Gets border device details from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnaintentapiv1businesssdaborderDevicedeviceIpAddress(request, Runsync, Runsynctimeout, sdaborderdevice, deviceipaddress, callback)</td>
    <td style="padding:15px">Deletes border device from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnaintentapiv1businesssdaborderDevice(request, Runsync, Runsynctimeout, sdaborderdevice, callback)</td>
    <td style="padding:15px">Adds border device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortAssignmentForAccessPointInSDAFabric(deviceManagementIpAddress, interfaceName, callback)</td>
    <td style="padding:15px">Delete Port assignment for access point in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/access-point?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortAssignmentForAccessPointInSDAFabric(deviceManagementIpAddress, interfaceName, callback)</td>
    <td style="padding:15px">Get Port assignment for access point in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/access-point?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPortAssignmentForAccessPointInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add Port assignment for access point in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/access-point?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInfoFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get device info from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitPeerNetworkInfo(transitPeerNetworkName, callback)</td>
    <td style="padding:15px">Get Transit Peer Network Info</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/transit-peer-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTransitPeerNetwork(request, callback)</td>
    <td style="padding:15px">Add Transit Peer Network</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/transit-peer-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitPeerNetwork(transitPeerNetworkName, callback)</td>
    <td style="padding:15px">Delete Transit Peer Network</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/transit-peer-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEdgeDeviceFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Delete edge device from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/edge-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEdgeDeviceFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get edge device from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/edge-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEdgeDeviceInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add edge device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/edge-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIPPoolInSDAVirtualNetwork(request, callback)</td>
    <td style="padding:15px">Add IP Pool in SDA Virtual Network</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtualnetwork/ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPPoolFromSDAVirtualNetwork(siteNameHierarchy, virtualNetworkName, ipPoolName, callback)</td>
    <td style="padding:15px">Delete IP Pool from SDA Virtual Network</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtualnetwork/ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPPoolFromSDAVirtualNetwork(siteNameHierarchy, virtualNetworkName, ipPoolName, callback)</td>
    <td style="padding:15px">Get IP Pool from SDA Virtual Network</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtualnetwork/ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMulticastFromSDAFabric(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Delete multicast from SDA fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMulticastDetailsFromSDAFabric(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Get multicast details from SDA fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMulticastInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add multicast in SDA fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualNetworkFromSDAFabric(virtualNetworkName, siteNameHierarchy, callback)</td>
    <td style="padding:15px">Get VN from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVNInFabric(request, callback)</td>
    <td style="padding:15px">Add VN in fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVNFromSDAFabric(virtualNetworkName, siteNameHierarchy, callback)</td>
    <td style="padding:15px">Delete VN from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultAuthenticationProfileFromSDAFabric(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Delete default authentication profile from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/authentication-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDefaultAuthenticationProfileInSDAFabric(request, callback)</td>
    <td style="padding:15px">Update default authentication profile in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/authentication-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultAuthenticationProfileFromSDAFabric(siteNameHierarchy, authenticateTemplateName, callback)</td>
    <td style="padding:15px">Get default authentication profile from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/authentication-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDefaultAuthenticationTemplateInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add default authentication template in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/authentication-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteFromSDAFabric(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Delete Site from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/fabric-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteFromSDAFabric(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Get Site from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/fabric-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSiteInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add Site in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/fabric-site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reProvisionWiredDevice(request, callback)</td>
    <td style="padding:15px">Re-Provision Wired Device</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/provision-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionWiredDevice(request, callback)</td>
    <td style="padding:15px">Provision Wired Device</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/provision-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProvisionedWiredDevice(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get Provisioned Wired Device</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/provision-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProvisionedWiredDevice(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Delete provisioned Wired Device</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/provision-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualNetworkSummary(siteNameHierarchy, callback)</td>
    <td style="padding:15px">Get Virtual Network Summary</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/virtual-network/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceRoleInSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get device role in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/device/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPortAssignmentForUserDeviceInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add Port assignment for user device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/user-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortAssignmentForUserDeviceInSDAFabric(deviceManagementIpAddress, interfaceName, callback)</td>
    <td style="padding:15px">Get Port assignment for user device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/user-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortAssignmentForUserDeviceInSDAFabric(deviceManagementIpAddress, interfaceName, callback)</td>
    <td style="padding:15px">Delete Port assignment for user device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/user-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBorderDeviceDetailFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get border device detail from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBorderDeviceInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add border device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBorderDeviceFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Delete border device from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/border-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControlPlaneDeviceFromSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Get control plane device from SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/control-plane-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addControlPlaneDeviceInSDAFabric(request, callback)</td>
    <td style="padding:15px">Add control plane device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/control-plane-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteControlPlaneDeviceInSDAFabric(deviceManagementIpAddress, callback)</td>
    <td style="padding:15px">Delete control plane device in SDA Fabric</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/control-plane-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVirtualNetworkWithScalableGroups(request, callback)</td>
    <td style="padding:15px">Update virtual network with scalable groups</td>
    <td style="padding:15px">{base_path}/{version}/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualNetworkWithScalableGroups(virtualNetworkName, callback)</td>
    <td style="padding:15px">Delete virtual network with scalable groups</td>
    <td style="padding:15px">{base_path}/{version}/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualNetworkWithScalableGroups(virtualNetworkName, callback)</td>
    <td style="padding:15px">Get virtual network with scalable groups</td>
    <td style="padding:15px">{base_path}/{version}/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVirtualNetworkWithScalableGroups(request, callback)</td>
    <td style="padding:15px">Add virtual network with scalable groups</td>
    <td style="padding:15px">{base_path}/{version}/virtual-network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSetsV2(attributes, name, offset, limit, callback)</td>
    <td style="padding:15px">Get Application Set/s</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationSetsV2(body, callback)</td>
    <td style="padding:15px">Create Application Set/s</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationPolicyQueuingProfile(id, body, callback)</td>
    <td style="padding:15px">Delete Application Policy Queuing Profile</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-queuing-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationPolicyQueuingProfile(name, callback)</td>
    <td style="padding:15px">Get Application Policy Queuing Profile</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-queuing-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationPolicyQueuingProfile(body, callback)</td>
    <td style="padding:15px">Update Application Policy Queuing Profile</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-queuing-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationPolicyQueuingProfile(body, callback)</td>
    <td style="padding:15px">Create Application Policy Queuing Profile</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-queuing-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationPolicyDefault(callback)</td>
    <td style="padding:15px">Get Application Policy Default</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQosDeviceInterfaceInfo(networkDeviceId, callback)</td>
    <td style="padding:15px">Get Qos Device Interface Info</td>
    <td style="padding:15px">{base_path}/{version}/qos-device-interface-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQosDeviceInterfaceInfo(body, callback)</td>
    <td style="padding:15px">Update Qos Device Interface Info</td>
    <td style="padding:15px">{base_path}/{version}/qos-device-interface-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQosDeviceInterfaceInfo(body, callback)</td>
    <td style="padding:15px">Create Qos Device Interface Info</td>
    <td style="padding:15px">{base_path}/{version}/qos-device-interface-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationPolicy(policyScope, callback)</td>
    <td style="padding:15px">Get Application Policy</td>
    <td style="padding:15px">{base_path}/{version}/app-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQosDeviceInterfaceInfo(id, callback)</td>
    <td style="padding:15px">Delete Qos Device Interface Info</td>
    <td style="padding:15px">{base_path}/{version}/qos-device-interface-info/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationsV2(attributes, name, offset, limit, callback)</td>
    <td style="padding:15px">Get Application/s</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editApplicationsV2(body, callback)</td>
    <td style="padding:15px">Edit Application/s</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationV2(body, callback)</td>
    <td style="padding:15px">Create Application/s</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQosDeviceInterfaceInfoCount(callback)</td>
    <td style="padding:15px">Get Qos Device Interface Info Count</td>
    <td style="padding:15px">{base_path}/{version}/qos-device-interface-info-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSetCountV2(scalableGroupType, callback)</td>
    <td style="padding:15px">Get Application Set Count</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationV2(id, callback)</td>
    <td style="padding:15px">Delete Application</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplicationPolicyIntent(body, callback)</td>
    <td style="padding:15px">Application Policy Intent</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-intent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationSetV2(id, callback)</td>
    <td style="padding:15px">Delete Application Set</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationCountV2(scalableGroupType, callback)</td>
    <td style="padding:15px">Get Application Count</td>
    <td style="padding:15px">{base_path}/{version}/applications-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationPolicyQueuingProfileCount(callback)</td>
    <td style="padding:15px">Get Application Policy Queuing Profile Count</td>
    <td style="padding:15px">{base_path}/{version}/app-policy-queuing-profile-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationsCount(callback)</td>
    <td style="padding:15px">Get Applications Count</td>
    <td style="padding:15px">{base_path}/{version}/applications-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplications(offset, limit, name, callback)</td>
    <td style="padding:15px">Get Applications</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editApplication(body, callback)</td>
    <td style="padding:15px">Edit Application</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplication(body, callback)</td>
    <td style="padding:15px">Create Application</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplication(id, callback)</td>
    <td style="padding:15px">Delete Application</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSets(offset, limit, name, callback)</td>
    <td style="padding:15px">Get Application Sets</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationSet(body, callback)</td>
    <td style="padding:15px">Create Application Set</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationSet(id, callback)</td>
    <td style="padding:15px">Delete Application Set</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSetsCount(callback)</td>
    <td style="padding:15px">Get Application Sets Count</td>
    <td style="padding:15px">{base_path}/{version}/application-policy-application-set-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureAccessPoints(body, callback)</td>
    <td style="padding:15px">Configure Access Points</td>
    <td style="padding:15px">{base_path}/{version}/wireless/accesspoint-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootAccessPoints(body, callback)</td>
    <td style="padding:15px">Reboot Access Points</td>
    <td style="padding:15px">{base_path}/{version}/device-reboot/apreboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPointConfiguration(key, callback)</td>
    <td style="padding:15px">Get Access Point Configuration</td>
    <td style="padding:15px">{base_path}/{version}/wireless/accesspoint-configuration/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPointRebootTaskResult(parentTaskId, callback)</td>
    <td style="padding:15px">Get Access Point Reboot task result</td>
    <td style="padding:15px">{base_path}/{version}/device-reboot/apreboot/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPointConfigurationTaskResult(taskId, callback)</td>
    <td style="padding:15px">Get Access Point Configuration task result</td>
    <td style="padding:15px">{base_path}/{version}/wireless/accesspoint-configuration/details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveRFProfiles(rfProfileName, callback)</td>
    <td style="padding:15px">Retrieve RF profiles</td>
    <td style="padding:15px">{base_path}/{version}/wireless/rf-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateRFProfile(body, callback)</td>
    <td style="padding:15px">Create or Update RF profile</td>
    <td style="padding:15px">{base_path}/{version}/wireless/rf-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRFProfiles(rfProfileName, callback)</td>
    <td style="padding:15px">Delete RF profiles</td>
    <td style="padding:15px">{base_path}/{version}/wireless/rf-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pSKOverride(body, callback)</td>
    <td style="padding:15px">PSK override</td>
    <td style="padding:15px">{base_path}/{version}/wireless/psk-override?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWirelessProfile(profileName, callback)</td>
    <td style="padding:15px">Get Wireless Profile</td>
    <td style="padding:15px">{base_path}/{version}/wireless/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWirelessProfile(body, callback)</td>
    <td style="padding:15px">Update Wireless Profile</td>
    <td style="padding:15px">{base_path}/{version}/wireless/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWirelessProfile(body, callback)</td>
    <td style="padding:15px">Create Wireless Profile</td>
    <td style="padding:15px">{base_path}/{version}/wireless/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionUpdate(body, callback)</td>
    <td style="padding:15px">Provision update</td>
    <td style="padding:15px">{base_path}/{version}/wireless/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provision(body, callback)</td>
    <td style="padding:15px">Provision</td>
    <td style="padding:15px">{base_path}/{version}/wireless/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSensorTestResults(siteId, startTime, endTime, testFailureBy, callback)</td>
    <td style="padding:15px">Sensor Test Results</td>
    <td style="padding:15px">{base_path}/{version}/AssuranceGetgetSensorTestResults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEnterpriseSSID(body, callback)</td>
    <td style="padding:15px">Update Enterprise SSID</td>
    <td style="padding:15px">{base_path}/{version}/enterprise-ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicInterface(interfaceName, callback)</td>
    <td style="padding:15px">Get dynamic interface</td>
    <td style="padding:15px">{base_path}/{version}/wireless/dynamic-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUpdateDynamicInterface(body, callback)</td>
    <td style="padding:15px">Create Update Dynamic interface</td>
    <td style="padding:15px">{base_path}/{version}/wireless/dynamic-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPProvision(body, callback)</td>
    <td style="padding:15px">AP Provision</td>
    <td style="padding:15px">{base_path}/{version}/wireless/ap-provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWirelessProfile(wirelessProfileName, body, callback)</td>
    <td style="padding:15px">Delete Wireless Profile</td>
    <td style="padding:15px">{base_path}/{version}/wireless-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicInterface(interfaceName, callback)</td>
    <td style="padding:15px">Delete dynamic interface</td>
    <td style="padding:15px">{base_path}/{version}/wireless/dynamic-interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createACloneOfTheGivenTemplate(name, templateId, projectId, callback)</td>
    <td style="padding:15px">Creates a clone of the given template</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/clone/name/{pathv1}/project/{pathv2}/template/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTheTemplatesProvided(projectName, doVersion, body, callback)</td>
    <td style="padding:15px">Imports the templates provided</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/name/{pathv1}/template/importtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTheProjectsForAGivenCriteria(body, callback)</td>
    <td style="padding:15px">Exports the projects for a given criteria.</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/name/exportprojects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTheDetailsOfAGivenProject(projectId, callback)</td>
    <td style="padding:15px">Gets the details of a given project.</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectDetailsV2(id, name, offset, limit, sortOrder, callback)</td>
    <td style="padding:15px">Get project(s) details</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateDetailsV2(id, name, projectId, projectName, softwareType, softwareVersion, productFamily, productSeries, productType, filterConflictingTemplates, tags, unCommitted, sortOrder, allTemplateAttributes, includeVersionDetails, offset, limit, callback)</td>
    <td style="padding:15px">Get template(s) details</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTheTemplatesForAGivenCriteria(body, callback)</td>
    <td style="padding:15px">Exports the templates for a given criteria.</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/template/exporttemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTheProjectsProvided(doVersion, body, callback)</td>
    <td style="padding:15px">Imports the Projects provided</td>
    <td style="padding:15px">{base_path}/{version}/template-programmer/project/importprojects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewsForAGivenViewGroup(viewGroupId, callback)</td>
    <td style="padding:15px">Get views for a given view group</td>
    <td style="padding:15px">{base_path}/{version}/data/view-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewDetailsForAGivenViewGroupView(viewGroupId, viewId, callback)</td>
    <td style="padding:15px">Get view details for a given view group & view</td>
    <td style="padding:15px">{base_path}/{version}/data/view-groups/{pathv1}/views/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAScheduledReport(reportId, callback)</td>
    <td style="padding:15px">Get a scheduled report</td>
    <td style="padding:15px">{base_path}/{version}/data/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAScheduledReport(reportId, callback)</td>
    <td style="padding:15px">Delete a scheduled report</td>
    <td style="padding:15px">{base_path}/{version}/data/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfScheduledReports(viewGroupId, viewId, callback)</td>
    <td style="padding:15px">Get list of scheduled reports</td>
    <td style="padding:15px">{base_path}/{version}/data/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrScheduleAReport(body, callback)</td>
    <td style="padding:15px">Create or Schedule a report</td>
    <td style="padding:15px">{base_path}/{version}/data/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllViewGroups(callback)</td>
    <td style="padding:15px">Get all view groups</td>
    <td style="padding:15px">{base_path}/{version}/data/view-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExecutionDetailsForAGivenReport(reportId, callback)</td>
    <td style="padding:15px">Get all execution details for a given report</td>
    <td style="padding:15px">{base_path}/{version}/data/reports/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadReportContent(reportId, executionId, callback)</td>
    <td style="padding:15px">Download report content</td>
    <td style="padding:15px">{base_path}/{version}/data/reports/{pathv1}/executions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChassisDetailsForDevice(deviceId, callback)</td>
    <td style="padding:15px">Get Chassis Details for Device</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/chassis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUserDefinedFields(id, name, callback)</td>
    <td style="padding:15px">Get All User-Defined-Fields</td>
    <td style="padding:15px">{base_path}/{version}/network-device/user-defined-field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserDefinedField(body, callback)</td>
    <td style="padding:15px">Create User-Defined-Field</td>
    <td style="padding:15px">{base_path}/{version}/network-device/user-defined-field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTheDetailsOfPhysicalComponentsOfTheGivenDevice(deviceUuid, type, callback)</td>
    <td style="padding:15px">Get the Details of Physical Components of the Given Device.</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnPOEInterfaceDetailsForTheDevice(deviceUuid, interfaceNameList, callback)</td>
    <td style="padding:15px">Returns POE interface details for the device.</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/interface/poe-detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearMacAddressTable(interfaceUuid, deploymentMode, body, callback)</td>
    <td style="padding:15px">Clear Mac-Address table</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}/operation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlannedAccessPointsForFloor(floorId, limit, offset, radios, callback)</td>
    <td style="padding:15px">Get Planned Access Points for Floor</td>
    <td style="padding:15px">{base_path}/{version}/floors/{pathv1}/planned-access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePlannedAccessPointForFloor(floorId, body, callback)</td>
    <td style="padding:15px">Update Planned Access Point for Floor</td>
    <td style="padding:15px">{base_path}/{version}/floors/{pathv1}/planned-access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPlannedAccessPointForFloor(floorId, body, callback)</td>
    <td style="padding:15px">Create Planned Access Point for Floor</td>
    <td style="padding:15px">{base_path}/{version}/floors/{pathv1}/planned-access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceHealth(deviceRole, siteId, health, startTime, endTime, limit, offset, callback)</td>
    <td style="padding:15px">getDeviceHealth</td>
    <td style="padding:15px">{base_path}/{version}/device-health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinecardDetails(deviceUuid, callback)</td>
    <td style="padding:15px">Get Linecard details</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/line-card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryInsightDeviceLinkMismatchAPI(siteId, offset, limit, category, sortBy, order, callback)</td>
    <td style="padding:15px">Inventory Insight Device Link Mismatch API</td>
    <td style="padding:15px">{base_path}/{version}/network-device/insight/{pathv1}/device-link?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePlannedAccessPointForFloor(floorId, plannedAccessPointUuid, callback)</td>
    <td style="padding:15px">Delete Planned Access Point for Floor</td>
    <td style="padding:15px">{base_path}/{version}/floors/{pathv1}/planned-access-points/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStackDetailsForDevice(deviceId, callback)</td>
    <td style="padding:15px">Get Stack Details for Device</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/stack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInterfaceStatsInfoV2(deviceId, body, callback)</td>
    <td style="padding:15px">Get Device Interface Stats Info</td>
    <td style="padding:15px">{base_path}/{version}/networkDevices/{pathv1}/interfaces/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserDefinedField(id, body, callback)</td>
    <td style="padding:15px">Update User-Defined-Field</td>
    <td style="padding:15px">{base_path}/{version}/network-device/user-defined-field/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserDefinedField(id, callback)</td>
    <td style="padding:15px">Delete User-Defined-Field</td>
    <td style="padding:15px">{base_path}/{version}/network-device/user-defined-field/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInterfaceDetails(interfaceUuid, deploymentMode, body, callback)</td>
    <td style="padding:15px">Update Interface details</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLegitOperationsForInterface(interfaceUuid, callback)</td>
    <td style="padding:15px">Legit operations for interface</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}/legit-operation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupervisorCardDetail(deviceUuid, callback)</td>
    <td style="padding:15px">Get Supervisor card detail</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/supervisor-card?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserDefinedFieldToDevice(deviceId, body, callback)</td>
    <td style="padding:15px">Add User-Defined-Field to device</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/user-defined-field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserDefinedFieldFromDevice(deviceId, name, callback)</td>
    <td style="padding:15px">Remove User-Defined-Field from device</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/user-defined-field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectedDeviceDetail(deviceUuid, interfaceUuid, callback)</td>
    <td style="padding:15px">Get connected device detail</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/interface/{pathv2}/neighbor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsDevicesAddedToCiscoDNACenterWithSnmpV3DES(siteId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Returns devices added to Cisco DNA center with snmp v3 DES.</td>
    <td style="padding:15px">{base_path}/{version}/network-device/insight/{pathv1}/insecure-connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceManagementAddress(deviceid, body, callback)</td>
    <td style="padding:15px">Update Device Management Address</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}/management-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlannedAccessPointsForBuilding(buildingId, limit, offset, radios, callback)</td>
    <td style="padding:15px">Get Planned Access Points for Building</td>
    <td style="padding:15px">{base_path}/{version}/buildings/{pathv1}/planned-access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceEnrichmentDetails(callback)</td>
    <td style="padding:15px">Get Device Enrichment Details</td>
    <td style="padding:15px">{base_path}/{version}/device-enrichment-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGlobalCredentialsV2(callback)</td>
    <td style="padding:15px">Get All Global Credentials V2</td>
    <td style="padding:15px">{base_path}/{version}/global-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalCredentialsV2(body, callback)</td>
    <td style="padding:15px">Update Global Credentials V2</td>
    <td style="padding:15px">{base_path}/{version}/global-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalCredentialsV2(body, callback)</td>
    <td style="padding:15px">Create Global Credentials V2</td>
    <td style="padding:15px">{base_path}/{version}/global-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalCredentialV2(id, callback)</td>
    <td style="padding:15px">Delete Global Credential V2</td>
    <td style="padding:15px">{base_path}/{version}/global-credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebhookDestination(webhookIds, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Webhook Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWebhookDestination(body, callback)</td>
    <td style="padding:15px">Update Webhook Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWebhookDestination(body, callback)</td>
    <td style="padding:15px">Create Webhook Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogDestination(configId, name, protocol, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Syslog Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/syslog-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSyslogDestination(body, callback)</td>
    <td style="padding:15px">Update Syslog Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/syslog-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSyslogDestination(body, callback)</td>
    <td style="padding:15px">Create Syslog Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/syslog-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSNMPDestination(body, callback)</td>
    <td style="padding:15px">Update SNMP Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/snmp-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSNMPDestination(body, callback)</td>
    <td style="padding:15px">Create SNMP Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/snmp-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountOfNotifications(eventIds, startTime, endTime, category, type, severity, domain, subDomain, source, callback)</td>
    <td style="padding:15px">Count of Notifications</td>
    <td style="padding:15px">{base_path}/{version}/event/event-series/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountOfEventSubscriptions(eventIds, callback)</td>
    <td style="padding:15px">Count of Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogSubscriptionDetails(name, instanceId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Syslog Subscription Details</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription-details/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailSubscriptionDetails(name, instanceId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Email Subscription Details</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription-details/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailEventSubscriptions(eventIds, offset, limit, sortBy, order, domain, subDomain, category, type, name, callback)</td>
    <td style="padding:15px">Get Email Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEmailEventSubscription(body, callback)</td>
    <td style="padding:15px">Update Email Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEmailEventSubscription(body, callback)</td>
    <td style="padding:15px">Create Email Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEvents(eventId, tags, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Events</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogSummary(parentInstanceId, isParentOnly, instanceId, name, eventId, category, severity, domain, subDomain, source, userId, context, eventHierarchy, siteId, deviceId, isSystemEvents, description, startTime, endTime, callback)</td>
    <td style="padding:15px">Get AuditLog Summary</td>
    <td style="padding:15px">/dna/data/api/{version}/event/event-series/audit-log/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventSubscriptions(eventIds, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEventSubscriptions(body, callback)</td>
    <td style="padding:15px">Update Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEventSubscriptions(body, callback)</td>
    <td style="padding:15px">Create Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventSubscriptions(subscriptions, callback)</td>
    <td style="padding:15px">Delete Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogEventSubscriptions(eventIds, offset, limit, sortBy, order, domain, subDomain, category, type, name, callback)</td>
    <td style="padding:15px">Get Syslog Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSyslogEventSubscription(body, callback)</td>
    <td style="padding:15px">Update Syslog Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSyslogEventSubscription(body, callback)</td>
    <td style="padding:15px">Create Syslog Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountOfEvents(eventId, tags, callback)</td>
    <td style="padding:15px">Count of Events</td>
    <td style="padding:15px">{base_path}/{version}/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventArtifacts(eventIds, tags, offset, limit, sortBy, order, search, callback)</td>
    <td style="padding:15px">Get EventArtifacts</td>
    <td style="padding:15px">/dna/system/api/{version}/event/artifact?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailDestination(callback)</td>
    <td style="padding:15px">Get Email Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/email-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEmailDestination(body, callback)</td>
    <td style="padding:15px">Update Email Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/email-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEmailDestination(body, callback)</td>
    <td style="padding:15px">Create Email Destination</td>
    <td style="padding:15px">{base_path}/{version}/event/email-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotifications(eventIds, startTime, endTime, category, type, severity, domain, subDomain, source, offset, limit, sortBy, order, tags, namespace, siteId, callback)</td>
    <td style="padding:15px">Get Notifications</td>
    <td style="padding:15px">{base_path}/{version}/event/event-series?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogRecords(parentInstanceId, instanceId, name, eventId, category, severity, domain, subDomain, source, userId, context, eventHierarchy, siteId, deviceId, isSystemEvents, description, offset, limit, startTime, endTime, sortBy, order, callback)</td>
    <td style="padding:15px">Get AuditLog Records</td>
    <td style="padding:15px">/dna/data/api/{version}/event/event-series/audit-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestWebhookEventSubscriptions(eventIds, offset, limit, sortBy, order, domain, subDomain, category, type, name, callback)</td>
    <td style="padding:15px">Get Rest/Webhook Event Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/rest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRestWebhookEventSubscription(body, callback)</td>
    <td style="padding:15px">Update Rest/Webhook Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/rest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRestWebhookEventSubscription(body, callback)</td>
    <td style="padding:15px">Create Rest/Webhook Event Subscription</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription/rest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogParentRecords(instanceId, name, eventId, category, severity, domain, subDomain, source, userId, context, eventHierarchy, siteId, deviceId, isSystemEvents, description, offset, limit, startTime, endTime, sortBy, order, callback)</td>
    <td style="padding:15px">Get AuditLog Parent Records</td>
    <td style="padding:15px">/dna/data/api/{version}/event/event-series/audit-log/parent-records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventArtifactCount(callback)</td>
    <td style="padding:15px">EventArtifact Count</td>
    <td style="padding:15px">/dna/system/api/{version}/event/artifact/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectorTypes(callback)</td>
    <td style="padding:15px">Get Connector Types</td>
    <td style="padding:15px">/dna/system/api/{version}/event/config/connector-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusAPIForEvents(executionId, callback)</td>
    <td style="padding:15px">Get Status API for Events</td>
    <td style="padding:15px">{base_path}/{version}/event/api-status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestWebhookSubscriptionDetails(name, instanceId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get Rest/Webhook Subscription Details</td>
    <td style="padding:15px">{base_path}/{version}/event/subscription-details/rest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNMPDestination(configId, offset, limit, sortBy, order, callback)</td>
    <td style="padding:15px">Get SNMP Destination</td>
    <td style="padding:15px">{base_path}/{version}/dna-event/snmp-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lANAutomationStopAndUpdateDevices(id, body, callback)</td>
    <td style="padding:15px">LAN Automation Stop and Update Devices</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lANAutomationStop(id, body, callback)</td>
    <td style="padding:15px">LAN Automation Stop</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lANAutomationDeviceUpdate(feature, body, callback)</td>
    <td style="padding:15px">LAN Automation Device Update</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/updateDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationLogById(id, callback)</td>
    <td style="padding:15px">LAN Automation Log by Id</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationStatusById(id, callback)</td>
    <td style="padding:15px">LAN Automation Status by Id</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationLog(offset, limit, callback)</td>
    <td style="padding:15px">LAN Automation Log</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lANAutomationStart(body, callback)</td>
    <td style="padding:15px">LAN Automation Start</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationStatus(offset, limit, callback)</td>
    <td style="padding:15px">LAN Automation Status</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationSessionCount(callback)</td>
    <td style="padding:15px">LAN Automation Session Count</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationLogsForIndividualDevices(id, serialNumber, logLevel, callback)</td>
    <td style="padding:15px">LAN Automation Logs for Individual Devices</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/log/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANAutomationActiveSessions(callback)</td>
    <td style="padding:15px">LAN Automation Active Sessions</td>
    <td style="padding:15px">{base_path}/{version}/lan-automation/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAuthenticationAndPolicyServerAccessConfiguration(id, body, callback)</td>
    <td style="padding:15px">Edit Authentication and Policy Server Access Configuration</td>
    <td style="padding:15px">{base_path}/{version}/authentication-policy-servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticationAndPolicyServerAccessConfiguration(id, callback)</td>
    <td style="padding:15px">Delete Authentication and Policy Server Access Configuration</td>
    <td style="padding:15px">{base_path}/{version}/authentication-policy-servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomPromptSupport(callback)</td>
    <td style="padding:15px">Custom-prompt support GET API</td>
    <td style="padding:15px">{base_path}/{version}/network-device/custom-prompt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomPrompt(body, callback)</td>
    <td style="padding:15px">Custom Prompt POST API</td>
    <td style="padding:15px">{base_path}/{version}/network-device/custom-prompt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationAndPolicyServers(isIseEnabled, state, role, callback)</td>
    <td style="padding:15px">Get Authentication and Policy Servers</td>
    <td style="padding:15px">{base_path}/{version}/authentication-policy-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAuthenticationAndPolicyServerAccessConfiguration(body, callback)</td>
    <td style="padding:15px">Add Authentication and Policy Server Access Configuration</td>
    <td style="padding:15px">{base_path}/{version}/authentication-policy-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoISEServerIntegrationStatus(callback)</td>
    <td style="padding:15px">Cisco ISE Server Integration Status</td>
    <td style="padding:15px">{base_path}/{version}/ise-integration-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptCiscoISEServerCertificateForCiscoISEServerIntegration(id, body, callback)</td>
    <td style="padding:15px">Accept Cisco ISE Server Certificate for Cisco ISE Server Integration</td>
    <td style="padding:15px">{base_path}/{version}/integrate-ise/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createITSMIntegrationSetting(body, callback)</td>
    <td style="padding:15px">Create ITSM Integration setting</td>
    <td style="padding:15px">{base_path}/{version}/integration-settings/instances/itsm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getITSMIntegrationSettingById(instanceId, body, callback)</td>
    <td style="padding:15px">Get ITSM Integration setting by Id</td>
    <td style="padding:15px">{base_path}/{version}/integration-settings/instances/itsm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateITSMIntegrationSetting(instanceId, body, callback)</td>
    <td style="padding:15px">Update ITSM Integration setting</td>
    <td style="padding:15px">{base_path}/{version}/integration-settings/instances/itsm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteITSMIntegrationSetting(instanceId, callback)</td>
    <td style="padding:15px">Delete ITSM Integration setting</td>
    <td style="padding:15px">{base_path}/{version}/integration-settings/instances/itsm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllITSMIntegrationSettings(body, callback)</td>
    <td style="padding:15px">Get all ITSM Integration settings</td>
    <td style="padding:15px">{base_path}/{version}/integration-settings/itsm/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceFamilyIdentifiers(callback)</td>
    <td style="padding:15px">Get Device Family Identifiers</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/device-family-identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagAsGoldenImage(body, callback)</td>
    <td style="padding:15px">Tag as Golden Image</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/golden?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGoldenTagStatusOfAnImage(siteId, deviceFamilyIdentifier, deviceRole, imageId, callback)</td>
    <td style="padding:15px">Get Golden Tag Status of an Image.</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/golden/site/{pathv1}/family/{pathv2}/role/{pathv3}/image/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGoldenTagForImage(siteId, deviceFamilyIdentifier, deviceRole, imageId, callback)</td>
    <td style="padding:15px">Remove Golden Tag for image.</td>
    <td style="padding:15px">{base_path}/{version}/image/importation/golden/site/{pathv1}/family/{pathv2}/role/{pathv3}/image/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDeviceCredentialToSiteV2(siteId, body, callback)</td>
    <td style="padding:15px">Assign Device Credential To Site V2</td>
    <td style="padding:15px">{base_path}/{version}/credential-to-site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkV2(siteId, body, callback)</td>
    <td style="padding:15px">Update Network V2</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkV2(siteId, body, callback)</td>
    <td style="padding:15px">Create Network V2</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalPool(offset, limit, callback)</td>
    <td style="padding:15px">Get Global Pool</td>
    <td style="padding:15px">{base_path}/{version}/global-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalPool(body, callback)</td>
    <td style="padding:15px">Update Global Pool</td>
    <td style="padding:15px">{base_path}/{version}/global-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalPool(body, callback)</td>
    <td style="padding:15px">Create Global Pool</td>
    <td style="padding:15px">{base_path}/{version}/global-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalIPPool(id, callback)</td>
    <td style="padding:15px">Delete Global IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/global-pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceCredential(id, callback)</td>
    <td style="padding:15px">Delete Device Credential</td>
    <td style="padding:15px">{base_path}/{version}/device-credential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetwork(siteId, callback)</td>
    <td style="padding:15px">Get Network</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReserveIPSubpool(siteId, id, body, callback)</td>
    <td style="padding:15px">Update Reserve IP Subpool</td>
    <td style="padding:15px">{base_path}/{version}/reserve-ip-subpool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reserveIPSubpool(siteId, body, callback)</td>
    <td style="padding:15px">Reserve IP Subpool</td>
    <td style="padding:15px">{base_path}/{version}/reserve-ip-subpool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReserveIPSubpool(siteId, offset, limit, ignoreInheritedGroups, poolUsage, callback)</td>
    <td style="padding:15px">Get Reserve IP Subpool</td>
    <td style="padding:15px">{base_path}/{version}/reserve-ip-subpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSPProfile(spProfileName, callback)</td>
    <td style="padding:15px">Delete SP Profile</td>
    <td style="padding:15px">{base_path}/{version}/sp-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDeviceCredentialToSite(siteId, body, callback)</td>
    <td style="padding:15px">Assign Device Credential To Site</td>
    <td style="padding:15px">{base_path}/{version}/credential-to-site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCredentialDetails(siteId, callback)</td>
    <td style="padding:15px">Get Device Credential Details</td>
    <td style="padding:15px">{base_path}/{version}/device-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceCredentials(body, callback)</td>
    <td style="padding:15px">Update Device Credentials</td>
    <td style="padding:15px">{base_path}/{version}/device-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceCredentials(body, callback)</td>
    <td style="padding:15px">Create Device Credentials</td>
    <td style="padding:15px">{base_path}/{version}/device-credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceProviderDetails(callback)</td>
    <td style="padding:15px">Get Service provider Details</td>
    <td style="padding:15px">{base_path}/{version}/service-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSPProfile(body, callback)</td>
    <td style="padding:15px">Update SP Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSPProfile(body, callback)</td>
    <td style="padding:15px">Create SP Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetwork(siteId, body, callback)</td>
    <td style="padding:15px">Update Network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(siteId, body, callback)</td>
    <td style="padding:15px">Create Network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseReserveIPSubpool(id, callback)</td>
    <td style="padding:15px">Release Reserve IP Subpool</td>
    <td style="padding:15px">{base_path}/{version}/reserve-ip-subpool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(nameSpace, callback)</td>
    <td style="padding:15px">uploadFile</td>
    <td style="padding:15px">{base_path}/{version}/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientProximity(username, numberDays, timeResolution, callback)</td>
    <td style="padding:15px">Client Proximity</td>
    <td style="padding:15px">{base_path}/{version}/client-proximity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientEnrichmentDetails(callback)</td>
    <td style="padding:15px">Get Client Enrichment Details</td>
    <td style="padding:15px">{base_path}/{version}/client-enrichment-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCountV2(id, callback)</td>
    <td style="padding:15px">Get Site Count V2</td>
    <td style="padding:15px">{base_path}/{version}/site/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMapArchiveCancelAnImport(importContextUuid, callback)</td>
    <td style="padding:15px">Import Map Archive - Cancel an Import</td>
    <td style="padding:15px">{base_path}/{version}/maps/import/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMapArchivePerformImport(importContextUuid, callback)</td>
    <td style="padding:15px">Import Map Archive - Perform Import</td>
    <td style="padding:15px">{base_path}/{version}/maps/import/{pathv1}/perform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteV2(groupNameHierarchy, id, type, offset, limit, callback)</td>
    <td style="padding:15px">Get Site V2</td>
    <td style="padding:15px">{base_path}/{version}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mapsSupportedAccessPoints(callback)</td>
    <td style="padding:15px">Maps Supported Access Points</td>
    <td style="padding:15px">{base_path}/{version}/maps/supported-access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesThatAreAssignedToASite(id, offset, limit, memberType, level, callback)</td>
    <td style="padding:15px">Get devices that are assigned to a site</td>
    <td style="padding:15px">{base_path}/{version}/site-member/{pathv1}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMapArchiveStartImport(callback)</td>
    <td style="padding:15px">Import Map Archive - Start Import</td>
    <td style="padding:15px">{base_path}/{version}/maps/import/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMapArchiveImportStatus(importContextUuid, callback)</td>
    <td style="padding:15px">Import Map Archive - Import Status</td>
    <td style="padding:15px">{base_path}/{version}/maps/import/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignDevicesToSite(siteId, body, callback)</td>
    <td style="padding:15px">Assign Devices To Site</td>
    <td style="padding:15px">{base_path}/{version}/assign-device-to-site/{pathv1}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportMapArchive(siteHierarchyUuid, body, callback)</td>
    <td style="padding:15px">Export Map Archive</td>
    <td style="padding:15px">{base_path}/{version}/maps/export/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSite(name, siteId, type, offset, limit, callback)</td>
    <td style="padding:15px">Get Site</td>
    <td style="padding:15px">{base_path}/{version}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteCount(siteId, callback)</td>
    <td style="padding:15px">Get Site Count</td>
    <td style="padding:15px">{base_path}/{version}/site/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMembership(siteId, offset, limit, deviceFamily, serialNumber, callback)</td>
    <td style="padding:15px">Get Membership</td>
    <td style="padding:15px">{base_path}/{version}/membership/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSite(siteId, body, callback)</td>
    <td style="padding:15px">Update Site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSite(siteId, callback)</td>
    <td style="padding:15px">Delete Site</td>
    <td style="padding:15px">{base_path}/{version}/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBusinessAPIExecutionDetails(executionId, callback)</td>
    <td style="padding:15px">Get Business API Execution Details</td>
    <td style="padding:15px">{base_path}/{version}/dnacaap/management/execution-status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeDevice(body, callback)</td>
    <td style="padding:15px">Authorize Device</td>
    <td style="padding:15px">/api/{version}/onboarding/pnp-device/authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCertificate(pkPassword, listOfUsers, callback)</td>
    <td style="padding:15px">importCertificate</td>
    <td style="padding:15px">{base_path}/{version}/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCertificateP12(p12Password, pkPassword, listOfUsers, callback)</td>
    <td style="padding:15px">importCertificateP12</td>
    <td style="padding:15px">{base_path}/{version}/certificate-p12?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEoXStatusForAllDevices(callback)</td>
    <td style="padding:15px">Get EoX Status For All Devices</td>
    <td style="padding:15px">{base_path}/{version}/eox-status/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEoXSummary(callback)</td>
    <td style="padding:15px">Get EoX Summary</td>
    <td style="padding:15px">{base_path}/{version}/eox-status/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEoXDetailsPerDevice(deviceId, callback)</td>
    <td style="padding:15px">Get EoX Details Per Device</td>
    <td style="padding:15px">{base_path}/{version}/eox-status/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associate(networkProfileId, siteId, callback)</td>
    <td style="padding:15px">Associate</td>
    <td style="padding:15px">{base_path}/{version}/networkprofile/{pathv1}/site/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociate(networkProfileId, siteId, callback)</td>
    <td style="padding:15px">Disassociate</td>
    <td style="padding:15px">{base_path}/{version}/networkprofile/{pathv1}/site/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNFVProfile(id, offset, limit, name, callback)</td>
    <td style="padding:15px">Get NFV Profile</td>
    <td style="padding:15px">{base_path}/{version}/nfv/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNFVProfile(id, name, body, callback)</td>
    <td style="padding:15px">Update NFV Profile</td>
    <td style="padding:15px">{base_path}/{version}/nfv/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNFVProfile(id, name, callback)</td>
    <td style="padding:15px">Delete NFV Profile</td>
    <td style="padding:15px">{base_path}/{version}/nfv/network-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nFVProvisioningDetail(body, callback)</td>
    <td style="padding:15px">NFV Provisioning Detail</td>
    <td style="padding:15px">{base_path}/{version}/nfv-provision-detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNFVProfile(body, callback)</td>
    <td style="padding:15px">Create NFV Profile</td>
    <td style="padding:15px">{base_path}/{version}/nfv/network-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationArchiveDetails(deviceId, fileType, createdTime, createdBy, offset, limit, callback)</td>
    <td style="padding:15px">Get configuration archive details</td>
    <td style="padding:15px">{base_path}/{version}/network-device-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportDeviceConfigurations(body, callback)</td>
    <td style="padding:15px">Export Device configurations</td>
    <td style="padding:15px">{base_path}/{version}/network-device-archive/cleartext?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdvisoriesSummary(callback)</td>
    <td style="padding:15px">Get Advisories Summary</td>
    <td style="padding:15px">{base_path}/{version}/security-advisory/advisory/aggregate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdvisoriesList(callback)</td>
    <td style="padding:15px">Get Advisories List</td>
    <td style="padding:15px">{base_path}/{version}/security-advisory/advisory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdvisoriesPerDevice(deviceId, callback)</td>
    <td style="padding:15px">Get Advisories Per Device</td>
    <td style="padding:15px">{base_path}/{version}/security-advisory/device/{pathv1}/advisory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdvisoryIDsPerDevice(deviceId, callback)</td>
    <td style="padding:15px">Get Advisory IDs Per Device</td>
    <td style="padding:15px">{base_path}/{version}/security-advisory/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesPerAdvisory(advisoryId, callback)</td>
    <td style="padding:15px">Get Devices Per Advisory</td>
    <td style="padding:15px">{base_path}/{version}/security-advisory/advisory/{pathv1}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceDetailCount(complianceType, complianceStatus, callback)</td>
    <td style="padding:15px">Get Compliance Detail Count</td>
    <td style="padding:15px">{base_path}/{version}/compliance/detail/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceDetailsOfDevice(deviceUuid, category, complianceType, diffList, callback)</td>
    <td style="padding:15px">Compliance Details of Device</td>
    <td style="padding:15px">{base_path}/{version}/compliance/{pathv1}/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitDeviceConfiguration(body, callback)</td>
    <td style="padding:15px">Commit device configuration</td>
    <td style="padding:15px">{base_path}/{version}/network-device-config/write-memory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceComplianceStatus(deviceUuid, callback)</td>
    <td style="padding:15px">Device Compliance Status</td>
    <td style="padding:15px">{base_path}/{version}/compliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatusCount(complianceStatus, callback)</td>
    <td style="padding:15px">Get Compliance Status Count</td>
    <td style="padding:15px">{base_path}/{version}/compliance/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatus(complianceStatus, deviceUuid, callback)</td>
    <td style="padding:15px">Get Compliance Status</td>
    <td style="padding:15px">{base_path}/{version}/compliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceDetail(complianceType, complianceStatus, deviceUuid, offset, limit, callback)</td>
    <td style="padding:15px">Get Compliance Detail</td>
    <td style="padding:15px">{base_path}/{version}/compliance/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runCompliance(body, callback)</td>
    <td style="padding:15px">Run Compliance</td>
    <td style="padding:15px">{base_path}/{version}/compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseUsageDetails(smartAccountId, virtualAccountName, deviceType, callback)</td>
    <td style="padding:15px">License Usage Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/usage/smartAccount/{pathv1}/virtualAccount/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLicenseSummary(pageNumber, order, sortBy, dnaLevel, deviceType, limit, registrationStatus, virtualAccountName, smartAccountId, deviceUuid, callback)</td>
    <td style="padding:15px">Device License Summary</td>
    <td style="padding:15px">{base_path}/{version}/licenses/device/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualAccountDetails(smartAccountId, callback)</td>
    <td style="padding:15px">Virtual Account Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/smartAccount/{pathv1}/virtualAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDeregistration(body, callback)</td>
    <td style="padding:15px">Device Deregistration</td>
    <td style="padding:15px">{base_path}/{version}/licenses/smartAccount/virtualAccount/deregister?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCountDetails(deviceType, registrationStatus, dnaLevel, virtualAccountName, smartAccountId, callback)</td>
    <td style="padding:15px">Device Count Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/device/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceRegistration(virtualAccountName, body, callback)</td>
    <td style="padding:15px">Device Registration</td>
    <td style="padding:15px">{base_path}/{version}/licenses/smartAccount/virtualAccount/{pathv1}/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartAccountDetails(callback)</td>
    <td style="padding:15px">Smart Account Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/smartAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeVirtualAccount(smartAccountId, virtualAccountName, body, callback)</td>
    <td style="padding:15px">Change Virtual Account</td>
    <td style="padding:15px">{base_path}/{version}/licenses/smartAccount/{pathv1}/virtualAccount/{pathv2}/device/transfer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLicenseDetails(deviceUuid, callback)</td>
    <td style="padding:15px">Device License Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/device/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseTermDetails(smartAccountId, virtualAccountName, deviceType, callback)</td>
    <td style="padding:15px">License Term Details</td>
    <td style="padding:15px">{base_path}/{version}/licenses/term/smartAccount/{pathv1}/virtualAccount/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployDeviceReplacementWorkflow(body, callback)</td>
    <td style="padding:15px">Deploy device replacement workflow</td>
    <td style="padding:15px">{base_path}/{version}/device-replacement/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnListOfReplacementDevicesWithReplacementDetails(faultyDeviceName, faultyDevicePlatform, replacementDevicePlatform, faultyDeviceSerialNumber, replacementDeviceSerialNumber, replacementStatus, family, sortBy, sortOrder, offset, limit, callback)</td>
    <td style="padding:15px">Return list of replacement devices with replacement details</td>
    <td style="padding:15px">{base_path}/{version}/device-replacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unMarkDeviceForReplacement(body, callback)</td>
    <td style="padding:15px">UnMark device for replacement</td>
    <td style="padding:15px">{base_path}/{version}/device-replacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markDeviceForReplacement(body, callback)</td>
    <td style="padding:15px">Mark device for replacement</td>
    <td style="padding:15px">{base_path}/{version}/device-replacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnReplacementDevicesCount(replacementStatus, callback)</td>
    <td style="padding:15px">Return replacement devices count</td>
    <td style="padding:15px">{base_path}/{version}/device-replacement/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemHealthCount(domain, subdomain, callback)</td>
    <td style="padding:15px">System Health Count API</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/system/health/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemHealth(summary, domain, subdomain, limit, offset, callback)</td>
    <td style="padding:15px">System Health API</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/system/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPerformanceHistorical(kpi, startTime, endTime, callback)</td>
    <td style="padding:15px">System Performance Historical API</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/system/performance/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPerformanceAPI(kpi, functionParam, startTime, endTime, callback)</td>
    <td style="padding:15px">System Performance API</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/system/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRole(body, callback)</td>
    <td style="padding:15px">Update role API</td>
    <td style="padding:15px">/dna/system/api/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRole(body, callback)</td>
    <td style="padding:15px">Add role API</td>
    <td style="padding:15px">/dna/system/api/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">Delete user API</td>
    <td style="padding:15px">/dna/system/api/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(invokeSource, authSource, callback)</td>
    <td style="padding:15px">Get users API</td>
    <td style="padding:15px">/dna/system/api/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(body, callback)</td>
    <td style="padding:15px">Update user API</td>
    <td style="padding:15px">/dna/system/api/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUser(body, callback)</td>
    <td style="padding:15px">Add user API</td>
    <td style="padding:15px">/dna/system/api/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(callback)</td>
    <td style="padding:15px">Get roles API</td>
    <td style="padding:15px">/dna/system/api/{version}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissions(callback)</td>
    <td style="padding:15px">Get permissions API</td>
    <td style="padding:15px">/dna/system/api/{version}/role/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalAuthenticationServers(invokeSource, callback)</td>
    <td style="padding:15px">Get external authentication servers API</td>
    <td style="padding:15px">/dna/system/api/{version}/users/external-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(roleId, callback)</td>
    <td style="padding:15px">Delete role API</td>
    <td style="padding:15px">/dna/system/api/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoDNACenterReleaseSummary(callback)</td>
    <td style="padding:15px">Cisco DNA Center Release Summary</td>
    <td style="padding:15px">{base_path}/{version}/dnac-release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoDNACenterNodesConfigurationSummary(callback)</td>
    <td style="padding:15px">Cisco DNA Center Nodes Configuration Summary</td>
    <td style="padding:15px">{base_path}/{version}/nodes-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoDNACenterPackagesSummary(callback)</td>
    <td style="padding:15px">Cisco DNA Center Packages Summary</td>
    <td style="padding:15px">{base_path}/{version}/dnac-packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssues(startTime, endTime, siteId, deviceId, macAddress, priority, issueStatus, aiDriven, callback)</td>
    <td style="padding:15px">get issues</td>
    <td style="padding:15px">{base_path}/{version}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueEnrichmentDetails(callback)</td>
    <td style="padding:15px">Get Issue Enrichment Details</td>
    <td style="padding:15px">{base_path}/{version}/issue-enrichment-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeSuggestedActionsCommands(body, callback)</td>
    <td style="padding:15px">Execute Suggested Actions Commands</td>
    <td style="padding:15px">{base_path}/{version}/execute-suggested-actions-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSensors(siteId, callback)</td>
    <td style="padding:15px">Sensors</td>
    <td style="padding:15px">{base_path}/{version}/sensor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSensorTestTemplate(body, callback)</td>
    <td style="padding:15px">Create sensor test template</td>
    <td style="padding:15px">{base_path}/{version}/sensor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSensorTest(templateName, callback)</td>
    <td style="padding:15px">Delete sensor test</td>
    <td style="padding:15px">{base_path}/{version}/sensor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateSensorTestTemplate(body, callback)</td>
    <td style="padding:15px">Duplicate sensor test template</td>
    <td style="padding:15px">{base_path}/{version}/sensorTestTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSensorTestTemplate(body, callback)</td>
    <td style="padding:15px">Edit sensor test template</td>
    <td style="padding:15px">{base_path}/{version}/AssuranceScheduleSensorTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runNowSensorTest(body, callback)</td>
    <td style="padding:15px">Run now sensor test</td>
    <td style="padding:15px">{base_path}/{version}/sensor-run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWLCToFabricDomain(body, callback)</td>
    <td style="padding:15px">Add WLC to Fabric Domain</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/wireless-controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeWLCFromFabricDomain(deviceIPAddress, body, callback)</td>
    <td style="padding:15px">Remove WLC from Fabric Domain</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/wireless-controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSIDToIPPoolMapping(vlanName, siteNameHierarchy, body, callback)</td>
    <td style="padding:15px">Get SSID to IP Pool Mapping</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/ssid-ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSSIDToIPPoolMapping(body, callback)</td>
    <td style="padding:15px">Update SSID to IP Pool Mapping</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/ssid-ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSIDToIPPoolMapping(body, callback)</td>
    <td style="padding:15px">Add SSID to IP Pool Mapping</td>
    <td style="padding:15px">{base_path}/{version}/business/sda/hostonboarding/ssid-ippool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationHealth(siteId, deviceId, macAddress, startTime, endTime, applicationHealth, offset, limit, applicationName, callback)</td>
    <td style="padding:15px">getApplicationHealth</td>
    <td style="padding:15px">{base_path}/{version}/application-health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDBSyncStatus(status, date, body, callback)</td>
    <td style="padding:15px">Get CMDB Sync Status</td>
    <td style="padding:15px">{base_path}/{version}/cmdb-sync/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFailedITSMEvents(instanceId, callback)</td>
    <td style="padding:15px">Get Failed ITSM Events</td>
    <td style="padding:15px">{base_path}/{version}/integration/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryIntegrationEvents(body, callback)</td>
    <td style="padding:15px">Retry Integration Events</td>
    <td style="padding:15px">{base_path}/{version}/integration/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEnrichmentDetails(callback)</td>
    <td style="padding:15px">Get User Enrichment Details</td>
    <td style="padding:15px">{base_path}/{version}/user-enrichment-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
