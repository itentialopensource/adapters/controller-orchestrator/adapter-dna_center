# Cisco DNA Center

Vendor: Cisco Systems
Homepage: https://www.cisco.com/

Product: DNA Center
Product Page: https://www.cisco.com/site/id/en/products/networking/dna-center-platform/index.html

## Introduction
We classify Cisco DNA Center into the Network Services domain as Cisco DNA Center provides capabilities in centralized control and orchestration of various network services, including provisioning, monitoring, and troubleshooting of devices. 

"Cisco DNA Center provides automation capabilities in the Enterprise Network." 

The Cisco DNA Center adapter can be integrated to the Itential Device Broker which will allow your Cisco DNA Center devices to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Cisco DNA Center adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco DNA Center. With this adapter you have the ability to perform operations such as:

- Configure and Manage Cisco DNA Center Devices. 
- ITSM
- Sites
- Tasks
- Networks

## Additional Product Documentation
The [API documents for Cisco DNA Center](https://developer.cisco.com/docs/dna-center/#!api-quick-start/cisco-dna-center-platform-api-overview)

