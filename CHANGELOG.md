
## 0.11.5 [10-15-2024]

* Changes made at 2024.10.14_21:33PM

See merge request itentialopensource/adapters/adapter-dna_center!31

---

## 0.11.4 [08-25-2024]

* fix vulnerabilities

See merge request itentialopensource/adapters/adapter-dna_center!29

---

## 0.11.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-dna_center!28

---

## 0.11.2 [08-15-2024]

* Changes made at 2024.08.14_19:53PM

See merge request itentialopensource/adapters/adapter-dna_center!27

---

## 0.11.1 [08-07-2024]

* Changes made at 2024.08.06_22:00PM

See merge request itentialopensource/adapters/adapter-dna_center!26

---

## 0.11.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!25

---

## 0.10.0 [04-09-2024]

* Add more calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!24

---

## 0.9.8 [03-28-2024]

* Changes made at 2024.03.28_13:47PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!22

---

## 0.9.7 [03-21-2024]

* Changes made at 2024.03.21_14:31PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!21

---

## 0.9.6 [03-13-2024]

* Changes made at 2024.03.13_11:13AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!20

---

## 0.9.5 [03-11-2024]

* Changes made at 2024.03.11_16:02PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!19

---

## 0.9.4 [02-28-2024]

* Changes made at 2024.02.28_11:32AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!18

---

## 0.9.3 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!17

---

## 0.9.2 [12-14-2023]

* Add SDA Calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!16

---

## 0.9.1 [11-07-2023]

* Add SDA Calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!16

---

## 0.9.0 [11-07-2023]

* More Migration Changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!15

---

## 0.8.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!14

---

## 0.7.0 [10-03-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!14

---

## 0.6.2 [07-20-2023]

* Fix request array

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!13

---

## 0.6.1 [04-12-2023]

* Cisco DNA Center Limit and Offset

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!12

---

## 0.6.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!11

---

## 0.5.7 [05-03-2022]

- Add 2 calls based on different paths seen in documentation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!10

---

## 0.5.6 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!8

---

## 0.5.5 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!7

---

## 0.5.4 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!6

---

## 0.5.3 [11-22-2019]

- Updated the entitypaths and then pipeline having issues - mostly due to not being able to run the audit but then the publish failed.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!5

---

## 0.5.2 [11-22-2019] & 0.5.1 [11-19-2019]

- Update the healthcheck url to one that has been tested in the lab

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!4

---

## 0.5.0 [11-12-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!3

---

## 0.4.0 [09-13-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorization and prepare for app artifact

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!1

---

## 0.1.1 [04-24-2019]

- Initial Commit

See commit b3acaab

---
