
## 0.6.2 [07-20-2023]

* Fix request array

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!13

---

## 0.6.1 [04-12-2023]

* Cisco DNA Center Limit and Offset

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!12

---

## 0.6.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!11

---

## 0.5.7 [05-03-2022]

- Add 2 calls based on different paths seen in documentation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!10

---

## 0.5.6 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!8

---

## 0.5.5 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!7

---

## 0.5.4 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!6

---

## 0.5.3 [11-22-2019]

- Updated the entitypaths and then pipeline having issues - mostly due to not being able to run the audit but then the publish failed.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!5

---

## 0.5.2 [11-22-2019] & 0.5.1 [11-19-2019]

- Update the healthcheck url to one that has been tested in the lab

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!4

---

## 0.5.0 [11-12-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!3

---

## 0.4.0 [09-13-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorization and prepare for app artifact

See merge request itentialopensource/adapters/controller-orchestrator/adapter-dna_center!1

---

## 0.1.1 [04-24-2019]

- Initial Commit

See commit b3acaab

---
